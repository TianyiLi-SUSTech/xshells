#!/usr/bin/python

## Usage:
## 1) run all tests:
##      ./test.py
## 2) run selected tests:
##      ./test.py geodynamo -stepper=PC2 -exe=./xsbig_hyb

## define test cases with xshells.hpp files, a list of xshells.par files that contain
## special TEST comments, and executables to build and test.
problems = [
        {'name': 'MAGNETIC DIPOLE DECAY RATE & KINEMATIC DYNAMO',
    'hpp': 'problems/kinematic_dynamos/xshells.hpp',
    'lst': ['problems/kinematic_dynamos/xshells.par', 'problems/kinematic_dynamos/xshells.par.dudley_james_s2t2', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb']},
        {'name': 'GEODYNAMO BENCHMARK',
    'hpp': 'problems/geodynamo/xshells.hpp',
    'lst': ['problems/geodynamo/xshells.par', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
        {'name': 'FULL-SPHERE BENCHMARK',
    'hpp': 'problems/full_sphere/xshells.hpp',
    'lst': ['problems/full_sphere/xshells.par.fs3', 'problems/full_sphere/xshells.par.fs1', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
        {'name': 'FULL-SPHERE PRECESSION',
     'hpp': 'problems/precession_sphere/xshells.hpp',
    'lst': ['problems/precession_sphere/xshells.par','problems/precession_sphere/xshells.par.rotframe', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
       ]

# time steppers to test:
steppers = ['PC2', 'BPR353', 'CNAB2', 'SBDF2', 'SBDF3']

global_opt = ['-shared_mem=-1','-bidir_solve=1']   # with shared memory and bidirectional solves


from numpy import *
import re
from subprocess import call
from shutil import copyfile
import os
import time
import sys
sys.path.insert(0, './python')   # allow to find the module xsplot, even if it is not installed:
import xsplot

## some init code:
if hasattr(time,'perf_counter'):
    mytime = time.perf_counter
else:
    mytime = time.time


## Handle command line arguments:
exe=[]
for a in sys.argv[1:]:
    if a.startswith('-stepper'):   # select stepper
        steppers=[a[9:],]
    elif a.startswith('-exe'):     # select executable
        exe.append(a[5:])
    else:   # reduce the problems to the ones matching string given as argument (if given)
        pb2 = []
        for pb in problems:
            if a in pb['name'].lower():
                pb2.append(pb)
        problems = pb2
if len(exe) > 0:
    for pb in problems:
        pb['exe'] = exe


## functions

def compare_with_ref(diag_file, time, diag_dict):
    """ compare result of run with a reference, for testing purposes """
    y=xsplot.load_diags(diag_file, print_header=False)
    w = where(y['t'] == time)[0]  # compare values at given time
    if len(w) == 0:
        return(1)   # time not available => error
    it = w[0]   
    err_max = 0
    for d in diag_dict.keys():
        ref = diag_dict[d]
        if d not in y:
            print(bcolors.FAIL + '!! diagnostic %s unavaiable !!' % d + bcolors.ENDC)
            return(2)   # diagnostic not available => error
        run = y[d][it]
        if isnan(run):
            return(Inf) # Nan ? => error
        err = run-ref
        if ref != 0.:
            err = err/ref
        err_max = max( err_max, abs(err) )
        print('  * ', d, ': ref=%g, run=%g' % (ref,run))
    return(err_max)

def run_test(exe, job, cmd, parfile, stepper='PC2'):
    v = {'ref':None, 'time':None, 'opt':''}   # uninitialize
    exec(cmd,v)     # set variables
    nrj_file = 'energy.'+job
    if exe is not None:
        nthreads=4
        callpar = v['opt'].split()
        callpar = [exe, parfile] + callpar + global_opt
        if exe == './xsbig_mpi':
            callpar = ['mpirun', '-n', '4'] + callpar
        elif exe in ['./xsbig_hyb', './xsbig_hyb2']:
            callpar = ['mpirun', '--bind-to','none', '-n','2'] + callpar
            nthreads = 2
        callpar.append('-restart=0')  # we always want to start from scratch
        callpar.append('-movie=0')  # we don't want to write so many files
        callpar.append('-stepper='+stepper)
        print(' '.join(callpar))
        if os.path.isfile(nrj_file):
            os.remove(nrj_file)
        with open('log.'+job, "w") as logfile:
            tex=mytime()
            ev=os.environ.copy()
            ev['OMP_NUM_THREADS'] = '%d' % nthreads
            call(callpar, stdout=logfile, env=ev)
            tex=mytime()-tex
    errmax = compare_with_ref(nrj_file, v['time'], v['ref'])
    return(errmax,tex)

def make_exe(hpp, exe):
    copyfile(hpp, 'xshells.hpp')
    os.system('make -j4 ' + ' '.join(exe))

## color in output:
class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

jobreg = re.compile(r"job\s*=\s*([^#\s]*)")
passed, suspected, failed = 0,0,0

for pb in problems:
    exe = pb['exe']
    lst = pb['lst']
    print(bcolors.BOLD+bcolors.WARNING + '####', pb['name'], '####'+bcolors.ENDC)
    make_exe(pb['hpp'], exe)
    for case in lst:
        tests = []
        f = open(case,'r')
        l = f.readline()
        while l != '':
            if l.startswith("#TEST "):
                tests.append(l.replace("#TEST ","").strip())
            if l[0] != '#':
                x = l.split('#')[0]  # remove everything after the #
                jobs = jobreg.findall(x)
                if len(jobs) > 0:
                    job = jobs[0]
            l=f.readline()

        print('  ***',case,'**',job,'***')
        for ex in exe:
            for test in tests:
                for ts in steppers:
                    err,tex=run_test(ex, job, test, case, ts)
                    print("\t max relative error = %.3g, runtime = %gs" % (err,tex))
                    if err > 1e-2:
                        failed += 1
                        print(bcolors.FAIL+ "\t ERROR: more than 1% relative error with reference case. Something is wrong." +bcolors.ENDC)
                    elif err > 5e-4:
                        suspected += 1
                        print(bcolors.WARNING+ "\t WARNING: differences with reference case are suspicious." +bcolors.ENDC)
                    else:
                        passed += 1
                        print(bcolors.OKGREEN + "\t PASS" +bcolors.ENDC)
                print('')

print(bcolors.BOLD + '**** SUMMARY ****' + bcolors.ENDC)
if passed > 0:
    print(bcolors.OKGREEN + '   %d tests OK' % passed +bcolors.ENDC)
if suspected > 0:
    print(bcolors.WARNING + '   %d tests suspect' % suspected +bcolors.ENDC)
if failed > 0:
    print(bcolors.FAIL + '   %d tests FAILED' % failed +bcolors.ENDC)
    sys.exit(failed)

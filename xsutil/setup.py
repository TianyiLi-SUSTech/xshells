#!/usr/bin/python

from distutils.core import setup

setup(name='xsutil',
      version='0.1',
      description='python tools for handling output of xshells',
      author='Elliot Kaplan',
      packages=['parametermap', 'xsenergy', 'xsbatch', 'xspar'],
      scripts=['xsenergy/plot_dataset.py', 'xsenergy/subruns.py',
               'xspar/xshells_update.py', 'xspar/xspar_read.py',
               'xsbatch/xsppbatch.py', 'xsbatch/xspp_read.py',
#               'xsbatch/xspp_toh5.py', 'xsbatch/xspph5_tomovie.py',
               ],
      requires=['numpy', 'scipy', 'matplotlib', 'pandas'],
)

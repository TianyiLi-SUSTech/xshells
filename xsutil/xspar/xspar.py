#!/usr/bin/python3

# -*- coding: utf-8 -*-
import re
import numpy as np

OPDICT = {'+': np.add,
          '*': np.multiply,
          '/': np.divide}

OPREG= re.compile(r'[{}]'.format(''.join(OPDICT.keys())))


def parse_xspar(xspath, *xsopts, empty=None, evaluate=False, return_dict=False):
    """parse xshells.par file

    reads an xshells.par file and returns a dictionary containing the
    values of the requested options. If evaluate is False, only strings
    are returned. If evaluate is True, floats are returned where
    possible, and some simple arithmetic can be performed.
    """
    # regex for the specified parameters
    parreg = re.compile(r'''^
    (?P<opt>{}) # options to read
    \s*=\s*
    (?P<value>[^#\s]*)
    '''.format('|'.join(xsopts)), re.VERBOSE)

    # parse the xshells.par files
    with open(xspath, 'r') as f:
        matches = (parreg.match(line) for line in f)
        xsoptdict = {m.group('opt'): m.group('value') for m in matches if m}
    # fill in blanks
    if empty is not None:
        for opt in xsopts:
            if opt not in xsoptdict.keys():
                xsoptdict[opt] = empty
    # evaluate numerical values, attempt to evaluate formulae
    if evaluate:
        xsoptdict = evaluate_xsoptdict(xspath, xsoptdict)
    if return_dict:
        return xsoptdict
    if len(xsoptdict) == 1:
        return xsoptdict[xsopts[0]]
    # if return_dict is not set, values are returned in the same order they
    # were requested in.
    return tuple([xsoptdict.get(key, None) for key in xsopts])


def use_template(src, dest, **xsopts):
    """write an xshells.par file from a template

    Reads in an xshells.par file from src and writes a new one to dest
    replacing the specified options.
    """
    parreg = re.compile(r'''^
    (?P<opt>{}) # options to read
    \s*=\s* 
    (?P<value>[^#\s*]*) # value to replace
    '''.format('|'.join(xsopts.keys())), re.VERBOSE)
    with open(src, 'r') as fin, open(dest, 'w') as fout:
        matches = ((line, parreg.match(line)) for line in fin)
        outlines = (m.group('opt')+' = '+str(xsopts[m.group('opt')]+'\n')
                    if m else l for l, m in matches)
        fout.writelines(outlines)
    

def _evaluate_piece(xspath, xseval, expression):
    # function for evalutating each piece of an expression
    # first do a straight evaluation
    try:
        return float(expression)
    # next see if it's already been calculated
    except ValueError:
        try:
            return xseval[expression]
        # next pull it from the xshells.par file (recursive)
        except KeyError:
            return parse_xspar(xspath, val_string, evaluate=True)[val_string]


def evaluate_xsoptdict(xspath, xsoptdict):
    """Evalues expressions in the xshells.par file."""

    # do straight conversions to floats for cases with no operations
    xseval = {key: float(val) for key, val in xsoptdict.items() if OPREG.search(val) is None}
    for key in xseval.keys():
        xsoptdict.pop(key)
    xseval['pi'] = np.pi
    # now step through each string with operations
    for key, value in xsoptdict.items():
        val_float = 0.0
        op = '+'
        start = -1
        for match in OPREG.finditer(value):
            val_string = value[start+1:match.start()]
            val_float = OPDICT[op](val_float, _evaluate_piece(xspath, xseval, val_string))
            op = match.group(0)
            start = match.start()
        val_string = value[start+1:]
        xseval[key] = OPDICT[op](val_float, _evaluate_piece(xspath, xseval, val_string))
    xseval.pop('pi')
    return xseval


def parse_xspath(xspath, regex=r'(?P<param>\D*)(?P<val>[-e.\d]*)', sep='_'):
    '''parses filepaths

    The filepaths must end with
    [label]_parval1_parval2..._parvalN. Where each parval is a
    combination of a string and a number. Alternate regexprs can be
    provided
    '''
    assert xspath.rfind('xshells.par') != -1, 'must provide xshells.par files'
    # replace doubled separators with single before the split
    d = xspath.replace(sep*2,sep).split(sep)[-2] 
    dictionary = {match.group('param'): float(match.group('val'))
                  for match in re.finditer(sep+regex, d)}
    # pull the jobname from the xshells.par file
    dictionary.update(parse_xspar(xspath, 'job'))
    return dictionary



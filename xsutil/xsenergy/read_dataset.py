"""
Set of routines to read in energy.[JOBNAME] files and convert them into pandas dataframes.
"""
import re
from colorama import Fore
from subprocess import check_output, Popen, PIPE
from collections import deque
from itertools import groupby, chain
from colorama import Fore

import pandas as pd
import numpy as np

def warn(st):
    """Format a string as a warning."""
    return ''.join([Fore.RED, st, Fore.RESET])

# regex to split up all the fields in the header, regardless of type
NAMESREG = re.compile(r'\s*[,]?\s*')
# regex to split the header into groups, and the groups into entries
GROUPSREG = re.compile(r'\s*\t\s*')
ENTRIESREG = re.compile(r'\s*,\s*')
# regex to process the specprobe entries
SPECPROBEREG = re.compile(r'(?P<specprobe>pt\d)_(?P<m>m=\d*)_(?P<reim>Re|Im)')
# regex for symmetry heirarchies
SYMLABELS = (('z', {'Z': 'zonal', 'NZ': 'nonzonal'}),
             ('k', {'u_': 'kinetic', 'b_': 'magnetic'}),
             ('p', {'p': 'poloidal', 't': 'toroidal'}),
             ('s', {'s': 'symmetric', 'a': 'antisymmetric'}))
SYMREG = re.compile('E'+''.join(('(?P<{}>{})'.format(k, '|'.join(v.keys())) for k, v in SYMLABELS)))
SYMLABELS = {k: v for k, v in SYMLABELS}
# regex for velocity probes
# PROBEREG = re.compile(r'(?P<probe>[bv][rtp])_(?P<NR>\d*)_(?P<LAT>\d*)_(?P<LONG>\d*)')
PROBEREG = re.compile(r'(?P<probe>[bv][rtp])(?P<ind>\d*)')
POTREG = re.compile(r'(?P<V>v)(?P<lat>[+-]\d\d)')
# regex for mode energies
MENGREG = re.compile(r'(?P<m>m=\d*)')

class LastHeader():
    """Checks for new header strings. For use with groupby"""
    def __init__(self, sentinel='#'):
        self.sentinel = sentinel
        self.lastheader = ''
        self.index=0

    def check(self, line):
        self.index += 1
        if line.startswith(self.sentinel):
            self.lastheader = line
        return self.lastheader
    

def sym_heirarchy(dat, order=('sym','poltor','zonal')):
    """Makes a heirarchical column index for energy data.
    
    Given a dataframe of kinetic/magnetic energies split into
    symmetries, poloidal/toroidal bases, and zonal/nonzonal components,
    returns the a heirarchical index with the various components split at
    different heirarchy levels.
    """
    ordqueue = (odr[0].lower() for odr in order)
    # get the regexes for each column
    regs = [SYMREG.match(col) for col in dat.columns]
    cols = [[SYMLABELS[odr][match.group(odr)] for match in regs] for odr in ordqueue]
    ind =  pd.MultiIndex.from_arrays(cols)
    ind.names = order
    return ind

def to_magnitude_phase(dat):
    """
    Converts a dataframe containing real and imaginary components
    into one containing magnitude and phase information
    """
    reg = re.compile(r'm=[1-9]\d*')
    # get the columns of the phase data
    cols = dat.columns[[reg.match(dat.columns.levels[1][i]) is not None
                        for i in dat.columns.labels[1]]]
    # make a new set of column labels, with Real and Imaginary components rotated to top level
    cols2 = cols.swaplevel(2, 1).swaplevel(0, 1)
    cols3 = pd.MultiIndex(levels=[['', 'Mag', 'Phase'], cols2.levels[1], cols2.levels[2]],
                          labels=cols2.labels)
    reim = dat[cols]
    reim.columns = cols2
    # create an empty dataframe to hold the magnitue and phase data
    magpha = pd.DataFrame(np.empty((len(dat.index), len(cols3)), np.double),
                          index=dat.index, columns=cols3)
    magpha.Mag = np.sqrt(reim.Re**2 + reim.Im**2)
    magpha.Phase = np.arctan2(reim.Im, reim.Re).fillna(0.0).apply(np.unwrap, axis=0)
    return magpha

def to_difference(dat):
    """Calculates potential difference.

    Converts a dataframe consisting of raw potentials at various
    latitudes to one of potential differences labeled by midpoint.
    Only works with non heirarchical data at the moment
    """
    
    matches = (POTREG.match(c) for c in dat.columns)
    col_lat = [(m.group(0), np.int(m.group('lat'))) for m in matches if m]
    cols, lats = list(chain(zip(*col_lat)))
    lats = np.array(lats)
    mids = (lats[1:] + lats[:-1]) // 2
    newcol = {c: 'v{:+2d}'.format(m) for c, m in zip(cols, mids)}
    for c1, c2 in zip(cols[:-1], cols[1:]):
        dat[c1] = dat[c2] - dat[c1]
    dat.rename(columns=newcol, inplace=True)
    dat.drop(c2, inplace=True, axis=1)
    return            

def sort_columns(dat, limit=None, sortby='max'):
    """sorts the columns of a dataframe by the specified function"""
    ind = getattr(dat, sortby)()
    ind.sort(ascending=False)
    if limit:
        ind = ind[ind.index[:min(limit, len(ind.index))]]
    return dat.reindex(columns=ind.index)

def regex_columns(dat, regex):
    """reindexes a dataframe to use the columns specified in reg"""
    reg = re.compile(regex)
    return dat.reindex(columns=dat.columns[[reg.search(c) is not None for c in dat.columns]])

def parse_header(header, comment='%', **kwargs):
    """returns a list of column names based on the provided header"""
    try:
        header = header.strip(comment).strip()
    except TypeError: # necessary if the result of check_output is used
        header = header.decode("utf-8").strip(comment).strip()
    # get the column names
    names = NAMESREG.split(header)
    # pop the empty string if it's there
    if names[-1] == '':
        names.pop()
    if not kwargs.get('heirarchy', False):
        return names, None
    # default ordering for energy split
    order = [g[0].lower() for g in kwargs.get('engorder', ('kind', 'zonal', 'poltor', 'symmetry'))]

    # tab characters split groups
    groups = deque(GROUPSREG.split(header))
    # we know that the first items are t, energies, and dt
    convdict = {'u': 'kin', 'b': 'mag', 't': 'therm'}
    columns = [[groups.popleft(), ]]
    columns += [['energy', convdict.pop(key[-1]), ] for key in groups.popleft().split(',')]
    columns += [[groups.popleft(), ]]
    while groups:
        group = ENTRIESREG.split(groups.popleft().strip().strip(','))
        # spectral convergence
        if group[0] == 'Spectral_Convergence':
            columns += [['Spectral_Convergence', ]]
            continue
        # energy split by symmetry
        match = SYMREG.match(group[0])
        if match:
            matches = (SYMREG.match(g) for g in group)
            columns += [[SYMLABELS[r][m.group(r)] for r in order]
                        for m in matches]
            continue
        # point probes
        match = PROBEREG.match(group[0])
        if match:
            matches = (PROBEREG.match(g) for g in group)
            columns += [[m.group(r) for r in ('probe', 'ind')] for m in matches]
            # columns += [[m.group(r) for r in ('probe', 'NR', 'LAT', 'LONG')]
            #             for m in matches]   
            continue
        # energy split by m in core
        match = MENGREG.match(group[0])
        if match:
            matches = (MENGREG.match(g) for g in group)
            columns += [['m_eng_core', m.group('m')] for m in matches]
            continue
        # voltages at surface
        match = POTREG.match(group[0])
        if match:
            matches = (POTREG.match(g) for g in group)
            columns += [['potential', m.group('lat')] for m in matches]
        # kinetic energy split by mode number
        if len(group) == 1:
            columns += [g.split('=') for g in group[0].split()]
            continue
        # local phase information
        for grp in group:
            try:
                columns += [list(SPECPROBEREG.search(grp).groups())]
            except AttributeError:
                pass
    # convert the columns from a list of lists to a dict of lists, for use later
    columns = {k: v for k, v in zip(names, columns)}
    return names, columns
    # # pop the index column if there is one
    # if kwargs.get('index_col', False):
    #     columns.pop(names.index(kwargs['index_col']))
    # # add empty strings to name lists for even length
    # depth = max([len(n) for n in columns])
    # columns = [n + ['', ] * (depth-len(n)) for n in columns]
    # columns = list(zip(*columns))
    # return names, columns

def read_table(fname, sep=' ', warnings=False,
               column_regex=None, column_names=None,
               **kwargs):
    """wrapper for pd.read_table"""
    assert (column_names is None) or (column_regex is None), 'cannot specify a regex and column names'
    # defaults
    default_kwargs = (('index_col', 't'), ('comment', '%'))
    kwargs.update({key: val for key, val in default_kwargs if key not in kwargs.keys()})
    
    index_col = kwargs.get('index_col', 't')
    comment = kwargs.get('comment', '%')

    # compile the column_regex if necessary
    if column_regex is not None:
        if not hasattr(column_regex, 'match'):
            column_regex = re.compile(column_regex)
    
    # find the header lines in the energy file. Determine how many rows to read
    # at one time from their separation.
    multicol = {}
    with open(fname, 'r') as fobj:
        lastheader = LastHeader(kwargs.get('comment'))
        for headerline, readlines in groupby(fobj, lastheader.check):
            names, columns = parse_header(headerline, **kwargs)
            # determine the columns to parse
            # select by regex
            usecols = None
            try:
                # make sure that time is included no matter what. Use a regex
                # match to find the rest of the columns
                names = [(0, names[0]),] + \
                  [(i, n) for i, n in enumerate(names[1:], 1) if column_regex.match(n)]
                    
                usecols, names = tuple(n for n in (zip(*names)))
            except AttributeError:
                pass
            # select by a list of column names
            try:
                names = [(0, names[0]),] + \
                  [(i, n) for i, n in enumerate(names[1:], 1) if n in column_names]
                usecols, names = tuple(n for n in zip(*names))
            except TypeError:
                pass
            # shorten the multiindex if it's there
            try:
                columns = {n: columns[n] for n in names}
            except TypeError:
                pass
            # old versions of xshells would only write a header if the
            # simulation were brand new (i.e. not a restart). If that's the
            # case there may be lines that have the wrong number of columns,
            # breaking loadtxt. The energy file has to be fixed manually, but
            # the error message should help find the location of the split.
            try:
                df = np.loadtxt(readlines, comments=kwargs.get('comment'),
                                usecols=usecols)
            except ValueError as err:
                print(fname, err)
                print(len(fobj.readline().split()))
                # get the next header
                iterator = groupby(fobj, lastheader.check)
                for headerline, readlines in groupby(fobj, lastheader.check):
                    print(headerline, lastheader.index)
                raise err
            # the DataFrame object won't be initialized properly if there are
            # fewer rows than columns. If that's the case the simulation was
            # probably aborted after one time step. The energy file must be
            # fixed manually.
            try:
                df = pd.DataFrame(df, columns=names)
            except ValueError as err:
                if warnings:
                    print(Fore.RED, err, Fore.RESET)
                    raise err
                continue
            if index_col is not None:
                df.drop_duplicates(index_col, inplace=True)
                df.set_index(index_col, inplace=True)
                df.sort_index(inplace=True)
            # if we already have a return dataframe, update it
            try:
                dat = dat.merge(df, how='outer',
                                left_index=True, right_index=True,
                                suffixes=('', '_'))
                for col in (c for c in dat.columns if not c.endswith('_')):
                    upd = ''.join((col, '_'))
                    try:
                        dat[col].update(dat[upd])
                        dat.drop(upd, axis=1, inplace=True)
                    except KeyError:
                        pass
            except UnboundLocalError:
                dat = df
            if columns is not None:
                multicol.update(columns)
    # generate the multi index if necessary
    if multicol:
        ind = [multicol[c] for c in dat.columns]
        depth = max((len(c) for c in ind))
        ind = [c + ['', ] * (depth - len(c)) for c in ind]
        dat.columns = pd.MultiIndex.from_arrays((list(zip(*ind))))
    return dat


# if __name__=='__main__':
#     print(read_table('/home/kaplane/energy.EK3E-6', heirarchical=True, index_col='t')['kinetic'])
#     print(read_table('/home/kaplane/energy.EK3E-6', heirarchical=True, index_col='t').columns)

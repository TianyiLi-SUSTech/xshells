#!/usr/bin/python3
# stdlib imports
from itertools import groupby, compress
import argparse
from colorama import Fore
# Scipy stack imports
import numpy as np
import pandas as pd
# xshells imports
from xsenergy.read_dataset import parse_header

class SubrunCount():
    """Checks for new header strings. For use with groupby"""
    def __init__(self, sentinel='#'):
        self.sentinel = sentinel
        self.index = 0
        self.tail = ''

    def check(self, line):
        self.index += 1
        # extra spaces at end make sure that new subruns are registered by
        # groupby. The spaces themselves are discarded by parse_header
        if line.startswith(self.sentinel):
            self.lastheader = ''.join([line, self.tail])
            self.tail += ' '
        return self.lastheader

def warn(st):
    """Format a string as a warning."""
    return ''.join([Fore.RED, st, Fore.RESET])

def subruns(fname, comment='%', columns=None, timeonly=False):
    """Statistics of individual runs of xshells.

    Splits a file into segments and calculates the first order
    statistics on each segment.
    """
    usecols = None
    if timeonly:
        usecols = [0, ]
    subruncount = SubrunCount(comment)
    with open(fname, 'r') as fobj: 
        for subind, (headerline, readlines) in enumerate(groupby(fobj, subruncount.check)):
            startind = np.int(subruncount.index)
            names = parse_header(headerline)[0]
            if timeonly:
                names = names[:1]
            try:
                dat = np.loadtxt(readlines, comments=comment,
                                 usecols=usecols)
            except ValueError as err:
                print(warn("malformed row at {}".format(subruncount.index)))
            try:
                dat = pd.DataFrame(dat, columns=names)
                print('Subrun {}\n\tLines {}-{}'.format(
                    subind, startind, subruncount.index-1))
            # catch runs too short to load into a dataframe properly
            except ValueError as err:
                print(warn("Subrun {} failed to load. Error at line {}".format(subind, startind)))
                continue
            # reduce the printout to desired columns
            if columns is not None:
                dat.drop([c for c in dat.columns if c not in columns], axis=1, inplace=True)
            if timeonly:
                print(dat.describe([]).drop(['50%', 'mean', 'std']).T)
            else:
                print(dat.describe([]).drop('50%').T)
            dat.set_index('t', inplace=True)
            if not dat.index.is_monotonic:
                print(warn('Time does not increase monotonically'))
            print()
    return

def remove_sub(fname, target, remove=[], comment='%'):
    """Cuts specified runs from file."""
    with open(fname, 'r') as fin, open(target, 'w') as fout:
        subruncount = SubrunCount(comment)
        fin.seek(0)
        for subind, (headerline, readlines) in enumerate(groupby(fin, subruncount.check)):
            if subind not in remove:
                fout.writelines(readlines)
    return

def remove_malformed(fname, target, comment='%'):
    """Deletes malformed rows

    Any row which does not work with the preceeding column declaration
    is removed. These rows usually appear if two instances of xshells
    were running concurrently.
    """
    subruncount = SubrunCount(comment)
    malformed = []
    # run through once to find all the malformed lines
    with open(fname, 'r') as fobj:
        for subind, (headerline, readlines) in enumerate(groupby(fobj, subruncount.check)):
            startind = np.int(subruncount.index)
            while True:
                try:
                    dat = np.loadtxt(readlines, comments=comment)
                except ValueError as err:
                    malformed.append(subruncount.index)
                    print("malformed row at {}".format(subruncount.index))
                    continue
                break
    # lines are 1 indexed when speaking to user (vi has 1-indexed rows), need
    # zero indexed rows for the output
    writeout = np.ones(subruncount.index, np.bool)
    writeout[np.array(malformed) -1] = False
    with open(fname, 'r') as fin, open(target, 'w') as fout:
        fout.writelines(compress(fin, writeout))

if __name__=='__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='''
        Produces summary statistics on individual runs of xshells
        within a single energy file.
        ''')
    parser.add_argument('fname', type=str,
                        help='name of file to summarize')
    parser.add_argument('-r', '--remove', nargs='+', default=None,
                        help='''segments to remove from the energy file.
                        First argument is the name of the new file.''')
    parser.add_argument('-c', '--columns', nargs='+',
                        default=['t', 'Eu', 'Et', 'Eb'],
                        help='Column names to summarize')
    parser.add_argument('--timeonly', action='store_true',
                        help='set to only check timespans')
    parser.add_argument('--fixrows', type=str, default=None,
                        help='set to delete malformed rows')
    clin = parser.parse_args()
    print(Fore.CYAN + clin.fname + Fore.RESET)
    subruns(clin.fname, columns=clin.columns, timeonly=clin.timeonly)
    if clin.remove is not None:
        remove_sub(clin.fname, clin.remove[0], [np.int(r) for r in clin.remove[1:]])
    if clin.fixrows is not None:
        remove_malformed(clin.fname, clin.fixrows, comment='%')

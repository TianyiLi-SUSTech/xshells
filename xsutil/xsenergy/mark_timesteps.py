import numpy as np
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from subprocess import check_output
import sys
from glob import glob
from os.path import getsize

from xsenergy.read_dataset import read_table

def get_inds(df, field, files, sp_args):
    col = 'E{}'.format(field.lower())
    df = df.sort_index(by=col)
    tarr = np.empty(len(files), np.double)
    tarr[-1] = 0.0
    for i, f in enumerate(files):
        print('{} / {}'.format(i, len(files)),f)
        sp_args[1] = f
        # total energy of file is last entry of string
        eng = np.double(check_output(sp_args).split()[-1])
        # get the time with the closest energy. Make sure that t is
        # always increasing
        tarr[i] = (df[col][df.index > tarr[i-1]]-eng).abs().argmin()
    return files, df.ix[tarr]

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='plots time traces of energy and marks locations of field[UBT] files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('directory', type=str,
                        help='location of dataset')
    parser.add_argument('suffix', type=str,
                        help='name of run')
    parser.add_argument('--xshells_dir', type=str, default='.',
                        help='location of xspp executable')
    clin = parser.parse_args()
    spec = (clin.directory, clin.suffix)
    # read the dataset
    dat = read_table('{}/energy.{}'.format(*spec))
    sp_args = ['{}/xspp'.format(clin.xshells_dir), '', 'nrj']
    ax = dat.plot('Eu')
    markfiles = sorted(glob('{}/fieldU_[0-9][0-9][0-9][0-9].{}'.format(*spec)))
    markfiles += ['{}/fieldU_back.{}'.format(*spec), '{}/fieldU.{}'.format(*spec)]
    markfiles = markfiles[::10]
    files, df = get_inds(dat, 'U', markfiles, sp_args)
    print(df)
    # for (f, t, eng) in zip(files, tarr, engarr):
    #     try:
    #         ax.text(t, eng, f.split('/')[-1])
    #     except IndexError:
    #         pass
    # plt.show()

    

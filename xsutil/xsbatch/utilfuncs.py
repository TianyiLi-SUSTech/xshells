# convenience functions
import sys
import shutil
from io import StringIO
from functools import wraps
from tempfile import TemporaryDirectory
# use redirect stdout to silence some of the load calls. If we're on an earlier
# python we need to roll out own. c.f stackoverflow.com/questions/4675728
try:
    from contextlib import redirect_stdout
except ImportError:
    from contextlib import contextmanager
    import sys
    @contextmanager
    def redirect_stdout(new_target):
        old_target, sys.stdout = sys.stdout, new_target # replace sys.stdout
        try:
            yield new_target # run some code with the replaced stdout
        finally:
            sys.stdout = old_target # restore to the previous value

def boolcheck(a):
    if a == 'True':
        return True
    if a == 'False':
        return False
    raise ValueError

def conv_kwargs(kwdict):
    for k, v in kwdict.items():
        v = v.split(',')
        for dtype in (int, float, boolcheck):
            try:
                v = [dtype(a) for a in v]
                if len(v) == 1: v = v[0]
                kwdict[k] = v
                break
            except:
                ValueError
                pass
    return kwdict

class Dumpdir(TemporaryDirectory):
    """Dump directory for xspp out, returns to cwd on exit"""
    def __init__(self, *args, **kwargs):
        TemporaryDirectory.__init__(self, *args, **kwargs)
        self.cwd = shutil.os.getcwd()

    def __enter__(self):
        shutil.os.chdir(self.name)
        return self

    def __exit__(self, exc, value, tb):
        shutil.os.chdir(self.cwd)
        TemporaryDirectory.__exit__(self, exc, value, tb)

class Toolbar(object):
    """Progress bar"""
    def __init__(self, steps, width=70, full_width=False):
        if full_width:
            width = steps
        self.width = min(width, steps)
        self.index = int(0)
        self.modulus = max((steps // self.width), 1)

    def __enter__(self):
        sys.stdout.write("[%s]" % (" " * int(self.width +4)))
        sys.stdout.flush()
        sys.stdout.write("\b" * int(self.width+ 5)) # return to start of line, after '['
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        sys.stdout.write('\n')

    def update(self):
        self.index += 1
        if (self.index % self.modulus) == 0:
            sys.stdout.write("-")
            sys.stdout.flush()
        lab = '{:04d}'.format(self.index)
        sys.stdout.write(lab)
        sys.stdout.flush()
        sys.stdout.write('\b'*4)
        sys.stdout.flush()

    def close(self):
        sys.stdout.write("\n")

class ToolbarList(Toolbar):
    """Updates toolbar as a list is processed."""
    def __init__(self, listobj, **kwargs):
        Toolbar.__init__(self, len(listobj), **kwargs) 
        self.list = listobj

    def __len__(self):
        return len(self.list)

    def __getitem__(self, key):
        return self.list[key]

    def __iter__(self):
        self.__enter__()
        self.index=0
        return self
        
    def __next__(self):
        if self.index == len(self.list):
            sys.stdout.write("\n")
            raise StopIteration
        self.update()
        return self.list[self.index-1]
            
class OpenFilename(object):
    """Context manager that opens a filename and closes it on exit, but

    does nothing for file-like objects. Will make sure the file is utf-8
    encoded, and try iso-8859-1 if it is not
    """
    def __init__(self, filename, *args, **kwargs):
        self.closing = kwargs.pop('closing', False)
        if isinstance(filename, str):
            self.filehandle = open(filename, *args, **kwargs)
            # make sure we're using the right encoding
            try:
                line = self.filehandle.readline()
                self.filehandle.seek(0)
            except UnicodeDecodeError:
                self.filehandle.close()
                self.filehandle = open(filename, *args, encoding='iso-8859-1', **kwargs)
            self.closing = True
        else:
            self.filehandle = filename

    def __enter__(self):
        return self.filehandle

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.closing:
            self.filehandle.close()
        return False

# decorator to have the toolbar update as part of the function call
def toolfunc(func):
    @wraps(func)
    def func_wrapper(toolbar, *args, **kwargs):
        try:
            toolbar.update()
        except AttributeError:
            pass
        return func(*args, **kwargs)
    return func_wrapper

# decorator to keep extraneous print statements from being issued
def chompstdout(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        with StringIO() as buf:
            with redirect_stdout(buf):
                out = func(*args, **kwargs)
        return out
    return func_wrapper

# decorator to read in files with various encodings
def isoload(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        try:
            out = func(*args, **kwargs)
        except UnicodeDecodeError:
            with open(args[0], encoding='iso-8859-1') as fin:
                newargs = tuple([fin,] + list(args[1:]) )
                out = func(*newargs, **kwargs)
        return out
    return func_wrapper

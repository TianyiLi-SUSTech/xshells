from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.widgets import Slider, Button, RadioButtons
from matplotlib.patches import Rectangle
from matplotlib.lines import Line2D

import numpy as np
from pandas import DataFrame, Series
import pandas as pd

from scipy.interpolate import griddata

CM = plt.cm.seismic
CM.set_bad('k', 1.0)
GS = GridSpec(1, 1, left=0.1, right=0.8, bottom=0.1, top=0.9)
CGS = GridSpec(1, 1, left=0.85, right=0.9, bottom=0.1, top=0.9)

class PivotContour():
    """Contour plot with options to plot over selectable independent variables"""
    def __init__(self, df, axiskeys, contourkeys, metakeys, fig=None, **kwargs):
        assert type(df) is DataFrame, "df must be a pandas DataFrame object"

        # set variables based on **kwargs
        self.method = kwargs.get('method', 'nearest')

        # split the dataframe into independent variables (the axes),
        # dependent variables (contour values), and metadata
        # (everything else)
        self.data_i = df.reindex(columns=axiskeys)
        self.data_d = df.reindex(columns=contourkeys)
        self.data_m = df.reindex(columns=metakeys)

        self.ndim = len(self.data_i.columns)

        # set up the plotting surfaces
        self.fig = fig
        if self.fig is None:
            self.fig = plt.figure()
        self.ax = self.fig.add_subplot(GS[0, 0])
        self.cax = self.fig.add_subplot(CGS[0, 0])

        self.nsliders = kwargs.get('nsliders', self.ndim-2)
        self.axinds = list(range(self.nsliders))

        self.xind = self.nsliders
        self.yind = self.nsliders + 1
        # set up the sliders
        sgs = GridSpec(2, 1,
                       left=0.1, right=0.9, wspace=0.2,
                       bottom=0.01, top=0.05, hspace=0.01)
        col = self.data_i.columns[0]
        self.slideax = self.fig.add_subplot(sgs[0, 0])
        self.slider = Slider(self.slideax,
                             col,
                             self.data_i[col].min(),
                             self.data_i[col].max(),)
        self.slider.on_changed(self.make_contour)
        self.slidebutton = (
            RadioButtons(
                self.fig.add_subplot(sgs[1, 0], axisbg='lightgoldenrodyellow'),
                self.data_i.columns)
        )
        self.slidebutton.on_clicked(lambda label: setattr(self, 'choice', self.data_i[label]))
        setattr(self, 'choice', self.data_i[self.data_i.columns[0]])

        for c in self.slidebutton.circles:
            c.center = (c.center[1], 0.5)
            c.height *= 6
        for l in self.slidebutton.labels:
            p = l.get_position()
            l.set_position((p[1], 0.5))
            l.set_horizontalalignment('center')

        # set up the buttons
        self.dButton = RadioButtons(
            self.fig.add_subplot(
                GridSpec(1, 1, left=0.92, right=0.98, bottom=0.1, top=0.9)[0, 0],
                axisbg='lightgoldenrodyellow'),
            self.data_d.columns)
        self.dButton.on_clicked(lambda label: setattr(self, 'ddat', self.data_d[label]))
        setattr(self, 'ddat', self.data_d[self.data_d.columns[0]])

        self.xButton = RadioButtons(
            self.fig.add_subplot(
                GridSpec(1, 1, left=0.1, right=0.8, bottom=0.92, top=0.98)[0, 0],
                axisbg='lightgoldenrodyellow'),
            self.data_i.columns)
        
        self.xButton.on_clicked(lambda label: setattr(self, 'xdat', self.data_i[label]))
        setattr(self, 'xdat', self.data_i[self.data_i.columns[1]])
        for c in self.xButton.circles:
            c.center = (c.center[1], 0.5)
            c.height *= 6
        for l in self.xButton.labels:
            p = l.get_position()
            l.set_position((p[1], 0.5))
            l.set_horizontalalignment('center')

        self.yButton = RadioButtons(
            self.fig.add_subplot(
                GridSpec(1, 1, left=0.02, right=0.08, bottom=0.1, top=0.9)[0, 0],
                axisbg='lightgoldenrodyellow'),
            self.data_i.columns)
        self.yButton.on_clicked(lambda label: setattr(self, 'ydat', self.data_i[label]))
        setattr(self, 'ydat', self.data_i[self.data_i.columns[2]])

        self.updateButton = Button(
            self.fig.add_subplot(
                GridSpec(1, 1, left=0.81, right=0.85, bottom=0.91, top=0.95)[0, 0]),
            'update')
        self.updateButton.on_clicked(self._set_axes_and_sliders)
        self._set_axes_and_sliders()
        self.fig.canvas.mpl_connect('pick_event', self.pick_datapoint)

    def _set_axes_and_sliders(self, *args):
        self.slider.disconnect_events()
        del self.slider
        self.slideax.clear()
        self.slider = Slider(self.slideax,
                             self.choice.name,
                             self.choice.min(), self.choice.max(),
                             color='grey')
        self.slider.on_changed(self.make_contour)
        for v in self.choice.unique():
            self.slideax.axvline(v, color='black')
        self.ax.set_xlabel(self.xdat.name)
        self.ax.set_ylabel(self.ydat.name)

    def _choose_from_column(self, *args):
        vals = self.choice.unique()
        val = vals[np.argmin(np.abs(vals-self.slider.val))]
        return self.choice == val

    def make_contour(self, *args, **kwargs):
        self.ax.clear()
        self.cax.clear()
        print(self.choice.name, self.xdat.name, self.ydat.name)
        # select data based on the slider value
        ind = self._choose_from_column()
        x = self.xdat[ind]
        y = self.ydat[ind]
        data_d = self.ddat[ind]
        # get the plotting range
        lev = np.linspace(data_d.min(), data_d.max(), 10)
        
        # if -(data_d.min()) < rng:
        #     rng = -(data_d.min())
        # lev = np.linspace(-rng/3, rng/3, 10)
        # lev = np.linspace(0, 1.0, 20)
        # get an index of valid values
        valid = data_d.notnull()
        invalid = data_d.isnull()

        # make the interpolated data
        nx, ny = kwargs.get('nx', 100), kwargs.get('ny', 100)
        xi = np.linspace(x.min(), x.max(), nx)
        yi = np.linspace(y.min(), y.max(), ny)
        grid = (xi[None, :], yi[:, None])

        dat = griddata((x[valid], y[valid]), data_d[valid],
                       grid, method=self.method)

        cont = self.ax.contourf(xi, yi, dat, lev, cmap=CM, extend='both')
        self.ax.contour(xi, yi, dat, np.r_[0.0], colors='black')
        cbar = plt.colorbar(cont, cax=self.cax)
        
        self.ax.plot(x[valid], y[valid], 'o', color='black', picker=5)
        self.ax.plot(x[invalid], y[invalid], 'd', color='pink', picker=5)
        for t in self.ax.yaxis.get_major_ticks():
            t.label1On = False
            t.label2On = True
            t.set_zorder(100)

    def pick_datapoint(self, event):
        if isinstance(event.artist, Line2D):
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind
            return np.take(xdata, ind)[0], np.take(ydata, ind)[0]
        return None

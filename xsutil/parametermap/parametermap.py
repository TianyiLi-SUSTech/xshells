from glob import glob
from os import sep
from subprocess import call

import numpy as np
import pandas as pd
from pandas import DataFrame, Series
from parametermap.pivot_contour import PivotContour
#from pivot_contour import PivotContour

from matplotlib import pyplot as plt
from xspar.xspar import parse_xspath, parse_xspar
from xsenergy.read_dataset import read_table
import re

class ParameterMap(PivotContour):
    """An interactive database for results of a sweep of parameters.

    This is initialized on a list of xshells.par files and needs to be
    provided with methods for determining the swept parameters,
    dependent variables, and alternate parameters (if wanted).

    The parameter parser function must be of the form
    dict = parserfcn(filepath)
    where the filepath refers to an xshells.par
    file, and the return dictionary contains the parameter names and
    their values for each path (either the path itself, or the
    xshells.par file can provide these values)

    The dvars parser function must accept a dataframe and 
    return a pandas dataframe object

    The alternate parameters functions (the keyword argument buckingham)
    must accept a pandas dataframe object and return an iterator
    containing keys and pandas Series objects

    The parameters keyword can be set to an iterable to limit the
    parameters that can be selected during operation.
    """
    def __init__(self, flist, parser, dvars, buckingham=None, parameters=[],
                 pargs=(), pkwargs={}, dargs=(), dkwargs={}, **kwargs):
        print(kwargs)
        self.plot_energy = kwargs.get('plot_energy', True)
        self.script = kwargs.get('script')
        # get a list of directories from the file list
        dlist = [f[:f.rindex('xshells.par')] for f in flist]
        # first run through the directories and get the desired parameters
        data = DataFrame([parser(f, *pargs, **pkwargs) for f in flist], index=dlist)
        # add an entry for the jobname
        data['job'] = Series([parse_xspar(f, 'job') for f in flist],
                             index=dlist)
        # next determine alternative parameters if desired
        if buckingham is not None:
            for c, ser in buckingham(data):
                data[c] = ser

        # make the list of independent and dependent variables
        if not parameters:
            parameters = list(data.columns)
            metadata = [parameters.pop(parameters.index('job')),]
        else:
            metadata = ['job',]

        # next call dependentfcn on each directory.
        dep = DataFrame({d: {k: v for k, v in dvars(d, j)} for d, j in data.job.iterkv()})
        dep = dep.T
        dependent = list(dep.columns)
        for key, val in dep.iteritems():
            data[key] = val
        # for fcn in dvars:
        #     data[fcn.__name__] =Series([fcn(d, data.job[d], *dargs, **dkwargs) for d in dlist],
        #                                index=dlist)

        PivotContour.__init__(self, data, parameters, dependent, metadata,
                              **kwargs)
        self.picks = pd.Index([])

    def pick_datapoint(self, event):
        """chooses the indices that match the event point.

        If the point is right-clicked, an index is built up containing
        the selected data points. Upon the first left-click, if there is
        an analysis script attached to the ParameterMap object, it is
        called on the directories selected. Otherwise the directories
        are printed to stdout.
        """
        inds = self._choose_from_column()
        xdat = self.xdat[inds]
        ydat = self.ydat[inds]
        x, y = PivotContour.pick_datapoint(self, event)
        # find the point specified and add it to the index
        xreduced = xdat.ix[ydat == y]
        self.picks = self.picks.union(xreduced.index[xreduced == x])
        print(self.picks)
        # find the point chosen, then do some voodoo to actually find
        # the directory at the specified index, then call it a list so
        # that it can be appended to the existing directory list
        if event.mouseevent.button == 1:
            if self.plot_energy:
                for d in self.picks:
                    eng = d+'energy.'+self.data_m.job[d]
                    dat = read_table(eng, heirarchy=True)
                    dat.m.plot().set_yscale('log')
                plt.show()
            if self.script is not None:
                call(['bash', self.script] + list(self.picks))
            else:
                print(self.data_i.ix[self.picks])
                print(self.data_d.ix[self.picks])
            self.picks = pd.Index([])

# xslist = glob('/home/kaplane/stuff/*/xshells.par')
# def parse_xspath(xspath, regex=r'_(?P<param>\D*)(?P<val>[-e.\d]*)'):
#     '''parses filepaths

#     The filepaths must end with
#     [label]_parval1_parval2..._parvalN. Where each parval is a
#     combination of a string and a number. Alternate regexprs can be
#     provided
#     '''
#     assert xspath.rfind('xshells.par') != -1, 'must provide xshells.par files'
#     # replace doubled separators with single before the split
#     d = xspath.replace(sep*2,sep).split(sep)[-2] 
#     dictionary = {match.group('param'): float(match.group('val')) for match in re.finditer(regex, d)}
#     # pull the jobname from the xshells.par file
#     dictionary.update(parse_xspar(xspath, 'job', return_dict=True))
#     return dictionary

# def noise(directory, jobname):
#     yield 'noise1', np.random.random(1)[0]
#     yield 'noise2', np.random.random(2)[0]

# def bham(dframe):
#     yield 'log10Pm', dframe.Pm.apply(np.log10)
#     yield 'Re', dframe.Po * dframe.Pm

# pmap = ParameterMap(xslist, parse_xspath, noise, buckingham=bham, parameters=['log10Pm', 'Po', 'Re', 'ang'], method='nearest')
# plt.show()

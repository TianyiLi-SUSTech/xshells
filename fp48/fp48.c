/*
 * Copyright (c) 2019 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/** FP48: a simple library to compress scientific double precision floating point data.
 * For spatially correlated data, there are number of compression methods. See e.g. [1].
 * But for weakly correlated data, for instance data in the spectral (Fourier transform),
 * domain, this method does not lead to significant compression ratios.
 * 
 * FP48 simply uses another floating point format, inspired by the IEEE754 standards for
 * 64 bits and 32 bits floating point numbers (double and float respectively) [2].
 * One FP48 number uses 48 bits of storage (40 bits of mantissa, 7 bits of exponent and
 * 1 sign bit).
 * It has thus MUCH more accuracy than fp32 (float), and only about 1e-4 less than 
 * fp64 (double).
 * So that makes it a lossy compression algorithm.
 * However, when dealing with numerical simulation data, the least significant 12 bits
 * or so are often numerical noise, so that discarding them causes little harm.
 * 7 bits of exponent is not much (less than fp32!), but one can shift the exponent
 * to adapt to the data (aka scaling).
 * 
 * One advantage of FP48 is that it has a deterministic compression ratio of 0.75.
 * Observed speed is several GB/s, about one third to one half of the memcpy speed.
 * 
 * Caveats: 
 *    - assumes little-endian architecture (like intel x86_64)
 *    - round towards zero is performed when packing.
 *    - infinities are packed to NaN (with sign).
 *    - assumes loading unaligned data is safe (undefined behaviour by the standards).
 * 
 * [1] https://computation.llnl.gov/projects/floating-point-compression/
 * [2] https://wikipedia.org/wiki/IEEE_754
 */

/* custom FP48 format (precision: 1e-12, range = 1e-19 to 1e19)
1 sign bit
7 exponent bits
40 mantissa bits

=> zeros are packed to zeros (with sign)
=> nan and inf are both packed to nan (with sign). infinities are thus lost...
=> optional exponent shift, to shift the range of representable numbers.
=> assumes little endian storage (intel).
=> assumes unaligned loads and stores of 64 bit data is safe.

TODO: 
 - pack and unpack should translate infinities.
 - pack with correct rounding (instead of truncating).
*/

#include <stdio.h>
#include "fp48.h"

typedef unsigned long long uint64;		// 64 bit integer

// Some bitmasks for internal use:
const uint64 fp48_sign_msk = 0x0000800000000000ull;
const uint64 fp48_expo_msk = 0x00007F0000000000ull;
const uint64 fp48_mant_msk = 0x000000FFFFFFFFFFull;
const uint64 fp48_exma_msk = 0x00007FFFFFFFFFFFull;

const uint64 fp64_sign_msk = 0x8000000000000000ull;
const uint64 fp64_expo_msk = 0x7FF0000000000000ull;
const uint64 fp64_exma_msk = 0x7FFFFFFFFFFFFFFFull;

const int exp0 = 959;		// allows representation from 1e-19 to 1e19.

#ifdef __AVX2__

#define FP48_X4
//#warning "using AVX2"
#include <immintrin.h>
// note: AVX2 has been introduced in the intel Haswell architecture (end of 2013).

typedef __m256i uint64x4;

inline static uint64x4 dbl_to_fp48_x4(uint64x4 d, const uint64 exp_shift64)
{
	uint64x4 vexpshift = _mm256_set1_epi64x(exp_shift64 << 12);

	uint64x4 s = _mm256_and_si256(_mm256_set1_epi64x(fp64_sign_msk), d);		// sign bit
	uint64x4 x = _mm256_andnot_si256(_mm256_set1_epi64x(fp64_sign_msk), d);		// exponent & mantissa

	// check for exponent within acceptable bounds:
	x = _mm256_sub_epi64(x, vexpshift);

	uint64x4 msk  = _mm256_cmpgt_epi64(_mm256_set1_epi64x(0), x);		// 0 > x
	uint64x4 msk2 = _mm256_cmpgt_epi64(x, _mm256_set1_epi64x(fp48_exma_msk<<12));	// all bits set to one if x is NOT representable in FP48.
	x = _mm256_slli_epi64(x,4);
	s = _mm256_or_si256(msk2, s);			// set to nan
	x = _mm256_andnot_si256(msk, x);		// zero out when x<0
	x = _mm256_or_si256(s, x);			// set sign and/or nan

	x = _mm256_shuffle_epi8(x, _mm256_setr_epi8(2,3,4,5,6,7,  10,11,12,13,14,15,  -1,-1,-1,-1,
												2,3,4,5,6,7,  10,11,12,13,14,15,  -1,-1,-1,-1 ) );		// pack fp48 contiguously, in each 128bit lane
	x = _mm256_permutevar8x32_epi32(x, _mm256_setr_epi32(0,1,2,4, 5,6,7,7) );		// put data in the correct 64bit lane, crossing 128bit lanes.	
	return x;
}

/// \internal converts two fp48 values loaded contiguously in the lower part of x into two double precision float.
inline static uint64x4 fp48_to_dbl_x4(uint64x4 x, const uint64 exp_shift64)
{
	uint64x4 vexpshift = _mm256_set1_epi64x(exp_shift64 << 12);

	// the following instruction is costly but required to put everything in the correct lane.
	// it is sometimes not worth it, and the 128 bit version may run as fast or even faster.
	x = _mm256_permutevar8x32_epi32(x, _mm256_setr_epi32(0,1,1,2, 3,4,4,5) );		// put data in the correct 64bit lane, crossing 128bit lanes.

	x = _mm256_shuffle_epi8(x, _mm256_setr_epi8(-1,-1,0,1,2,3,4,5,  -1,-1,10,11,12,13,14,15,
												-1,-1,0,1,2,3,4,5,  -1,-1,10,11,12,13,14,15));		// align sign with fp64.

	uint64x4 s = _mm256_and_si256(_mm256_set1_epi64x(fp64_sign_msk), x);	// sign bit
	x = _mm256_andnot_si256(_mm256_set1_epi64x(fp64_sign_msk), x);		// remove sign bit

	uint64x4 msk2 = _mm256_cmpeq_epi64(x, _mm256_set1_epi64x(fp48_exma_msk<<16));	// all bits set to one if equal to NaN.
	uint64x4 msk  = _mm256_cmpeq_epi64(x, _mm256_set1_epi64x(0));	// all bits set to one if equal to zero.

	x = _mm256_add_epi64(_mm256_srli_epi64(x,4), vexpshift);
	s = _mm256_or_si256(s, msk2);		// s is the sign or NaN mask
	x = _mm256_andnot_si256(msk, x);	// zero the result where it should be zero.
	x = _mm256_or_si256( x, s );		// set the correct sign (or NaN).
	return x;
}

#elif defined( __SSE4_2__ )

#define FP48_X2
//#warning "using SSE4.2"
#include <smmintrin.h>
// note: SSE4.2 has been introduced in the intel Nehalem architecture (end of 2008).

typedef __m128i uint64x2;

/// \internal converts two fp48 values loaded contiguously in the lower part of x into two double precision float.
/// requires SSE4.2 for the _mm_cmpgt_epi64 instructions.
inline static uint64x2 dbl_to_fp48_x2(uint64x2 d, const uint64 exp_shift64)
{
	uint64x2 vexpshift = _mm_set1_epi64x(exp_shift64 << 12);

	uint64x2 s = _mm_and_si128(_mm_set1_epi64x(fp64_sign_msk), d);		// sign bit
	uint64x2 x = _mm_andnot_si128(_mm_set1_epi64x(fp64_sign_msk), d);;		// exponent & mantissa

	// check for exponent within acceptable bounds:
	x = _mm_sub_epi64(x, vexpshift);

	uint64x2 msk  = _mm_cmpgt_epi64(_mm_set1_epi64x(0), x);		// 0 > x
	uint64x2 msk2 = _mm_cmpgt_epi64(x, _mm_set1_epi64x(fp48_exma_msk<<12));	// all bits set to one if x is NOT representable in FP48.
	x = _mm_slli_epi64(x,4);
	s = _mm_or_si128(msk2, s);			// set to nan
	x = _mm_andnot_si128(msk, x);		// zero out when x<0
	x = _mm_or_si128(s, x);			// set sign and/or nan

	return 	_mm_shuffle_epi8(x, _mm_setr_epi8(2,3,4,5,6,7,  10,11,12,13,14,15,  -1,-1,-1,-1));		// pack fp48 contiguously.
}

/// \internal converts two fp48 values loaded contiguously in the lower part of x into two double precision float.
/// requires SSE4.1 for _mm_cmpeq_epi64
inline static uint64x2 fp48_to_dbl_x2(uint64x2 x, const uint64 exp_shift64)
{
	uint64x2 vexpshift = _mm_set1_epi64x(exp_shift64 << 12);

	x = _mm_shuffle_epi8(x, _mm_setr_epi8(-1,-1,0,1,2,3,4,5, -1,-1,6,7,8,9,10,11));		// put each fp48 into lower and upper part of xmm

	uint64x2 s = _mm_and_si128(_mm_set1_epi64x(fp64_sign_msk), x);	// sign bit
	x = _mm_andnot_si128(_mm_set1_epi64x(fp64_sign_msk), x);		// remove sign bit

	uint64x2 msk2 = _mm_cmpeq_epi64(x, _mm_set1_epi64x(fp48_exma_msk<<16));	// all bits set to one if x is NaN.
	uint64x2 msk  = _mm_cmpeq_epi64(x, _mm_set1_epi64x(0));	// all bits set to one if x is zero.

	x = _mm_add_epi64(_mm_srli_epi64(x,4), vexpshift);
	s = _mm_or_si128(s, msk2);		// s is the sign or NaN mask
	x = _mm_andnot_si128(msk, x);	// zero the result where it should be zero.
	x = _mm_or_si128( x, s );		// set the correct sign (or NaN).
	return x;
}

#endif



inline static uint64 dbl_to_fp48(uint64 d, const uint64 exp_shift64)
{
	uint64 s = d & fp64_sign_msk;		// sign bit
	uint64 a = d & fp64_exma_msk;		// exponent & mantissa
	// check for exponent within acceptable bounds:
	long long r = (a>>12);
	s >>= 16;		// sign bit of fp48
	if ((r -= exp_shift64) < 0) {
		r = 0;
	}
	else if (r >= (128LL<<40))		// not representable in FP48: mark as nan (keep sign).
		r = fp48_exma_msk;
	return s | r;
}

/// packs n double precision numbers (8 bytes each) to n fp48 numbers (6 bytes each).
/// exp_shift can be adjusted to rescale the data in the range 1e-19 to 1e19.  (default: 0 = no rescaling).
/// Optimal exp_shift requires knowledge of the data, and can be computed by calling fp48_exp_shift.
void fp48_pack(const double* dbl, char* fp48, long n, int exp_shift)
{
	if (n<=0) return;	// nothing to do.
	const uint64 exp_shift64 = ((uint64)exp_shift + exp0) << 40;
	uint64 x;
	long i=n-1;
	#ifdef FP48_X4
	for (; i>3; i-=4) {
		__m256i vx = _mm256_loadu_si256( (__m256i*)dbl );
		vx = dbl_to_fp48_x4(vx, exp_shift64);
		_mm256_storeu_si256( (__m256i*)fp48, vx);
		fp48 += 24;
		dbl += 4;
	}
	#elif defined( FP48_X2 )
	for (; i>1; i-=2) {
		__m128i vx = _mm_loadu_si128( (__m128i*)dbl );
		vx = dbl_to_fp48_x2(vx, exp_shift64);
		_mm_storeu_si128( (__m128i*)fp48, vx);
		fp48 += 12;
		dbl += 2;
	}
	#endif
	while(1) {
		// TODO: use integer math to round correctly before packing.
		x = *((uint64*)dbl);
		x = dbl_to_fp48(x, exp_shift64);
		if (--i < 0) break;	// this test prevents overflow
		*(uint64*)fp48 = x;		// WARNING! this writes 64 bits. Can overflow the array if no padding is used.
		fp48 += 6;
		dbl ++;
	}
	// the last value is written without overflow.
	*(int*)fp48 = (int) x;
	*(short*)(fp48+4) = (short) (x>>32);
}

/* UNPACKING */

/// \internal converts the fp48 value in the lower part of x into a double precision float.
inline static uint64 fp48_to_dbl(uint64 x, const uint64 exp_shift64)
{
	uint64 d = x<<16;	// if zero or nan, this is the unpacked value.
	if (
		(x &= fp48_exma_msk) 	// remove sign bit, is the value non-zero ?
			&& (x != fp48_exma_msk)
		) {		// is the value not a NaN ?
			d = (d & fp64_sign_msk)  |  ((x + exp_shift64) << 12);
	}
	return d;
}

/// unpacks n fp48 numbers (6 bytes each) to n double precision numbers (8 bytes each).
/// exp_shift must be the same as the one used to pack (default: 0).
void fp48_unpack(const char* fp48, double* dbl, long n, int exp_shift)
{
	if (n<=0) return;
	const uint64 exp_shift64 = ((uint64)exp_shift + exp0) << 40;
	uint64 x;
	long i = n-1;
	#ifdef FP48_X4
	for (; i>3; i-=4) {		// convert 4 by 4, leave out the last 4.
	 	__m256i vx = _mm256_loadu_si256( (__m256i*)fp48 );		// WARNING! this reads 256 bits. We leave out the last 4 elements to avoid overflow.
		fp48 += 24;
		vx = fp48_to_dbl_x4(vx, exp_shift64);
		 _mm256_storeu_si256( (__m256i*)dbl, vx);
		dbl += 4;
	}
	#elif defined( FP48_X2 )
	for (; i>1; i-=2) {		// convert 2 by 2, leave out the last 2.
	 	__m128i vx = _mm_loadu_si128( (__m128i*)fp48 );		// WARNING! this reads 128 bits. We leave out the last 2 elements to avoid overflow.
		fp48 += 12;
		vx = fp48_to_dbl_x2(vx, exp_shift64);
		_mm_storeu_si128( (__m128i*)dbl, vx);
		dbl += 2;
	}
	#endif
	for (; i>0; i--) {		// leave out the last one
	 	x = *((uint64*)fp48);		// WARNING! this reads 64 bits. We leave out the last element to avoid overflow.
		fp48 += 6;
		* (uint64*)dbl++ = fp48_to_dbl(x, exp_shift64);
	}
	// the last element is read without overflow
	x = (uint64) (*((unsigned*)fp48)) | (( (uint64) *((unsigned short*)(fp48+4))) << 32 );
	* (uint64*)dbl = fp48_to_dbl(x, exp_shift64);
}

/* AUXILIARY */

/// returns minimum and maximum exponents of an array of double precision numbers.
/// if exp_max - exp_min <= 127, then the data can be packed without any underflows / flush to zero.
int fp48_exp_range(const double* dbl, long n, int* exp_min, int* exp_max)
{
	int min_exp = 0x7FE;	// initial values 
	int max_exp = 0x001;	// chosen carefully
	long i = 0;

#ifdef __SSE4_1__
	#ifdef __AVX2__
	// FULLY WORKING
	__m256i vmin_exp = _mm256_set1_epi16( min_exp << 4 );
	__m256i vmax_exp = _mm256_set1_epi16( max_exp << 4 );
	for (; i<n-15; i+=16) {		// pack 16 exponents into 1 ymm 256-bit register
		__m256i vx = _mm256_loadu_si256( (__m256i*)dbl );
		__m256i vx2 = _mm256_loadu_si256( (__m256i*)(dbl+4) );
		__m256i vx3 = _mm256_loadu_si256( (__m256i*)(dbl+8) );
		__m256i vx4 = _mm256_loadu_si256( (__m256i*)(dbl+12) );
		vx = _mm256_castps_si256( _mm256_shuffle_ps(_mm256_castsi256_ps(vx), _mm256_castsi256_ps(vx2), 0xDD) );
		vx2 = _mm256_castps_si256( _mm256_shuffle_ps(_mm256_castsi256_ps(vx3), _mm256_castsi256_ps(vx4), 0xDD) );
		vx = _mm256_srli_epi32(vx, 16);
		vx = _mm256_blend_epi16(vx, vx2, 0xAA);

		dbl += 16;
		vx = _mm256_and_si256(vx, _mm256_set1_epi16(fp64_expo_msk>>48));		// keep exponent only
		__m256i sx = _mm256_add_epi16(vx, _mm256_set1_epi16(0x0010));	// nan or inf become negative; zero becomes 1.
		__m256i ux = _mm256_sub_epi16(vx, _mm256_set1_epi16(0x0010));	// zero becomes < 0; nan or inf become 0x7FE << 20
		vmax_exp = _mm256_max_epi16(vmax_exp, sx);
		vmin_exp = _mm256_min_epu16(vmin_exp, ux);
	}
	// reduction step to 128 bits
	__m128i vmax_exp128 = _mm_max_epi16(_mm256_castsi256_si128(vmax_exp),  _mm256_extractf128_si256(vmax_exp, 1));
	__m128i vmin_exp128 = _mm_min_epi16(_mm256_castsi256_si128(vmin_exp),  _mm256_extractf128_si256(vmin_exp, 1));

	#else
	// PASS TEST
	__m128i vmin_exp128 = _mm_set1_epi16( min_exp << 4 );
	__m128i vmax_exp128 = _mm_set1_epi16( max_exp << 4 );
	for (; i<n-7; i+=8) {		// pack 8 exponents into 1 xmm 128-bit register
		__m128i vx = _mm_loadu_si128( (__m128i*)dbl );
		__m128i vx2 = _mm_loadu_si128( (__m128i*)(dbl+2) );
		__m128i vx3 = _mm_loadu_si128( (__m128i*)(dbl+4) );
		__m128i vx4 = _mm_loadu_si128( (__m128i*)(dbl+6) );
		vx = _mm_castps_si128( _mm_shuffle_ps(_mm_castsi128_ps(vx), _mm_castsi128_ps(vx2), 0xDD) );
		vx3 = _mm_castps_si128( _mm_shuffle_ps(_mm_castsi128_ps(vx3), _mm_castsi128_ps(vx4), 0xDD) );
		vx = _mm_srli_epi32(vx, 16);
		vx = _mm_blend_epi16(vx, vx3, 0xAA);

		dbl += 8;
		vx = _mm_and_si128(vx, _mm_set1_epi16(fp64_expo_msk>>48));	// keep exponent only
		__m128i sx = _mm_add_epi16(vx, _mm_set1_epi16(0x0010));	// nan or inf become negative; zero becomes 1.
		__m128i ux = _mm_sub_epi16(vx, _mm_set1_epi16(0x0010));	// zero becomes < 0; nan or inf become 0x7FE << 20
		vmax_exp128 = _mm_max_epi16(sx, vmax_exp128);
		vmin_exp128 = _mm_min_epu16(ux, vmin_exp128);
	}
	#endif
	// common reduction step:
	vmax_exp128 = _mm_max_epi16(vmax_exp128, _mm_slli_epi32(vmax_exp128, 16));		// only the high word has the correct value
	vmin_exp128 = _mm_min_epi16(vmin_exp128, _mm_slli_epi32(vmin_exp128, 16));		// only the high word has the correct value
	vmax_exp128 = _mm_max_epi16(vmax_exp128, _mm_shuffle_epi32(vmax_exp128, 0x4E) );
	vmin_exp128 = _mm_min_epi16(vmin_exp128, _mm_shuffle_epi32(vmin_exp128, 0x4E) );
	vmax_exp128 = _mm_max_epi16(vmax_exp128, _mm_shuffle_epi32(vmax_exp128, 0xB1) );
	vmin_exp128 = _mm_min_epi16(vmin_exp128, _mm_shuffle_epi32(vmin_exp128, 0xB1) );
	max_exp = _mm_cvtsi128_si32(vmax_exp128) >> 20;
	min_exp = _mm_cvtsi128_si32(vmin_exp128) >> 20;
	max_exp -= 1;
	min_exp += 1;
	
#endif

	if (i<n) {
		// relying on some undefined behaviour (x86_64)
		uint64 min_exp64 = ((uint64) min_exp) << 52;
		long long max_exp64 = ((uint64) max_exp) << 52;
		do {
			uint64 x = ((uint64*)dbl)[i++];
			x &= fp64_expo_msk;		// keep exponent only
			long long sx = x + 0x0010000000000000ll;	// nan or inf become negative; zero becomes 1.
			long long ux = x - 0x0010000000000000ll;	// zero becomes < 0; nan or inf become 0x7FE << 52
			if (sx > max_exp64) max_exp64 = x;		// signed compare ignores nan or inf.
			if ((uint64)ux < (uint64)min_exp64) min_exp64 = x;		// unsigned compare ignores 0 or denormal.
		} while (i<n);
		min_exp = min_exp64 >> 52;
		max_exp = max_exp64 >> 52;
	}

	*exp_min = min_exp;
	*exp_max = max_exp;
	//printf("min exp=%d, max_exp=%d\n", *exp_min - exp0, *exp_max-exp0);
	return (max_exp - min_exp <= 127);		// true if the range is compatible with FP48 without flush to zero.

/*	// ISO-C
	int min_exp = 0xFFF;
	int max_exp = 0;
	long i = 0;
	for (; i<n; i++) {
		uint64 x = ((uint64*)dbl)[i];
		x &= fp64_expo_msk;		// keep exponent only
		x &= fp64_expo_msk;		// keep exponent only
		if ((x) && (x != fp64_expo_msk)) {	// if not zero, denormal, nan or inf
			int exp = x>>52;		// exponent.
			if (exp > max_exp) max_exp = exp;
			if (exp < min_exp) min_exp = exp;
		}
	}
	*exp_min = min_exp;
	*exp_max = max_exp;
	return (max_exp - min_exp <= 127);		// true if the range is compatible with FP48 without flush to zero.
*/
}

/// compute the optimal exponent shift exp_shift to avoid NaNs.
/// returns the number of produced underflows (zeros).
int fp48_exp_shift(const double* dbl, long n, int* exp_shift)
{
	int min_exp, max_exp;
	int shift = 0;
	fp48_exp_range(dbl, n, &min_exp, &max_exp);

	if (max_exp - min_exp <= 127) {		// easy !
		if ((max_exp - exp0 > 127) || (min_exp - exp0 < 0)) {		// not compatible with zero shfit
			int margin = 127 - (max_exp - min_exp);		// > 0
			shift = min_exp - margin/2 - exp0;	// (max_exp + min_exp)/2 - 63 - exp0;
		} else shift = 0;			// compatible with zero shift
		*exp_shift = shift;
		return 0;		// no underflows.
	}

	printf("FP48 INFO slow mode\n");
	// there will be underflows, count them.
	shift = max_exp - 127;		// allow to represent the largest exponent
	*exp_shift = shift - exp0;
	int nans = 0;
	int underflows = 0;
	for (long i=0; i<n; i++) {
		uint64 x = ((uint64*)dbl)[i];
		x &= fp64_expo_msk;		// keep only the exponent.
		if (x) {	// don't count zeros or denormals.
			int exp = (x>>52) - shift;	// shifted exponent.
			if (exp > 127) nans++;			// should not happen
			if (exp < 0) underflows++;
		}
	}
	if (nans) printf(" [fp48_exp_shift] NaNs should not happen !\n");
	return underflows;
}

/* HIGHER-LEVEL */

/// same as fp48_pack, but computes the optimal exponent shift and stores it at the begining of fp48.
/// returns a pointer past the end of the written data.
char* fp48_write_stream(const double* dbl, char* fp48, long n)
{
	if (n<0) return fp48;

	int exp_shift;
	int underflows = fp48_exp_shift(dbl, n, &exp_shift);	// parse data to extract optimal exponent shift

	*(short*)fp48 = (short) exp_shift;		// write the exponent shift at the start of the stream.
	fp48_pack(dbl, fp48+2, n, exp_shift);	// write packed data.
	return fp48 + 2 + 6*n;
}

/// same as fp48_unpack, but reads the exponent shift at the begining of fp48.
/// returns a pointer past the end of the read data.
const char* fp48_read_stream(const char* fp48, double* dbl, long n)
{
	if (n<0) return fp48;

	int exp_shift = *(short*) fp48;			// read the exponent shift of the stream
	fp48_unpack(fp48+2, dbl, n, exp_shift);	// unpack data
	return fp48 + 2 + 6*n;
}

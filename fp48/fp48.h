/*
 * Copyright (c) 2019 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/** FP48: a simple library to compress scientific double precision floating point data.
 * For spatially correlated data, there are number of compression methods. See e.g. [1].
 * But for weakly correlated data, for instance data in the spectral (Fourier transform),
 * domain, this method does not lead to significant compression ratios.
 * 
 * FP48 simply uses another floating point format, inspired by the IEEE754 standards for
 * 64 bits and 32 bits floating point numbers (double and float respectively) [2].
 * One FP48 number uses 48 bits of storage (40 bits of mantissa, 7 bits of exponent and
 * 1 sign bit).
 * It has thus MUCH more accuracy than fp32 (float), and only about 1e-4 less than 
 * fp64 (double).
 * So that makes it a lossy compression algorithm.
 * However, when dealing with numerical simulation data, the least significant 12 bits
 * or so are often numerical noise, so that discarding them causes little harm.
 * 7 bits of exponent is not much (less than fp32!), but one can shift the exponent
 * to adapt to the data (aka scaling).
 * 
 * One advantage of FP48 is that it has a deterministic compression ratio of 0.75.
 * Observed speed is several GB/s, about one third to one half of the memcpy speed.
 * 
 * Caveats: 
 *    - assumes little-endian architecture (like intel x86_64)
 *    - round towards zero is performed when packing.
 *    - infinities are packed to NaN (with sign).
 *    - assumes loading unaligned data is safe (undefined behaviour by the standards).
 * 
 * [1] https://computation.llnl.gov/projects/floating-point-compression/
 * [2] https://wikipedia.org/wiki/IEEE_754
 */

/* custom FP48 format (precision: 1e-12, range = 1e-19 to 1e19)
1 sign bit
7 exponent bits
40 mantissa bits

=> zeros are packed to zeros (with sign)
=> nan and inf are both packed to nan (with sign). infinities are thus lost...
=> optional exponent shift, to shift the range of representable numbers.
=> assumes little endian storage (intel).
=> assumes unaligned loads and stores of 64 bit data is safe.

TODO: 
 - pack and unpack should translate infinities.
 - pack with correct rounding (instead of truncating).
*/

/* PACKING */

/// packs n double precision numbers (8 bytes each) to n fp48 numbers (6 bytes each).
/// exp_shift can be adjusted to rescale the data in the range 1e-19 to 1e19.  (default: 0 = no rescaling).
/// Optimal exp_shift requires knowledge of the data, and can be computed by calling fp48_exp_shift.
void fp48_pack(const double* dbl, char* fp48, long n, int exp_shift);

/* UNPACKING */

/// unpacks n fp48 numbers (6 bytes each) to n double precision numbers (8 bytes each).
/// exp_shift must be the same as the one used to pack (default: 0).
void fp48_unpack(const char* fp48, double* dbl, long n, int exp_shift);

/* AUXILIARY */

/// returns minimum and maximum exponents of an array of double precision numbers.
/// if exp_max - exp_min <= 127, then the data can be packed without any underflows / flush to zero.
int fp48_exp_range(const double* dbl, long n, int* exp_min, int* exp_max);

/// compute the optimal exponent shift exp_shift to avoid NaNs.
/// returns the number of produced underflows (zeros).
int fp48_exp_shift(const double* dbl, long n, int* exp_shift);

/* HIGHER-LEVEL */

/// same as fp48_pack, but computes the optimal exponent shift and stores it at the begining of fp48.
/// returns a pointer past the end of the written data.
char* fp48_write_stream(const double* dbl, char* fp48, long n);

/// same as fp48_unpack, but reads the exponent shift at the begining of fp48.
/// returns a pointer past the end of the read data.
const char* fp48_read_stream(const char* fp48, double* dbl, long n);


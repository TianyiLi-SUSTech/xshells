/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file qg2sh.cpp
/// Extrapolates QG-2D data to 3D spherical harmonics.

#define DEB printf("%s:%u pass\n", __FILE__, __LINE__)
//#define DEB (0)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// FFTW derivative d/dx = ik	(pas de moins !)
#include <fftw3.h>
#include <shtns.h>
#include "grid.cpp"
#include "xshells_spectral.cpp"
#include "xshells_spatial.cpp"
#include "xshells_io.cpp"

double *s;					///< radii in cylindrical space.
double *s_1;
struct TriDiag *Gs, *Ls;	///< Grad, Laplacian, cylindrical space.
long int NS;				///< number of radial shells, cylindrical space.
double pi = M_PI;

double beta = 0.;			///< 0 = don't correct divergence-free flow. 1 = beta-correction.
long int fill = 0;			///< output data everywhere if fill=1. Only fluid domain if not.
long int nobc = 0;			///< 0 = non-penetration boundary condition.
long int phirev = 0;		///< 1 = reverses the phi orientation (psi data of A.Fournier)
long int irmin, irmax;


/// write 2D vector field
void write_2D(const char *fn, double *us, double *up, int ns, int np)
{
	FILE *fp;
	long int i,j,k;

	fp = fopen(fn,"w");
	fprintf(fp,"%% [QG2SH] 2D psi function (r,phi) in cylindrical coordinates. first column is r.\n");
	for (i=0;i<ns;i++) {
		fprintf(fp,"\n%.6g ",s[i]);		// first row = radius
		for(j=0; j<np; j++) {
			k = j*ns +i;
			fprintf(fp,"%.6g %.6g 0  ",us[k],up[k]);	// data
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}

/*
/// 2D scalar field to 3D scalar field (spatial).
/// phi coordinate must be the same.
/// Some spatial filtering in radius/theta is needed !!!
void scalar_2D_to_3D(double *s, int ns, int incs, int np, int incp, double *a2d, double **a3d)
{
	double rr, sr,zr;
	double alp0, alp1;
	int ir,it,ip, is, nlat_2;

	nlat_2 = (NLAT+1)/2;		// compute only on one half of the sphere, then copy other half.

	for (ir=0; ir<NR; ir++) {
		rr = r[ir];
		is = 0;
		for (it=0; it<nlat_2; it++)
		{
			sr = rr*st[it];	zr = rr*ct[it];
			if ((sr >= s[0])&&(sr <= s[ns])) {
				while (s[is+1] < sr) is++;
//				if (is+1 > ns) printf("is overflow %d\n",is);
				alp1 = (sr-s[is])/(s[is+1]-s[is]);
				alp0 = 1. - alp1;
				for (ip=0; ip<NPHI; ip++) {
					a3d[ir][ip*NLAT + it] = alp0 * a2d[is*incs + ip*incp]  + alp1 * a2d[(is+1)*incs + ip*incp];
				}
			} else {
				a3d[ir][ip*NLAT + it] = 0.;
			}
		}
		for (ip=0; ip<NPHI; ip++) {
			for (it=nlat_2; it<NLAT; it++)	// copy symmetric data (scalar)
				a3d[ir][ip*NLAT +it] = a3d[ir][ip*NLAT + (NLAT-1-it)];
		}
	}
}
*/

/// 2D velocity field to 3D velocity field (spatial).
/// Some spatial filtering in radius/theta is needed !!!
void vect_QG_to_3D(int ns, int incs, int incp, double *vs, double *vp, struct VectField *V, double delta)
{
	double rr, sr,zr;
	double alp0, alp1;
	double z_eta = 0.0;
	double vss, vpp, vzz;
	double ds;
	int nlow, ntot;
	int ir,it,ip, is, nlat_2;
	double dr[NR];

	ir = 0;
		dr[ir] = (r[ir+1]-r[ir])*0.5;
	for (ir=1; ir< NR-1; ir++)
		dr[ir] = (r[ir+1]-r[ir-1])*0.5;
	ir = NR-1;
		dr[ir] = (r[ir]-r[ir-1])*0.5;

	nlat_2 = (NLAT+1)/2;		// compute only on one half of the sphere, then copy other half.
	nlow = 0;
	for (ir=irmin; ir<=irmax; ir++) {
		rr = r[ir];
		is = 0;
		for (it=0; it<nlat_2; it++)
		{
			sr = rr*st[it]; 	zr = rr*ct[it];
			if ((sr >= s[0])&&(sr <= s[ns-1])) {
				ds = dr[ir]*st[it] + zr*(pi/NLAT);		// local resolution in s direction.
				while (s[is+1] < sr) is++;
//				if (is+1 > ns) printf("is overflow %d\n",is);
				if ( ((delta == 0.)&&(ds > (s[is+1]-s[is]))) || ((delta != 0.)&&(ds > delta)) )
					nlow++; //printf("local resolution ds=%f too low ! (is=%d, r=%f, s=%f, z=%f)\n",(s[is+1]-s[is]),is,rr,sr,zr);
				ntot++;
				alp1 = (sr-s[is])/(s[is+1]-s[is]);
				alp0 = 1. - alp1;
				if (!nobc) z_eta = -sr/(1.0-sr*sr) *zr;
				for (ip=0; ip<NPHI; ip++) {
					vss = alp0*vs[is*incs + ip*incp] + alp1*vs[(is+1)*incs + ip*incp];
					vpp = alp0*vp[is*incs + ip*incp] + alp1*vp[(is+1)*incs + ip*incp];
					vzz = z_eta * vss;
					V->vr[ir][ip*NLAT +it] = vzz*ct[it] + vss*st[it];
					V->vt[ir][ip*NLAT +it] = vss*ct[it] - vzz*st[it];
					V->vp[ir][ip*NLAT +it] = vpp;
				}
			} else {
				for (ip=0; ip<NPHI; ip++) {
					V->vr[ir][ip*NLAT +it] = 0.0;
					V->vt[ir][ip*NLAT +it] = 0.0;
					V->vp[ir][ip*NLAT +it] = 0.0;
				}
			}
		}
		for (ip=0; ip<NPHI; ip++) {
			for (it=nlat_2; it<NLAT; it++) {	// copy symmetric data (scalar)
				V->vr[ir][ip*NLAT +it] =   V->vr[ir][ip*NLAT + (NLAT-1-it)];
				V->vt[ir][ip*NLAT +it] = - V->vt[ir][ip*NLAT + (NLAT-1-it)];
				V->vp[ir][ip*NLAT +it] =   V->vp[ir][ip*NLAT + (NLAT-1-it)];
			}
		}
	}

	printf("low resolution points : %d out of %d (%.2f%%)\n",nlow,ntot,100.*nlow/ntot);
}

/// get sizes from data written by Alex.
void get_sizes_scalar_2D_txt(char *fname, int* pns, int* pnp, double* s0)
{
	double s, p, f;
	double smax, pmax;
	long int i, ns, np;
	FILE *fp;

	printf("[Load] Reading 2D scalar data from text file '%s'...\n",fname);
	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[Load] file not found !");

// first pass : count phi and s :
	smax = -1.0;	pmax = -400.0;	*s0 = 1.e50;
	ns = 0;		np = 0;		i = 0;
	while (1) {
		i++;
		fscanf(fp, "%lf %lf %lf\n", &p, &s, &f);
		if (feof(fp)) break;
		if (s > smax) { smax=s;	 ns++; }
		if (p > pmax) { pmax=p;	 np++; }
		if (s < *s0) *s0 = s;
	}
	np --;		// first and last angle are the same, so we remove one.
	printf("File '%s' has %ld lines : ns=%ld, nphi = %ld\n",fname, i,ns,np);
	fclose(fp);

// return arguments :
	*pnp = np;	*pns = ns;
}

/// load data written by Alex.
/// 3 column ascii : phi s value
/// phi = -180 to 180; s <= 1; both increasing, with phi varying fastest.
/// Memory is allocated.
void load_scalar_2D_txt(char *fname, int ns, int np, double **ps, double **pdata)
{
	double s, p, f;
	double smax;
	double *sa, *data;
	long int is, ip;
	FILE *fp;

	printf("[Load] Reading 2D scalar data from text file '%s'...\n",fname);
	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[Load] file not found !");

	sa = (double*) malloc(sizeof(double) * ns);
	data = (double*) malloc(sizeof(double) * ns*np);

	if (phirev) printf("       => reversing phi orientation.\n");
	is = -1;	smax = -1.0;
	while(1) {
		fscanf(fp, "%lf %lf %lf\n", &p, &s, &f);
		if (feof(fp)) break;
		if (s>smax) {is++;	ip=np-1;	sa[is] = s;		smax =s;}			// store s array.
		if (phirev) p = -p;			// bugfix to Alex Fournier's psi data (not A)
		ip = (p/360)*np;		if (ip<0) ip+=np;		if (ip>=np) ip-=np;
		if ((ip>=0)&&(ip<np)) {
			if (is>=ns) runerr("s overflow");
			data[is + ip*ns] = f;
			ip--;		// phi varies fastest, and in reverse direction (bug of A. Fournier)
		}
	}
	fclose(fp);

// return arguments :
	*ps = sa;	*pdata = data;
}

/// go to spectral space.
/// fdata must be allocated (mmax modes)
void scal_spat_spec(int ns, int nphi, int mmax, double *data, double scale, cplx **fdata)
{
	cplx *fd;
	double fft_norm;
	fftw_plan fft;
	int nfft, ncplx, nreal;
	int is,m;
	
	nfft = nphi;
	ncplx = nphi/2 +1;
	nreal = 2*ncplx;
	fft_norm = 1.0/nfft;
	
// Allocate spectral fields.
	fd = (cplx *) VMALLOC(ns*ncplx * sizeof(cplx));
	
	fft = fftw_plan_many_dft_r2c(1, &nfft, ns, data, &nphi, ns, 1, reinterpret_cast<fftw_complex*>(fd), &ncplx, ns, 1, FFTW_ESTIMATE);
	fftw_execute(fft);
	
	for (m=0; m<=mmax; m++) {
		for (is=0; is<ns; is++) {
			(*fdata)[m*ns + is] = fd[m*ns + is] * fft_norm * scale;
		}
	}
	VFREE(fd);
}

/// low-pass filter so that the maximum radial scale is delta.
/// uses Gaussian smoothing.
double filter_radial(int ns, int np, int nlat, double scale, double *fdata)
{
	double ws[ns];		// filter weights
	double ds[ns];		// small interval
	double dr[NR];
	double ss,norm,delta;
	double *fd;
	int m, i0, i;
	
	delta = scale*( (M_PI/nlat)  + 2./NR );
	printf(" => radial filtering at length-scale %f\n",delta);

// copy data to temp storage.
	fd = (double *) malloc(ns*np * sizeof(double));
	for (m=0; m<ns*np; m++) fd[m] = fdata[m];

// compute interval size.
	i = 0;
		ds[i] = (s[i+1]-s[i])*0.5;
	for (i=1; i< ns-1; i++)
		ds[i] = (s[i+1]-s[i-1])*0.5;
	i = ns-1;
		ds[i] = (s[i]-s[i-1])*0.5;
		
// and for 3D also
	i = 0;
		dr[i] = (r[i+1]-r[i])*0.5;
	for (i=1; i< NR-1; i++)
		dr[i] = (r[i+1]-r[i-1])*0.5;
	i = NR-1;
		dr[i] = (r[i]-r[i-1])*0.5;

	for (i0=0; i0<ns; i0++) {
		for (m=0; m<np; m++) fdata[m*ns+ i0] = 0.0;
		// compute filter :
//		delta = 2.*((M_PI/nlat)*sqrt(1.-s[i0]*s[i0])  + dr[r_to_idx(s[i0])]);	// local cut-off.
//		delta = ( (M_PI/nlat)*(sqrt(1.-s[i0]*s[i0])+0.1)  + 1./NR );
		norm = 0.;
		for (i=0; i<ns; i++) {
			ss = (s[i]-s[i0])/delta;
			ws[i] = 0.0;
			if (ss*ss < 4*4) {	// weight is significant, sum it up.
				ws[i] = exp(-ss*ss) *ds[i];
				norm += ws[i];
			}
		}
		norm = 1.0/norm;
		// apply filter.
		for (i=1; i<ns-1; i++) {
			if (ws[i] > 0) {
				for (m=0; m<np; m++)
						fdata[m*ns +i0] += fd[m*ns +i] * ws[i]*norm;
			}
		}
	}
	free(fd);
	return(delta);
}

/// compute operators for finit differentiation in cylindrical space.
void init_Deriv_cyl(int ns)
{
	double t;
	int i;

	Gs = (struct TriDiag *) malloc(ns * sizeof(struct TriDiag));
	Ls = (struct TriDiag *) malloc(ns * sizeof(struct TriDiag));
	s_1 = (double *) malloc(ns * sizeof(double));
	
#define ds(is) (s[is+1]-s[is])
	// gradient et Laplacien
	for (i=1;i<ns-1;i++)
	{
//		printf("s =%f\n",s[i]);
		s_1[i] = 1.0/s[i];
		t = 1.0/((ds(i-1)+ds(i))*ds(i)*ds(i-1));
		Gs[i].l = -ds(i)*ds(i)*t;
		Gs[i].d = (ds(i)*ds(i) - ds(i-1)*ds(i-1))*t;	// =0 en grille reguliere.
		Gs[i].u = ds(i-1)*ds(i-1)*t;

		Ls[i].l =  2.0*ds(i)*t            + s_1[i]*Gs[i].l;
		Ls[i].d =  -2.0*(ds(i-1)+ds(i))*t + s_1[i]*Gs[i].d;
		Ls[i].u =  2.0*ds(i-1)*t          + s_1[i]*Gs[i].u;
	}
#undef ds
}

/// compute the QG velocity field in cylindrical space.
void psi_to_vectQG(int ns, int nphi, int mmax, cplx *psi, double **vs, double **vp)
{
	fftw_plan iffts, ifftp;
	cplx *vsf, *vpf;
	int ncplx, nreal;
	int i,j, m,ir;

	ncplx = nphi/2 +1;
	nreal = 2*ncplx;
	
	vsf = (cplx *) VMALLOC(sizeof(cplx) * ns * ncplx);
	vpf = (cplx *) VMALLOC(sizeof(cplx) * ns * ncplx);
	*vs = (double *) vsf;	*vp = (double *) vpf;

	iffts = fftw_plan_many_dft_c2r(1, &nphi, ns, reinterpret_cast<fftw_complex*>(vsf), &ncplx, ns, 1, *vs, &nreal, ns, 1, FFTW_ESTIMATE);
	ifftp = fftw_plan_many_dft_c2r(1, &nphi, ns, reinterpret_cast<fftw_complex*>(vpf), &ncplx, ns, 1, *vp, &nreal, ns, 1, FFTW_ESTIMATE);

// set beta for divergence free flow :
	printf(" => beta-correction weight = %f\n",beta);
#define BETA ( -s[ir]*beta/(1. - s[ir]*s[ir]) )

	for(m=0;m<=mmax;m++) {
		ir = 0;
			i= m*ns + ir;		j= m*ns + ir;
			vsf[j] = cplx(0,m*s_1[ir]) * psi[i];
			vpf[j] = -(psi[i+1]-psi[i])/(s[ir+1]-s[ir]) 	- BETA * psi[i];
		for(ir=1;ir<ns-1;ir++)
		{
			i= m*ns + ir;		j= m*ns + ir;
			vsf[j] = cplx(0,m*s_1[ir]) * psi[i];
			vpf[j] = -Gs[ir].l*psi[i-1] - Gs[ir].d*psi[i] - Gs[ir].u*psi[i+1]
					- BETA * psi[i];
		}
		ir = ns-1;
			i= m*ns + ir;		j= m*ns + ir;
			vsf[j] = cplx(0,m*s_1[ir]) * psi[i];
			vpf[j] = -(psi[i]-psi[i-1])/(s[ir]-s[ir-1]);
	}
#undef BETA
	// zero padding.
	for (m=mmax+1; m<ncplx; m++) {
		for (ir=0;ir<ns;ir++)
		{
			j = m*ns + ir;
			vsf[j] = 0.0;
			vpf[j] = 0.0;
		}
	}

	// do the IFFT. (unnormalized)
	fftw_execute(iffts);	fftw_execute(ifftp);
}

void usage()
{
	printf("\nUsage: qg2sh <stream-function-file> <output-file> [options] \n");
	printf("** list of available options :\n");
	printf(" -lmax=<lmax> : defines the maximum spherical harmonic degree <lmax> of output field\n");
	printf(" -mmax=<mmax> : defines the maximum spherical harmonic order <mmax> of output field\n");
	printf(" -nr=<nr> : defines the number <nr> of radial shells\n");
	printf(" -fill : output data everywhere (inner-core and mantle too, useful for magnetic fiels)\n");
	printf(" -nobc : don't enforce the non-penetration boundary condition on the sphere\n");
	printf(" -filter=<scale> : filter the 2D data before extrapolating, to match 3D resolution\n");
	printf(" -beta=<beta> : wheight for divergence-free correction. 0=no correction (default), 1=full correction.\n");
	printf(" -scale=<scale> : multiply the data by scale\n");
	printf(" -unit=[km/yr|m.T] : defines the unit of the input field, to convert in non-dimensional ones\n");
}

int main (int argc, char *argv[])
{
	struct VectField V;
	struct PolTor Vlm;
	double *dat, *vs, *vp;
	cplx *psi;
	int ns, np, i, j;
	double filter_scale = 0.;
	double tmp, scale=1.;
	char name[20], unit[20]="";

	printf(" [QG2SH] build %s, %s\n",__DATE__,__TIME__);
	if (argc <= 2) { usage(); exit(1); }

// get file infos
	get_sizes_scalar_2D_txt(argv[1], &ns, &np, &tmp);

// Guess parameters
	MMAX = np/3 -1;		// number of resolved modes.
	NR = ns*2;
	NLAT = 2* ceil( pi/2*sqrt(1. - tmp*tmp)*ns );		// worst case.
	LMAX = NLAT-2;
	if (LMAX >= 256) { LMAX = 255;	NLAT = 256; }
	if (MMAX > LMAX) {
		LMAX = MMAX;
		NLAT = 2*(LMAX/2+1);
	}

// parse options to override (bad!) defaults.
	for (i=3; i<argc; i++) {
		j = sscanf(argv[i],"-%[^=]=%lf",name,&tmp);
		if (j==1) tmp = 1.0;	// no value given : defaults to 1.
//		printf("argv=%s   (matched %d) : name=%s   val=%f\n",argv[i], j, name, tmp);
		if (strcmp(name,"lmax") == 0) { LMAX = tmp; NLAT = 2*LMAX; }
		if (strcmp(name,"mmax") == 0) MMAX = tmp;
		if (strcmp(name,"nr") == 0) NR = tmp;
		if (strcmp(name,"fill") == 0) fill = 1;
		if (strcmp(name,"phirev") == 0) phirev = tmp;
		if (strcmp(name,"nobc") == 0) nobc = 1;
		if (strcmp(name,"beta") == 0) beta = tmp;
		if (strcmp(name,"scale") == 0) scale = tmp;
		if (strcmp(name,"unit") == 0) sscanf(argv[i],"-unit=%s",unit);
		if (strcmp(name,"filter") == 0) filter_scale = tmp;
	}
	if (MMAX >= np/2) MMAX = np/2 -1;		// check for wrong parameters...

// scale and units :
	if (strcmp(unit,"km/yr") == 0) {	// psi has units of km^2/yr => Rcmb^2/(day/2pi)
		scale *= 1./(3480.*3480. * 2*M_PI*365);
		printf(" => converting from %s.\n",unit);
	} else if (strcmp(unit,"m.T") == 0) {
		scale *= 1./(2.*M_PI/(3600*24) * 3.48e6*3.48e6 * sqrt(4.*M_PI*1.e-7 * 1.09e4) );
		printf(" => converting from %s.\n",unit);
	}

// load data.
	load_scalar_2D_txt(argv[1], ns, np, &s, &dat);
	init_Deriv_cyl(ns);

// save original field.
	psi = (cplx*) VMALLOC(sizeof(cplx) * ns * (MMAX+1));
	scal_spat_spec(ns, np, MMAX, dat, scale, &psi);
	psi_to_vectQG(ns, (MMAX+1)*2, MMAX, psi, &vs, &vp);
	write_2D("o_disc.qg_org", vs, vp, ns, (MMAX+1)*2);
	printf("> 2D vector field from original data is written to o_disc.qg_org\n");

	init_rad_sph(0.0, s[0], s[ns-1], 1.0, 0,0);
	if (filter_scale > 0.) {		// radial filtering to match 3D resolution.
		filter_scale = filter_radial(ns, np, NLAT, filter_scale, dat);
		scal_spat_spec(ns, np, MMAX, dat, scale, &psi);
		psi_to_vectQG(ns, (MMAX+1)*2, MMAX, psi, &vs, &vp);
		write_2D("o_disc.qg_filter", vs, vp, ns, (MMAX+1)*2);
		printf("> *filtered* 2D vector field is written to o_disc.qg_filter\n");
	}

	NPHI = (MMAX+1)*2;	// avoid using too many points in phi.
// spherical harmonic part.
	init_sh(sht_quick_init, 1.e-10, 1);
	if (fill) { irmin = 0;   irmax = NR-1; }
		else  { irmin = NG;  irmax = NM;   }
	printf(" => output field from r=%f to r=%f\n",r[irmin],r[irmax]);

	Vlm.alloc(irmin, irmax);
	V.alloc(irmin, irmax);
	vect_QG_to_3D(ns, 1, ns, vs, vp, &V, filter_scale);

	spat_to_PolSphTor(&V, Vlm.Pol, NULL, Vlm.Tor, irmin, irmax);
	Vlm.save_single(argv[2],LMAX,MMAX);
	printf("3D field saved to file '%s'\n",argv[2]);
}


#include <vector>

#define NDEBUG
#undef SSE

#ifdef XS_MPI
	#error "XS_SPARSE not supported with MPI"
#endif

#include "eigen3/Eigen/Sparse"
#if XS_SPARSE != 2
	// SuperLU (default): link with -lsuperlu -lblas
	#include "eigen3/Eigen/SuperLUSupport"
#else
	// UmfPack/SuiteSparse: link with -lumfpack -lcholmod -lcolamd -lamd -lsuitesparseconfig -lblas
	#include "eigen3/Eigen/UmfPackSupport"
#endif

//#include "eigen3/unsupported/Eigen/SparseExtra"

namespace xs_sparse {

using namespace Eigen;

int Nstate = 0;			// number of linearly independant states (m and sym)
class UTstate *state;

enum comp {
	Ut, Up, T, T0, NCOMP
};

//typedef SparseMatrix<double, ColMajor> rSparse; // declares a column-major sparse matrix type of real (double)
typedef SparseMatrix<cplx, ColMajor> cSparse; // declares a column-major sparse matrix type of complex (double)
typedef Triplet<double> rCij;
typedef Triplet<cplx> cCij;

/// Velocity-Temperature coupled state.
class UTstate {
  public:
	VectorXcd V;		// the state of the system for a given m
	VectorXcd Vrhs;
	cSparse M;			// linear operator of the rhs of the equation [invariant]
	cSparse Mt;			// linear operator matrix for d/dt
	cSparse Mrhs;		// rhs linear operator for the time-scheme [depends on dt]
	cSparse Mlhs;		// lhs linear operator for the time-scheme [depends on dt]
	cSparse Mcor_s;		// Coriolis
//****************************************************************************************************************
//	/!\ WARNING! as of Eigen 3.3.7, the internal SparseLU solver is NOT suitable and leads to WRONG results.
//		Use UmfPackLU or SuperLU instead !
// 		SuperLU 5.2.1  is faster at solve, but slower at factorization than SuiteSparse(UMFPACK) 5.4
//****************************************************************************************************************
#if XS_SPARSE != 2
	SuperLU<cSparse> solver;	// external SuperLU solver
#else
	UmfPackLU<cSparse> solver;	// external Umfpack solver
#endif
	int m;				// order (azimutal mode)
	int sym;			// 0 (sym)  or 1 (antisym)
	cplx L0;			// angular momentum to be conserved (if applicable)

  private:
	int ofs[NCOMP+1];
	int nl[NCOMP];
	int nr[NCOMP];
	int l0[NCOMP];
	int irs[NCOMP];

  public:
	UTstate();
	~UTstate();

	void add_comp(comp c, int lend, int istart, int iend);		///< add component, with lend (increases size of vector, must be called NO MORE THAN ONCE for each component)
	long alloc();												///< allocate memory, once all components have been added.
	void reset();												///< reset to empty state, free memory if needed.

	long idx(enum comp c, int ir, int l) const;
	void add_coeff(std::vector<cCij> &cij, long id, enum comp c, int ir, int l, cplx v);
	void add_coeff(std::vector<rCij> &cij, long id, enum comp c, int ir, int l, double v);
	double build_matrix(double Omega0);
	double prepare_solve(double Cdt_lhs, double CM_lhs, double Cdt_rhs, double CM_rhs);

	void from_shells(VectorXcd& V, const StateVector& Xlm);		///< gather coefficients from PolTor and ScalarSH fields
	void to_shells(const VectorXcd& V, StateVector& Xlm) const;		///< dispatch coefficients to PolTor and ScalarSH fields

	cplx angular_momentum() const;
	void conserve_momentum();

	void from_shells(VectorXcd& V, comp ic, xs_array2d<cplx> shells);		///< gather coefficients from PolTor and ScalarSH fields
	void to_shells(const VectorXcd& V, comp ic, xs_array2d<cplx> shells) const;		///< dispatch coefficients to PolTor and ScalarSH fields
private:
	//void from_shell(comp ic, int ir, cplx* shell);
	//void to_shell(comp ic, int ir, cplx* shell) const;

	// use V.data() to get a pointer to the data.
	// use V.segment(start, size) to get a subvector.
};

void UTstate::add_comp(enum comp c, int lend, int istart, int iend)
{
	if (c >= NCOMP) runerr("[State:add_comp] undefined component");
	if (lend < m) runerr("[State::add_comp] empty component");

	int lstart = m;
	// select modes with symmetry "sym"
	if (c==Ut) {
		lstart += (1-sym);
	} else {
		lstart += sym;
	}
	if (c!=T0) {
		if (lstart==0) lstart += 2;		// mode l=0 does not exist for a divergence-less vector field
	}

	nl[c] = (lend-lstart)/2 + 1;
	l0[c] = lstart;
	irs[c] = istart;
	nr[c] = iend-istart+1;

	for (int i=c+1; i<=NCOMP; i++) {
		ofs[i] += nl[c]*nr[c];		// push next components by size.
	}
}

/// if l is not present, then a wrong index is returned.
long UTstate::idx(enum comp c, int ir, int l) const 
{
	return ofs[c] + (ir-irs[c])*nl[c] + (l-l0[c])/2;		// contiguous l, with symmetry selected by l0[c]
}

UTstate::UTstate() {
	for (int i=0; i<=NCOMP; i++) ofs[i] = 0;
	for (int i=0; i<NCOMP; i++) {
		l0[i] = -1;		nl[i] = 0;		nr[i] = 0;
	}
	m = 0;
	sym = 0;
	L0 = 0;
}

long UTstate::alloc()
{
	long n = ofs[NCOMP];
	V.resize(n);
	Vrhs.resize(n);
	Mrhs.resize(n,n);
	Mlhs.resize(n,n);
	Mt.resize(n,n);
	M.resize(n,n);
	Mcor_s.resize(n,n);
	return n;
}

void UTstate::reset()
{
	V.resize(0);
	for (int i=0; i<=NCOMP; i++) ofs[i] = 0;
	for (int i=0; i<NCOMP; i++) {
		l0[i] = -1;		nl[i] = 0;		nr[i] = 0;
	}
	L0 = 0;
}

UTstate::~UTstate() {
	reset();
}

void UTstate::add_coeff(std::vector<cCij> &cij, long id, enum comp c, int ir, int l, cplx v)
{
	if ((l>=l0[c])&&(l<=LMAX)&&(ir>=irs[c])&&(ir<irs[c]+nr[c])) {
		long id1 = idx(c, ir, l);
		//printf("(c,ir,l)=(%d,%d,%d)  ::  M(%d, %d) = %g,%g\n",c,ir,l, id, id1, real(v), imag(v));
		cij.push_back(cCij( id, id1, v));
	}
}

void UTstate::add_coeff(std::vector<rCij> &cij, long id, enum comp c, int ir, int l, double v)
{
	if ((l>=l0[c])&&(l<=LMAX)&&(ir>=irs[c])&&(ir<irs[c]+nr[c])) {
		long id1 = idx(c, ir, l);
		cij.push_back(rCij( id, id1, v));
	}
}


double UTstate::build_matrix(double Omega0)
{
	std::vector<cCij> cij;		// coefficient list in triplet format.
	std::vector<rCij> dij;
	std::vector<cCij> corij;		// coefficient list in triplet format.
	double tasm = xs_wtime();

	if (evol_ubt & EVOL_U) {
		int i0 = irs[Ut];
		int i1 = irs[Ut] + nr[Ut] -1;

		// Toroidal evolution :
		int bir = MUt.bandwidth();
		for (int l=l0[Ut]; l<=LMAX; l+=2) {
			// Viscosity
			for (int ir=i0; ir<=i1; ir++) {
				for (int k=-bir; k<=bir; k++) {
					add_coeff(cij, idx(Ut, ir, l), Ut, ir+k, l,  MUt.coeff(ir,l,k) );
				}
				add_coeff(dij, idx(Ut, ir, l), Ut, ir,   l,  1.0 );
			}
			// Coriolis
			if (Omega0 != 0.0) {
				int lm = LM(shtns,l, m);
				double Mc00 = 2.*Omega0 * Mcor[4*lm];		double Mc01 = 2.*Omega0 * Mcor[4*lm+1];		// for P/r
				double Mc10 = 2.*Omega0 * Mcor[4*lm+2];		double Mc11 = 2.*Omega0 * Mcor[4*lm+3];		// for dP/dr
				double Mm = 2.*Omega0 * Mm_l2[lm];
				const int bgr = 1;	// bandwidth
				for (int ir=i0; ir<=i1; ir++) {		// TODO: Here, should we use 4th-order FD ?
					add_coeff(corij, idx(Ut, ir, l), Up, ir, l-1,  -r_1[ir]*Mc00 );
					add_coeff(corij, idx(Ut, ir, l), Up, ir, l+1,  -r_1[ir]*Mc01 );
					const double* Gr = &GRAD(ir).Gd;
					for (int k=-bgr; k<=bgr; k++) {
						add_coeff(corij, idx(Ut, ir, l), Up, ir+k, l-1,  -Gr[k]*Mc10 );
						add_coeff(corij, idx(Ut, ir, l), Up, ir+k, l+1,  -Gr[k]*Mc11 );
					}
					if (m > 0) {
						add_coeff(corij, idx(Ut, ir, l), Ut, ir,   l,  cplx(0, Mm) );
					}
				}
			}
		}
		
		// Poloidal evolution :
		i0 = irs[Up];
		i1 = irs[Up] + nr[Up] -1;
		bir = MUp.bandwidth();
		int bir2 = MU_Lr.bandwidth();
		for (int l=l0[Up]; l<=LMAX; l+=2) {
			// Viscosity
			for (int ir=i0; ir<=i1; ir++) {
				for (int k=-bir; k<=bir; k++) {
					add_coeff(cij, idx(Up, ir, l), Up, ir+k, l,  -MUp.coeff(ir,l,k) );
				}
				for (int k=-bir2; k<=bir2; k++) {
					add_coeff(dij, idx(Up, ir, l), Up, ir+k, l,  -MU_Lr.coeff(ir,l,k) );
				}
			}
			// Coriolis
			if (Omega0 != 0.0) {
				int lm = LM(shtns,l, m);
				double Mc00 = 2.*Omega0 * Mcor[4*lm];		double Mc01 = 2.*Omega0 * Mcor[4*lm+1];		// for P/r
				double Mc10 = 2.*Omega0 * Mcor[4*lm+2];		double Mc11 = 2.*Omega0 * Mcor[4*lm+3];		// for dP/dr
				double Mm = 2.*Omega0 * Mm_l2[lm];
				const int bgr = 1;	// bandwidth
				for (int ir=i0; ir<=i1; ir++) {
					add_coeff(corij, idx(Up, ir, l), Ut, ir, l-1,  -r_1[ir]*Mc00 );
					add_coeff(corij, idx(Up, ir, l), Ut, ir, l+1,  -r_1[ir]*Mc01 );
					const double* Gr = &GRAD(ir).Gd;
					for (int k=-bgr; k<=bgr; k++) {
						add_coeff(corij, idx(Up, ir, l), Ut, ir+k, l-1,  -Gr[k]*Mc10 );
						add_coeff(corij, idx(Up, ir, l), Ut, ir+k, l+1,  -Gr[k]*Mc11 );
					}
					if (m > 0) {
						for (int k=-bir2; k<=bir2; k++) {
							add_coeff(corij, idx(Up, ir, l), Up, ir+k, l,  cplx(0, -Mm*MU_Lr.coeff(ir,l,k)) );
						}
					}
				}
			}
			if ((Grav0_r) && (evol_ubt & EVOL_T)) {
				// Buoyancy due to radial gravity
				for (int ir=i0; ir<=i1; ir++) {
					add_coeff(cij, idx(Up, ir, l), T, ir, l,  Grav0_r[ir] );
				}
			}
			if ((T0lm) && (evol_ubt & EVOL_U)) {
				// Advection of background temperature (l=0)
				// this l=0 temperature profile could be updated now and then. Probably when time-step changes ?
				int nj = T0lm->nlm;
				if ((nj>0) && ( T0lm->lm[0] == 0 )) {
					double l2norm = l2[l]/Y00_1;
					cplx* dT = T0lm->TdT + 1;		// radial gradient
					for (int ir=i0; ir<=i1; ir++) {
						add_coeff(cij, idx(T, ir, l), Up, ir, l,  -l2norm*r_1[ir]*real(dT[ir*nj*2]) );
					}
				}
			}
		}
	}
	if (evol_ubt & EVOL_T) {
		int bir = MT.bandwidth();
		int i0 = irs[T];
		int i1 = irs[T] + nr[T] -1;
		for (int l=l0[T]; l<=LMAX; l+=2) {
			// Diffusion
			for (int ir=i0; ir<=i1; ir++) {
				for (int k=-bir; k<=bir; k++) {
					add_coeff(cij, idx(T, ir, l), T, ir+k,   l,  MT.coeff(ir,l,k) );
				}
				add_coeff(dij, idx(T, ir, l), T, ir,   l,  1.0 );
			}
		}
		i0 = irs[T0];
		i1 = irs[T0] + nr[T0] -1;
		for (int l=l0[T0]; l<=0; l+=2) {
			// Diffusion
			for (int ir=i0; ir<=i1; ir++) {
				for (int k=-bir; k<=bir; k++) {
					add_coeff(cij, idx(T0, ir, l), T0, ir+k,   l,  MT.coeff(ir,l,k) );
				}
				add_coeff(dij, idx(T0, ir, l), T0, ir,   l,  1.0 );
			}
		}
	}
//	printf("*** nnz:  Mrhs=%d, Mt=%d, Mcor_s=%d\n",cij.size(), dij.size(), corij.size());
	M.setFromTriplets(cij.begin(), cij.end());
	Mt.setFromTriplets(dij.begin(), dij.end());
	Mcor_s.setFromTriplets(corij.begin(), corij.end());

	M += Mcor_s;	// implicit Coriolis
	solver.analyzePattern(M);		// symbolic factorization

	tasm = xs_wtime() - tasm;
	return tasm;
}

double UTstate::prepare_solve(double Cdt_lhs, double CM_lhs, double Cdt_rhs, double CM_rhs)
{
	double tfact = xs_wtime();

	Mlhs = CM_lhs*M + Cdt_lhs*Mt;
//	solver.analyzePattern(Mlhs);	// symbolic factorization
	solver.factorize(Mlhs);			// numeric factorization

	tfact = xs_wtime() - tfact;
	if(solver.info()!=Success) {
		printf("m=%d, sym=%d\n",m, sym);
		runerr("matrix factorization failed !!\n");
	}

	if (CM_rhs==0.0) {
		Mrhs = Cdt_rhs*Mt;
	} else {
		Mrhs = CM_rhs*M + Cdt_rhs*Mt;
	}
	return tfact;
}

/*
void UTstate::from_shell(comp ic, int ir, cplx* shell)
{
	if ((ir>=irs[ic])&&(ir<irs[ic]+nr[ic])) {
		const int lmin = l0[ic];
		const int lm0 = LM(shtns,lmin,m);
		const long idx0 = idx(ic, ir, lmin);
		for (int l=0; l<=(LMAX-lmin)/2; l++)	V.coeffRef(idx0 + l) = shell[lm0+2*l];
	}
}

void UTstate::to_shell(comp ic, int ir, cplx* shell) const
{
	if ((ir>=irs[ic])&&(ir<irs[ic]+nr[ic])) {
		const int lmin = l0[ic];
		const int lm0 = LM(shtns,lmin,m);
		const long idx0 = idx(ic, ir, lmin);
		for (int l=0; l<=(LMAX-lmin)/2; l++)	shell[lm0+2*l] = V.coeff(idx0 + l);
	}
}
*/

void UTstate::from_shells(VectorXcd& V, comp ic, xs_array2d<cplx> shells)
{
	if (ofs[ic] < ofs[ic+1]) {
		const int lmin = l0[ic];
		const int ire = nr[ic]-1 + irs[ic];
		const int lm0 = LM(shtns,lmin,m);
		for (int ir=irs[ic]; ir<=ire; ir++) {
			const long idx0 = idx(ic, ir, lmin);
			for (int l=0; l<nl[ic]; l++)	V.coeffRef(idx0 + l) = shells[ir][lm0+2*l];
		}
	}
}

void UTstate::from_shells(VectorXcd& V, const StateVector& Xlm)
{
	from_shells(V, Ut, Xlm.U.Tor);
	from_shells(V, Up, Xlm.U.Pol);
	from_shells(V, T, Xlm.T.Sca);
	from_shells(V, T0, Xlm.T.Sca);
}

void UTstate::to_shells(const VectorXcd& V, comp ic, xs_array2d<cplx> shells) const
{
	if (ofs[ic] < ofs[ic+1]) {
		const int lmin = l0[ic];
		const int ire = nr[ic]-1 + irs[ic];
		const int lm0 = LM(shtns,lmin,m);
		for (int ir=irs[ic]; ir<=ire; ir++) {
			const long idx0 = idx(ic, ir, lmin);
			for (int l=0; l<nl[ic]; l++)	shells[ir][lm0+2*l] = V.coeff(idx0 + l);
		}
	}
}

void UTstate::to_shells(const VectorXcd& V, StateVector& Xlm) const
{
	to_shells(V, Ut, Xlm.U.Tor);
	to_shells(V, Up, Xlm.U.Pol);
	to_shells(V, T, Xlm.T.Sca);
	to_shells(V, T0, Xlm.T.Sca);
}


/// compute angular momentum
cplx UTstate::angular_momentum() const
{
	cplx L = 0;
	if (l0[Ut] == 1) {		// toroidal l=1 is present.
		const int ire = nr[Ut]-1 + irs[Ut];
		for (int ir=irs[Ut]; ir<=ire; ir++) {
			int ir_p = ir+1;		int ir_m = ir-1;
			if (ir == Ulm.ir_bci)		ir_m = ir;
			else if (ir == Ulm.ir_bco)	ir_p = ir;
			double dV = r[ir]*r[ir]*r[ir] * 0.5*(r[ir_p]-r[ir_m]);
			L += dV* V.coeff(idx(Ut, ir, 1));
		}
	}
	return L;
}

/// correct solid-body rotation to conserve angular momentum.
void UTstate::conserve_momentum()
{
	if (l0[Ut] == 1) {		// toroidal l=1 is present.
		cplx L = angular_momentum();
		double ri = r[Ulm.ir_bci];
		double ro = r[Ulm.ir_bco];
		double n = 5.0/(ro*ro*ro*ro*ro - ri*ri*ri*ri*ri);		// normalization
		L = (L0 - L)*n;
		const int ire = nr[Ut]-1 + irs[Ut];
		for (int ir=irs[Ut]; ir<=ire; ir++) {
			V.coeffRef(idx(Ut, ir, 1)) += L * r[ir];		// correct solid-body rotation
		}
	}
}


/// select parity of state with "sym" : 0 = both, 1 = odd, 2 = even.
void prepare_state(StateVector& Xlm, int sym, double Omega0, int kill_sbr)
{
	Nstate = MMAX+1;
	if (sym == 0) Nstate *= 2;
	state = new xs_sparse::UTstate[Nstate];

	for (int is=0; is<Nstate; is++) {
		if (sym == 0) {
			state[is].m = (is/2)*MRES;
			state[is].sym = is&1;
		} else {
			state[is].m = is*MRES;
			state[is].sym = sym&1;
		}
	}

	#pragma omp parallel for schedule(dynamic,1)
	for (int is=0; is<Nstate; is++) {
		if (evol_ubt & EVOL_U) {
			state[is].add_comp(xs_sparse::Up, LMAX, Xlm.U.ir_bci+1, Xlm.U.ir_bco-1);
			state[is].add_comp(xs_sparse::Ut, LMAX, MUt.ir_bci, MUt.ir_bco);
		}
		if (evol_ubt & EVOL_T) {
			if (r[Tlm.ir_bci] == 0.0) {
				state[is].add_comp(xs_sparse::T, LMAX, Tlm.ir_bci+1, MT.ir_bco);
			} else {
				state[is].add_comp(xs_sparse::T, LMAX, MT.ir_bci, MT.ir_bco);
			}
			if ((state[is].m == 0) && (state[is].sym == 0))		// l=0 component
				state[is].add_comp(xs_sparse::T0, 0, MT.ir_bci, MT.ir_bco);
		}
		long size = state[is].alloc();

		double tasm = state[is].build_matrix(Omega0);
		long Msize = state[is].M.nonZeros();
		#ifdef XS_DEBUG
		printf("m=%d,sym=%d => vector size = %d,  Matrix nnz = %d  (sparsity = %g), assembled in %g sec.\n", state[is].m, state[is].sym, size , Msize, (((double) Msize)/(size*size)), tasm);
		#endif

	#ifndef NDEBUG
		if ((is<=1)||(size < 10000)) {
			char fname[100];
			sprintf(fname, "Matrix_m%d_%d",state[is].m,state[is].sym);
			saveMarket(state[is].M, fname);
			printf("m=%d,sym=%d => matrix written to file.\n",state[is].m,state[is].sym);
		}
	#endif

	// initial state from state Xlm
		state[is].from_shells(state[is].V, Xlm);
		if (kill_sbr > 1) state[is].L0 = state[is].angular_momentum();		// save initial angular momentum.
	}
}

void prepare_solve(double Cdt_lhs, double CM_lhs, double Cdt_rhs, double CM_rhs)
{
	printf("sparse LU: Cdt=%g, CM=%g   RHS: Cdt=%g, CM=%g\n",Cdt_lhs, CM_lhs, Cdt_rhs, CM_rhs);
	#pragma omp for schedule(dynamic,1)
	for (int is=0; is<Nstate; is++) {
		state[is].prepare_solve(Cdt_lhs, CM_lhs, Cdt_rhs, CM_rhs);
	}
}

void solve_state(StateVector& Xrhs, int kill_sbr)
{
	#pragma omp for schedule(dynamic,1)
	for (int im=0; im<Nstate; im++) {
		state[im].from_shells(state[im].Vrhs, Xrhs);	// V
		state[im].V = state[im].solver.solve(state[im].Vrhs);
		if (kill_sbr) state[im].conserve_momentum();
		state[im].to_shells(state[im].V, Xrhs);
	}
}

void apply_Mrhs(StateVector& Y, const StateVector& X)
{
	#pragma omp for schedule(dynamic,1)
	for (int im=0; im<Nstate; im++) {
		state[im].from_shells(state[im].V, X);
		state[im].Vrhs = state[im].Mrhs * state[im].V;
		state[im].to_shells(state[im].Vrhs, Y);
	}
}

void apply_M(StateVector& Y, const StateVector& X)
{
	#pragma omp for schedule(dynamic,1)
	for (int im=0; im<Nstate; im++) {
		state[im].from_shells(state[im].V, X);
		state[im].Vrhs = state[im].M * state[im].V;
		state[im].to_shells(state[im].Vrhs, Y);
	}
}

/// Add Coriolis force to the NL StateVector, as required by explicit_terms
void apply_Cor(StateVector& NL, const StateVector& X)
{
	#pragma omp for schedule(dynamic,1)
	for (int im=0; im<Nstate; im++) {
		state[im].from_shells(state[im].V, X);
		state[im].from_shells(state[im].Vrhs, Ut, NL.U.Pol);
		state[im].from_shells(state[im].Vrhs, Up, NL.U.Tor);
		state[im].Vrhs += state[im].Mcor_s * state[im].V;
		state[im].to_shells(state[im].Vrhs, Ut, NL.U.Pol);
		state[im].to_shells(state[im].Vrhs, Up, NL.U.Tor);
	}
}


}

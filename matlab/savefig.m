% savefig : save an octave figure to latex/eps format

function savefig(base)


axis square
papersize = get (gcf, "papersize"); # presently the paper units must be inches
border = 2;
set (gcf, "paperposition", [border, border, (papersize - 2*border)]); 
print([base '.eps'],'-depsc2');
print([base '.pdf'],'-dpdf');
%print([base '.png'],'-dpng','-F:16');

end


%papersize = get (gcf, "papersize"); # presently the paper units must be inches
%border = 0.25;
%set (gcf, "paperposition", [border, border, (papersize - 2*border)]) 

% how to load spatial data from 'xshells' runs into octave :
% 1) create a hdf5 file from a poltor* file using this command :
%    xspp <my-poltor-file> hdf5 <hdf5-file>
% 2) in octave, load the data from the hdf5 file using the following function :

function [Vr, Vtheta, Vphi, r, theta, phi, x, y, z] = load_xs3D(fname)

load(fname);		% import all variables from hdf5 file

Vr = single(Vr);		% it is stored in single precision in the hdf5 file
Vtheta = single(Vtheta);
Vphi = single(Vphi);

if length(phi) > 1		% add phi = 2*pi data for periodic representation :
	phi = [phi 2*pi];	% add last phi for periodic representation.
	np = length(phi);
	Vr(np,:,:) = Vr(1,:,:);
	Vtheta(np,:,:) = Vtheta(1,:,:);
	Vphi(np,:,:) = Vphi(1,:,:);
	s = reshape(sin(theta')*r, size(Vr(1,:,:)));
	z1 = reshape(cos(theta')*r, size(Vr(1,:,:)));
	x = single(zeros(size(Vr)));	% single precision is enough
	y = single(zeros(size(Vr)));
	z = single(zeros(size(Vr)));
	for ip=1:length(phi)		% build the [x,y,z] grid.
		x(ip,:,:) = s * cos(phi(ip));
		y(ip,:,:) = s * sin(phi(ip));
		z(ip,:,:) = z1;
	end
else
	Vr = squeeze(Vr);				% 2D meridional data only.
	Vthteta = squeeze(Vtheta);
	Vphi = squeeze(Vphi);
	x = single(sin(theta')*r);
	y = 0;
	z = single(cos(theta')*r);
end

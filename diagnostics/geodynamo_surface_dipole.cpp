/** XSHELLS diagnostics:
*  dipolar magnetic field at the surface (3 components) + quadrupole data + fraction of dipole field
*/

	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(6, "Bx, By, Bz, Bq0, Bqnz, f_dip\t ");		// append array for 4 diagnostics
		if (Blm.ire == Blm.ir_bco) {		// if this process has the last shell:
			const int ir = Blm.ir_bco;
			if ((MMAX > 0) && (MRES == 1)) {
				diags[0] = real(Blm.Pol[ir][LiM(shtns,1,1)])/Y11_st;		// Equatorial dipole, x
				diags[1] = imag(Blm.Pol[ir][LiM(shtns,1,1)])/Y11_st;		// Equatorial dipole, y
			}
			diags[2] = real(Blm.Pol[ir][LiM(shtns,1,0)])/Y10_ct;		// Axial dipole
			diags[3] = l2[2] * real(Blm.Pol[ir][LiM(shtns,2,0)]);		// Axial quadrupole
			const double r_1 = 1.0/r[ir];
			double qnz = 0.0;
			const int m_max = (MMAX*MRES >= 2) ? 2 : MMAX*MRES;
			for (int im=1; im*MRES<=m_max; im++) {
				cplx Br = l2[2] * Blm.Pol[ir][LiM(shtns,2,im)];
				qnz += norm(Br);
			}
			diags[4] = sqrt(2*qnz);		// non-axial quadrupole (count twice for m>0)
			// compute Br2 for l=1..12, needed to compute fdip (see Christensen & Aubert 2006, end of section 2)
			int llim = 12;		// maximum degree taken into account here.
			if (llim < LMAX) llim = LMAX;	// avoid array overflows !!!
			double Br2dip = 0.0;
			double Br2all = 0.0;
			double pre = 1.0;
			for (int im=0; im<=MMAX; im++) {
				for (int l=im*MRES; l<=llim; l++) {
					cplx Br = l2[l] * Blm.Pol[ir][LiM(shtns,l,im)] * r_1;
					double Br2 = pre * norm(Br);
					Br2all += Br2;
					if (l==1) Br2dip += Br2;
				}
				pre = 2.0;		// count twice for m>0
			}
			diags[5] = sqrt( Br2dip / Br2all );		// f_dip
		}
	}

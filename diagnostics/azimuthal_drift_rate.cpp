/** XSHELLS diagnostics:
*  azimuthal drift rate of temperature field at given order m.
*  m=4 for geodynamo benchmark (Christensen+ 2001), m=3 for full-sphere benchmark (Marti+ 2014).
*  the order m must be set in the xshells.par file with a line like "m_drift = 4"
*/

	if (evol_ubt & EVOL_T) {
		const int m_probe = mp.var["m_drift"];
		static cplx tlm = 0;
		if ((m_probe > 0) && (MMAX*MRES >= m_probe) && (m_probe % MRES == 0)) {
			double* diags = all_diags.append(2, "w_drift tp(m_drift)\t ");		// append array for 2 diagnostics
			int ir = r_to_idx((r[NM]+r[NG])/2);
			if (own(ir)) {
				cplx tlmo = tlm;
				tlm = Tlm[ir][LM(shtns, m_probe,m_probe)];
				diags[0] = -arg(tlm/tlmo)/(dt_log*m_probe);		// omega_drift
				diags[1] = abs(tlm);
				printf(" wdrift(l=m=%d) = %g  (amplitude=%g)\n",m_probe, diags[0], diags[1]);
			}
		}
	}

/** XSHELLS diagnostics:
*  Dissipation rates (kinetic and magnetic)
* */
if (evol_ubt & (EVOL_U|EVOL_B)) {
	const int nlm_align = ((NLM+3)/4)*4;
	cplx* Q = (cplx*) VMALLOC(3*nlm_align*sizeof(cplx));
	cplx* S = Q + nlm_align;	cplx* T = Q + 2*nlm_align;
	if (evol_ubt & EVOL_U) {		// integral of entropy^2 if u=0 at the boundaries.
		double* diags = all_diags.append(1, "D_nu\t ");		// append array for 1 diagnostic
		double Dnu = 0.0;
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			Ulm.curl_QST(ir, Q, S, T);
			double Dnu_r = energy(Q,S,T) * r2dr;
			#ifdef XS_NU_VAR
			Dnu += Dnu_r * nur[ir];
			#else
			Dnu += Dnu_r * jpar.nu;
			#endif
		}
		diags[0] = 2.0*Dnu;
	}
	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(1, "D_eta\t ");		// append array for 1 diagnostic
		double Deta = 0.0;
		for (int ir=Blm.irs; ir<=Blm.ire; ir++) {
			double r2dr = r[ir]*r[ir]*Blm.delta_r(ir);
			Blm.curl_QST(ir, Q, S, T);
			double Deta_r = energy(Q,S,T) * r2dr;
			#ifdef XS_ETA_VAR
			Deta += Deta_r * etar[ir];
			#else
			Deta += Deta_r * jpar.eta;
			#endif
		}
		diags[0] = 2.0*Deta;
	}
	VFREE(Q);
}

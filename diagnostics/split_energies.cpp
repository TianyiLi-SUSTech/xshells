/** XSHELLS diagnostics:
*  split energies (in symmetric, anti-symmetric, poloidal, toroidal, ...)
*/

	if (evol_ubt & EVOL_U) {
		double* diags = all_diags.append(8,		// append array for 8 diagnostics
			"EZu_ps, EZu_pa, EZu_ts, EZu_ta, ENZu_ps, ENZu_pa, ENZu_ts, ENZu_ta\t ");
		Ulm.energy_split(diags);		// store 8 components of the kinetic energy (see energy_split in xshells_spectral.cpp)
	}
	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(8,		// append array for 8 diagnostics
			"EZb_ps, EZb_pa, EZb_ts, EZb_ta, ENZb_ps, ENZb_pa, ENZb_ts, ENZb_ta\t ");
		Blm.energy_split(diags);		// store 8 components of the magnetic energy (see energy_split in xshells_spectral.cpp)
	}

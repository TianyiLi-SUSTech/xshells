/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file surf2vol.cpp
/// Extrapolates surface data (spherical harmonics) to 3D pol/tor data.

#define DEB printf("%s:%u pass\n", __FILE__, __LINE__)
//#define DEB (0)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// FFTW derivative d/dx = ik	(pas de moins !)
#include <fftw3.h>
#include <shtns.h>
#include "grid.cpp"
#include "xshells_spectral.cpp"
#include "xshells_spatial.cpp"
#include "xshells_io.cpp"
#include "xs_surf.c"		/* allow to load files from A. Pais */

double pi = M_PI;

double beta = 0.;			///< 0 = don't correct divergence-free flow. 1 = beta-correction.
double ric = 0.35;			///< radius of inner-core.
//long int nobc = 0;			///< 0 = non-penetration boundary condition.


/*
// compute the spatial field at a given latitude.
void SHtor_to_latcyl(cplx *Tlm, double cost, double *vs, double *vp, double *vz)
{
	double yl[LMAX+1];
	double dtyl[LMAX+1];
	double sint, vtt, vtp;
	cplx *Tl;
	long int l,m,im;

	sint = sqrt(1.0 - cost*cost);
	vtt = 0.; vtp =0.;
	m=0;
		gsl_sf_legendre_sphPlm_deriv_array(LTR, m, cost, &yl[m], &dtyl[m]);
		for (l=m; l<=LTR; l++) {
			vtt += dtyl[l] * creal( Tlm[l] );
		}
	for (im=1; im<=MTR; im++) {
		m = im*MRES;
		gsl_sf_legendre_sphPlm_deriv_array(LTR, m, cost, &yl[m], &dtyl[m]);
		eimp = 2.*cplx(cos(m*phi) , sin(m*phi));
		Ql = &Qlm[LiM(0,im)];	Sl = &Slm[LiM(0,im)];	Tl = &Tlm[LiM(0,im)];
		for (l=m; l<=LTR; l++) {
			vtt += dtyl[l] * creal(Tl[l]*eimp);
			vtp -= (yl[l] *m) * imag(Tl[l]*eimp);
		}
	}
	*vp = vtt*sint;		// vp = dT/dt		vt = i.m/sint *T
	*vs = (vtp/sint)*cost;	// vt*cost
	*vz = -vtp;			// - vt*sint
}
*/

void filter_sym(struct PolTor *Blm, int sym)
{
	long int ir, im, m, l, lm;
	long int ir0, ir1;

	if ((sym != 0) && (sym != 1)) return;
	printf("> symmetry = %d\n",sym);

	ir0 = Blm->irs;	ir1 = Blm->ire;
	for (ir=ir0; ir<=ir1; ir++) {
		for (im=0; im<=MMAX; im++) {
			m = im*MRES;
			for(l=m+1-sym; l<=LMAX; l+=2) {
				lm = LM(shtns,l,m);	Blm->Pol[ir][lm] = 0.0;
			}
			for(l=m+sym; l<=LMAX; l+=2) {
				lm = LM(shtns,l,m);	Blm->Tor[ir][lm] = 0.0;
			}
		}
	}
}

void usage()
{
	printf("\nUsage: surf2vol <stream-function-file> <output-file> [options] \n");
	printf("** list of available options :\n");
	printf(" -lmax=<lmax> : defines the maximum spherical harmonic degree <lmax> of output field\n");
	printf(" -mmax=<mmax> : defines the maximum spherical harmonic orfer <mmax> of output field\n");
	printf(" -nr=<nr> : defines the number <nr> of radial shells\n");
	printf(" -ric=<ric> : set the radius of inner core (0.35 by default)\n");
	printf(" -beta=<beta> : wheight for divergence-free correction. 0=no correction (default), 1=full correction.\n");
	printf(" -scale=<scale> : multiply the data by scale\n");
	printf(" -stream : data is a (pseudo-)stream-function\n");
	printf(" -unit=[km/yr|m.T|pais] : defines the unit of the input field, to convert in non-dimensional ones\n");
}

int main (int argc, char *argv[])
{
	struct VectField V;
	struct PolTor Vlm;
	double *vt, *vp, *vp1;
//	double *strf;
	cplx *tor;
	cplx *pol;
	int lmax_in;
	int i, j, nset;
	int strmf = 0;		// by default it is not a stream-function
	double tmp, scale=1.;
	char name[20], unit[20]="";

	printf(" [SURF2VOL] build %s, %s\n",__DATE__,__TIME__);
	if (argc <= 2) { usage(); exit(1); }

// get lmax from file
	lmax_in = 0;	nset = 0;
	pais_get_lmax(argv[1], &lmax_in, &nset);
	if (nset == 1) strmf = 1;		// only 1 set : it is a stream-function.

// Guess other parameters
	MMAX = lmax_in;
	LMAX = 3*MMAX;
	NLAT = 6*MMAX;		// for good theta resolution.
	NPHI = 2*(MMAX+1);
	NR = 10*MMAX;

// parse options to override (bad!) defaults.
	for (i=3; i<argc; i++) {
		j = sscanf(argv[i],"-%[^=]=%lf",name,&tmp);
		if (j==1) tmp = 1.0;	// no value given : defaults to 1.
//		printf("argv=%s   (matched %d) : name=%s   val=%f\n",argv[i], j, name, tmp);
		if (strcmp(name,"lmax") == 0) LMAX = tmp;
		if (strcmp(name,"mmax") == 0) MMAX = tmp;
		if (strcmp(name,"nr") == 0) NR = tmp;
		if (strcmp(name,"ric") == 0) ric = tmp;
		if (strcmp(name,"beta") == 0) beta = tmp;
		if (strcmp(name,"scale") == 0) scale = tmp;
		if (strcmp(name,"stream") == 0) strmf = tmp;
		if (strcmp(name,"unit") == 0) sscanf(argv[i],"-unit=%s",unit);
	}

	if (NLAT < 2*LMAX) NLAT = 2*LMAX;
	if (MMAX > LMAX) MMAX=LMAX;
	init_sh(sht_quick_init, 1.e-10, 1);
	init_rad_sph(0.0, ric, 1.0, 1.0, 0,0);

// scale and units :
	if (strcmp(unit,"km/yr") == 0) {	// psi has units of km^2/yr => Rcmb^2/(day/2pi)
		scale *= 1./(3480.*3480. * 2*M_PI*365);
		printf(" => converting from %s.\n",unit);
	} else if (strcmp(unit,"pais") == 0) {
		scale *= 1./(3480.* 2*M_PI*365);
		printf(" => converting from %s.\n",unit);
	} else if (strcmp(unit,"m.T") == 0) {
		scale *= 1./(2.*M_PI/(3600*24) * 3.48e6*3.48e6 * sqrt(4.*M_PI*1.e-7 * 1.09e4) );
		printf(" => converting from %s.\n",unit);
	}

	vt = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));		// spatial field.
	vp = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
	pol = (cplx *) VMALLOC(sizeof(cplx)*NLM);
	tor = (cplx *) VMALLOC(sizeof(cplx)*NLM);

	if (strmf) {
		printf(" > data type : streamfunction\n");
		pais_load_sh_text(argv[1], lmax_in, 0, scale, tor, NULL);		// load stream-function from file.
//		strf = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
//		SH_to_spat_l(shtns, tor,(cplx *) strf, MMAX);
//		write_shell2("o_psi",strf,strf);
		SHtor_to_spat_l(shtns, tor, vt, vp, LMAX);		// transform to spatial coordinates.
		for (i=0; i<NPHI; i++) {		// stream-fucntion must be divided by cos(theta)
			for (j=0; j<NLAT; j++) {
				vt[i*NLAT+j] *= 1./ct[j];		vp[i*NLAT+j] *= 1./ct[j];
			}
		}
		if (beta != 0) {		// correct for non divergence-free flow...
			vp1 = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
			SH_to_spat_l(shtns, tor, vp1, LMAX);
			for (i=0; i<NPHI; i++) {
				for (j=0; j<NLAT; j++) {
//					if (st[j] < 0.99)
						vp[i*NLAT+j] -= -st[j]*beta/(ct[j]*ct[j]) * vp1[i*NLAT+j];
				}
			}
		}
	} else {
		printf(" > data type : poloidal/toroidal component\n");
		pais_load_sh_text(argv[1], lmax_in, 0, scale, pol, NULL);		// load poloidal from file.
		pais_load_sh_text(argv[1], lmax_in, 1, scale, tor, NULL);		// load toroidal
		SHsphtor_to_spat_l(shtns, pol,tor,vt,vp, LMAX);		// transform to spatial coordinates.
	//	int lm,l;
	//	LM_L_LOOP( tor[lm] *= l2[lm]; )	// vorticity
	//	SH_to_spat_l(shtns, pol, vt, MMAX);
	//	SH_to_spat_l(shtns, tor, vp, MMAX);
	}

// write spatial surface data :
	write_shell2("o_shell.surf", vt, vp);
	printf("> Surface vector field from original data is written to o_shell.surf\n");

	Vlm.alloc(NG,NM);
	V.alloc(NG,NM);
	vect_surf_to_3D(vt, vp, &V, ric);

	spat_to_PolSphTor(&V, Vlm.Pol, NULL, Vlm.Tor, NG, NM);
	Vlm.save_single(argv[2],LMAX, MMAX);
	printf("3D field saved to file '%s'\n",argv[2]);
}

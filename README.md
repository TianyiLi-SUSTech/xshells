**XSHELLS is a high performance simulation code for the rotating Navier-Stokes equation in spherical shells,
optionally coupled to the induction and temperature equation.**

Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
XSHELLS is distributed under the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible) located in the LICENSE file.


FEATURES:
---------

- Designed for speed, it is probably the **fastest spherical MHD simulation code**.
- Uses finite differences in radial direction, and spherical harmonic expansion.
- Low memory footprint; can handle huge spherical harmonic resolutions.
- Written in C++ with efficiency, extensibility and customizability in mind.
- Export data to python/matplotlib and paraview.
- Can time-step various initial value problems in spherical shells or full spheres:
  geodynamo, spherical Couette flow, precession, inertial waves, torsional Alfvén waves,
  double-diffusive convection...
- Can run on a laptop or on massively parallel supercomputers
  (hybrid parallelization using OpenMP and/or MPI, scales well up to 1 process/shell).
- [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible with GNU GPL):
  everybody is free to use, modify, contribute.


INSTALL:
--------

If you accept the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible french License), you can [download XSHELLS](https://bitbucket.org/nschaeff/xshells/downloads).

The [SHTns library](https://bitbucket.org/nschaeff/shtns/) is required for spherical harmonic transforms.

Briefly, the shell command `./configure` will prepare the program for compilation, while
`./configure --help` will list available options (among which `--disable-openmp` and `--disable-mpi`).

Example problems are located in the `problems` directory.
For example, to build executables for the geodynamo benchmark (Christensen et al PEPI 2011):

    cp problems/geodynamo/* .
    make all

Adjust the parameters found in the `xshells.par` file, which will
be read by program at startup.
Depending on what you want you can then run:

  - `./xsbig` for the simple OpenMP version
  - `./xsbig_mpi` for the pure MPI code
  - `./xsbig_hyb` for the hybrid OpenMP-MPI executable.
  - `./xsbig_hyb2` for the radial MPI + in-shell OpenMP executable.
  - `./xsimp` for the OpenMP version with Coriolis force handled implicitely (beta).

Read the user manual for more details.


DOCUMENTATION:
--------------

- A user manual is available [online](https://nschaeff.bitbucket.io/xshells/manual.html)
  or for [download (pdf)](https://bitbucket.org/nschaeff/xshells/downloads).
  The latex source of the manual can be found in the `notes` directory.
- In addition, the main source files are documented using Doxygen comments.
  Run `make docs` to generate the documentation targeted at developers and contributors.


CHANGE LOG:
-----------

* v2.5.2  (25 Dec 2020)
	- Fix major bug introduced in v2.5.
	- Fix bug arising on PowerPC (where char is unsigned by default).

* v2.5.1  (13 Oct 2020)  [**BUGGY, don't use!**]
	- Fix compilation issues
	- ./configure now honors the MPICXX environement variable
	- xsplot can now handle line profiles from xspp.

* v2.5  (1 Oct 2020)  [**BUGGY, don't use!**]
	- Optimization of memory accesses. 10 to 30 % faster with SHTns v3.4 or later.
	- Experimental MPI-3 shared memory mode (not fully standard compliant);
	  add `-shared_mem=N` to command line to allow processes to share memory across N cores.
	- Experimental bidirectional solve to speed-up MPI solves; add `-bidir_solve=1`.
	- pyxshells.py supports reading field files with fp48 compressed data.
	- small bug fixes.

* v2.4  (28 Nov 2019)
	- New available time steppers: PC2 (default), CNAB2, SBDF2, SBDF3 and BPR353.
	- Many fixes and improvements.

* v2.3.1  (25 Sep 2019)
	- Fix configuration and compilation issues.
	- Improved testing script (run `test.py`).
	- New control variables `sconv_stop` and `omp_tasks`, see manual.
	- Other minor fixes and cleanups.

* v2.3  (27 Aug 2019)
	- New testing framework (run `test.py` or `make test`).
	- New FP48 compression of floating point data (optional).
	- New default CFL safety factors for magnetic field, allowing smooth
	  transient in the geodynamo benchmark.
	- New support for mean-field dynamo with axisymmetric alpha effect (see
	  `problems/kinematic_dynamos`, thanks to C. Hardy).
	- Color in console output to highlight warnings and errors.
	- Computing in a full sphere now only allowed if enabled at compile time.
	- Code cleanup and internal improvements.
	- Some minor bugfixes.

* v2.2.3  (11 Jun 2019)
	- Fix computation of electric current at outer insulating boundary.
	- Setting hyperviscosity now only allowed if enabled at compile time.
	- Documentation update.

* v2.2.2  (20 Mar 2019)
	- Fix configure script to include optimization options correctly. 

* v2.2.1  (8 Mar 2019)
	- Easier configuration and install, including SHTns library configuration 
          and compilation, better intel MKL detection, and better intel KNL support.

* v2.2  (31 Oct 2018)
	- several optimizations & fixes.
	- diagnostics are now code snippets found in the diagnostics directory, for
          better code reuse and maintenance; all xshells.hpp files have been updated.
	- all parameters of xshells.par can be overridden by command line arguments.
	- OpenMP 4 tasks are now used when it results in better load balancing.
	- Smarter distribution of shells between MPI processes with solid shells.
	- xsplot.py can now handle energy files with diagnostics added between restarts.
	- pyxshells.py can read Parody restart files (and convert them to xshells).

* v2.1  (16 May 2018)
	- several minor bug fixed.
	- improved python scripts; added xspeek.py to pretty display the energy files.
	- hyper-diffusivity can also be applied on the magnetic field.

* v2.0  (4 Dec 2017)
	- *warning*: previous xshells.hpp may not compile as-is.
	- new: time-stepping scheme switched to Predictor-Corrector type allowing
          3 to 4 times larger time-steps (change xshells.par accordingly!).
	- new: support for double-diffusive convection.
	- fixed and improved linear mode, which now allows full computation of
	  perturbations around base-fields (optionally including non-linear terms).
	- support for AVX-512 vectorization (e.g. intel KNL).
	- new `xsimp` handles Coriolis force implicitely (beta).
	- internal changes in banded matrix handling.
	- `xspp` writes numpy files instead of text files for most slices.
	- improved plotting system: use xsplot to display almost all outputs from xspp.

* v1.4  (30 Sep 2016)
	- spectral convergence for each field stored in energy file.
	- drop the `time_scheme` option and the obsolete and poor corrector mode.
	- fix broken linear mode (without magnetic field).
	- improvments to pyxshells module (by E. Kaplan).
	- fix bug in z-averaging (thanks to E. Kaplan).
	- other minor fixes and improvements

* v1.3  (8 Mar 2016)
	- better spectral convergence (Sconv) check (max of all shells).
	- spectral convergence checks adjusted from xshells.par with
	  variables `sconv_lmin` and `sconv_mmin`.
	- variable l-truncation controlled from xshells.par using `rsat_ltr`.
	- fix sign of rotation vector along y.
	- fix bugs appearing when using MPI and few shells per process.
	- other bugfixes and improvements, especially to plotting scripts.

* v1.2  (6 Oct 2015)
	- new parallelization mode using OpenMP in the angular directions:
	  compile and run `xsbig_hyb2`.
	- arbitrary gravity fields allowed.
	- safer default CFL safety factors.
	- real-time spectral convergence check.
	- full linear mode (now supporting non-axisymmetric base fields).
	- user defined bulk forcings (example in problems/tidal).
	- fixes, improvements, optimizations.

* v1.1  (15 Jul 2015)
	- global rotation no longer required to be along z-axis.
	- real-time diagnostic plotting (using gnuplot).
	- at least 1 radial shell per process required (instead of 2 before).
	- new real-time test for spectral convergence.
	- many fixes, improvements and optimizations.

* v1.0  (3 Jan 2015)
	- first public release.
	- hybrid OpenMP/MPI parallelization.
	- extensible diagnostic system.


/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file sh2spat.cpp
/// work with spherical harmonics surface data.

#define DEB printf("%s:%u pass\n", __FILE__, __LINE__)
//#define DEB (0)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex>
#include <math.h>

// FFTW derivative d/dx = ik	(pas de moins !)
#include <fftw3.h>
#include <shtns.h>
#include "grid.cpp"
#define XS_SURF_PAIS		/* allow to load files from A. Pais */
#include "xshells_spectral.cpp"
#include "xshells_spatial.cpp"
#include "xshells_io.cpp"
#include "xs_surf.c"

double pi = M_PI;

double beta = 0.;			///< 0 = don't correct divergence-free flow. 1 = beta-correction.
double ric = 0.35;			///< radius of inner-core.
//long int nobc = 0;			///< 0 = non-penetration boundary condition.


/// write 2D surface vector (on a spherical shell)
void write_shell2(char *fn, double *vt, double *vp)
{
	FILE *fp;
	long int i,j,k;	//phi, theta

	fp = fopen(fn,"w");
	fprintf(fp,"%% [SURF2VOL] Surface data (sphere, r=1). first line is (theta 0 0), first row is phi, then for each point, (r,theta,phi) components are stored together.\n0 ");
		for(j=0;j<NLAT;j++) {
			fprintf(fp,"%.6g 0 0 ",acos(ct[j]));	// first line = theta (radians)
		}
	for (i=0; i<NPHI; i++) {
		fprintf(fp,"\n%.6g ",2.*pi*i/(NPHI*MRES));		// first row = phi (radians)
		for(j=0; j<NLAT; j++) {
			k = i*NLAT+j;
			fprintf(fp,"0 %.6g %.6g  ",vt[k],vp[k]);		// data
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}


/// apply (l,m) filter
void filter_lm(cplx *sh, int lmin, int lmax, int mmin, int mmax)
{
	long int im, m, l, lm;

	if (lmax > LMAX) lmax = LMAX;	if (mmax > MMAX) mmax = MMAX;

	for (im=0; im<mmin; im++) {
		m = im*MRES;
		for(l=m; l<=LMAX; l++) {
			lm = LM(l,m);	sh[lm] = 0.0;
		}
	}
	for (im=mmin; im<=mmax; im++) {
		m = im*MRES;
		for(l=m; l<lmin; l++) {
			lm = LM(l,m);	sh[lm] = 0.0;
		}
		for(l=lmax+1; l<=LMAX; l++) {
			lm = LM(l,m);	sh[lm] = 0.0;
		}
	}
	for (im=mmax+1; im<=MMAX; im++) {
		m = im*MRES;
		for(l=m; l<=LMAX; l++) {
			lm = LM(l,m);	sh[lm] = 0.0;
		}
	}
}

/// keep only symmetric or anti-symmetric part (with respect to the equator).
/// sym = 1 : anti-symmetric (odd). sym = 0 : symmetric (even)
void filter_sym(cplx *sh, int sym)
{
	long int im, m, l, lm;

	if ((sym != 0) && (sym != 1)) return;
	printf("> symmetry = %d\n",sym);

	for (im=0; im<=MMAX; im++) {
		m = im*MRES;
		for(l=m+1-sym; l<=LMAX; l+=2) {
			lm = LM(l,m);	sh[lm] = 0.0;
		}
	}
}

void usage()
{
	printf("\nUsage: surf2vol <stream-function-file> <output-file> [options] \n");
	printf("** list of available options :\n");
	printf(" -llim=<lmin:lmax> : defines the range of harmonic degree for output field\n");
	printf(" -mlim=<mmin:mmax> : defines the range of harmonic order for output field\n");
	printf(" -ric=<ric> : set the radius of inner core (0.35 by default)\n");
	printf(" -beta=<beta> : wheight for divergence-free correction. 0=no correction (default), 1=full correction.\n");
	printf(" -scale=<scale> : multiply the data by scale\n");
	printf(" -stream : data is a (pseudo-)stream-function\n");
	printf(" -unit=[km/yr|m.T|pais] : defines the unit of the input field, to convert in non-dimensional ones\n");
}

int main (int argc, char *argv[])
{
	double *vt, *vp, *vp1;
	cplx *tor;
	cplx *pol;
	int lmax_in, lmin, lmax, mmin, mmax;
	int i, j, nset;
	int calc_vort = 0;	// by default compute velocity
	int strmf = 0;		// by default it is not a stream-function
	double tmp, tmp2, scale=1.;
	char name[20], unit[20]="";

	printf(" [sh2spat] build %s, %s\n",__DATE__,__TIME__);
	if (argc <= 2) { usage(); exit(1); }

// get lmax from file
	lmax_in = 0;	nset = 0;
	pais_get_lmax(argv[1], &lmax_in, &nset);
	if (nset == 1) strmf = 1;		// only 1 set : it is a stream-function.

// Guess other parameters
	MMAX = lmax_in;
	LMAX = lmax_in;
	NLAT = 6*LMAX+1;		// for good theta resolution.
	NPHI = 6*MMAX;
	lmin = 0;	lmax = LMAX;
	mmin = 0;	mmax = MMAX;

// parse options to override (bad!) defaults.
	for (i=3; i<argc; i++) {
		j = sscanf(argv[i],"-%[^=]=%lf:%lf",name,&tmp,&tmp2);
		if (j==1) tmp = 1.0;	// no value given : defaults to 1.
//		printf("argv=%s   (matched %d) : name=%s   val=%f\n",argv[i], j, name, tmp);
		if (strcmp(name,"llim") == 0) { lmin = tmp;	lmax = tmp2; }
		if (strcmp(name,"mlim") == 0) { mmin = tmp;	mmax = tmp2; }
		if (strcmp(name,"ric") == 0) ric = tmp;
		if (strcmp(name,"beta") == 0) beta = tmp;
		if (strcmp(name,"scale") == 0) scale = tmp;
		if (strcmp(name,"stream") == 0) strmf = tmp;
		if (strcmp(name,"unit") == 0) sscanf(argv[i],"-unit=%s",unit);
		if (strcmp(name,"vort") == 0) calc_vort = tmp;
	}

	init_sh(sht_reg_poles, 1.e-10, 1);

// scale and units :
	if (strcmp(unit,"km/yr") == 0) {	// psi has units of km^2/yr => Rcmb^2/(day/2pi)
		scale *= 1./(3480.*3480. * 2*M_PI*365);
		printf(" => converting from %s.\n",unit);
	} else if (strcmp(unit,"pais") == 0) {
		scale *= 1./(3480.* 2*M_PI*365);
		printf(" => converting from %s.\n",unit);
	} else if (strcmp(unit,"m.T") == 0) {
		scale *= 1./(2.*M_PI/(3600*24) * 3.48e6*3.48e6 * sqrt(4.*M_PI*1.e-7 * 1.09e4) );
		printf(" => converting from %s.\n",unit);
	}

	vt = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));		// spatial field.
	vp = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
	pol = (cplx *) VMALLOC(sizeof(cplx)*NLM);
	tor = (cplx *) VMALLOC(sizeof(cplx)*NLM);

	if (strmf) {
		printf(" > data type : streamfunction\n");
		pais_load_sh_text(argv[1], lmax_in, 0, scale, tor, NULL);		// load stream-function from file.

		filter_lm(tor, lmin, lmax, mmin, mmax);

//		strf = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
//		SH_to_spat_l(shtns, tor,(cplx *) strf, MMAX);
//		write_shell2("o_psi",strf,strf);
		SHtor_to_spat_l(shtns, tor, vt, vp, LMAX);		// transform to spatial coordinates.
		for (i=0; i<NPHI; i++) {		// stream-fucntion must be divided by cos(theta)
			for (j=0; j<NLAT; j++) {
				vt[i*NLAT+j] *= 1./ct[j];		vp[i*NLAT+j] *= 1./ct[j];
			}
		}
		if (beta != 0) {		// correct for non divergence-free flow...
			vp1 = (double *) VMALLOC(NSPAT_ALLOC(shtns) * sizeof(double));
			SH_to_spat_l(shtns, tor, vp1, LMAX);
			for (i=0; i<NPHI; i++) {
				for (j=0; j<NLAT; j++) {
//					if (st[j] < 0.99)
						vp[i*NLAT+j] -= -st[j]*beta/(ct[j]*ct[j]) * vp1[i*NLAT+j];
				}
			}
		}
	} else {
		printf(" > data type : poloidal/toroidal component\n");
		pais_load_sh_text(argv[1], lmax_in, 0, scale, pol, NULL);		// load poloidal from file.
		pais_load_sh_text(argv[1], lmax_in, 1, scale, tor, NULL);		// load toroidal

		filter_lm(tor, lmin, lmax, mmin, mmax);
		filter_lm(pol, lmin, lmax, mmin, mmax);

		if (calc_vort) {
			int lm;
			LM_LOOP( tor[lm] *= l2[lm]; )	// vorticity
			SH_to_spat(shtns, pol, vt);
			SH_to_spat(shtns, tor, vp);
		} else {
			SHsphtor_to_spat(shtns, pol,tor,vt,vp);		// transform to spatial coordinates.
		}
	}

	write_shell2(argv[2], vt, vp);
	printf("> Surface vector field from original data is written to %s\n",argv[2]);

}

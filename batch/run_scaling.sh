
## the template batch scheduler script
template=job_ompi.knl_template
## the file in which the batch scheduler script will be written
curjob=job_tmp
## the command to launch the batch scheduler
submit=ccc_msub

## number of radial shells to avoid too many processes.
NR=336
## cores per node:
cores_per_node=64

for exe in ..\\/xsbig_hyb2_mpi3shmem_knl ..\\/xsbig_hyb_mpi3shmem_knl
do
  for nt in 4 8 16
  do
    for nn in 1 2 4 7 8 12 14 16 24 32 48 64
    do
       np=`echo "$nn * $cores_per_node / $nt" | bc`
       if (( np <= NR )); then
           echo $nn $np $nt
           sed $template -e "s/%NODES/$nn/" -e "s/%THREADS/$nt/" -e "s/%PROCS/$np/" -e "s/%EXE/$exe/" > $curjob
           ccc_msub $curjob
       fi
    done
  done
done


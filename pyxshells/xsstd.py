import matplotlib
matplotlib.use("Agg")   # allow to run without X

from pylab import *
from numpy import *
from pyxshells import *
import shtns
import xsplot
import sys

if len(sys.argv) < 6:
    print('  usage: %s base ext istart iend istep [field_avg]' % sys.argv[0])
    print('    example: %s fieldU_ job3 0 100 1' % sys.argv[0])
    exit()

base = sys.argv[1] # 'fieldU_'
ext = sys.argv[2]  # 'st1ns_hr2'
istart = int(sys.argv[3])  # 37
iend = int(sys.argv[4])    # 423
istep = int(sys.argv[5])   # 1
Alm = None
if len(sys.argv) > 6:
	favg = sys.argv[6] # average field to be removed
	Alm = load_field( favg )		# load average field (to be subtracted)
	#nr = Alm.ire - Alm.irs + 1
	print("=> Average field 'favg' will be subtracted")

def save_merid(filename, r, ct, V, fchar='X'):
	b = zeros((1, len(r)+1, len(ct)+1))
	b[0,1:,0] = r
	b[0,0,1:] = arccos(ct)
	b[0,1:,1:] = V
	b[0,0,0] = (ord(fchar)-64)*16 + 15   # magic value decoded into plot title
	save(filename, b)


info,r =get_field_info('%s%04d.%s' % (base,istart,ext))
sh = shtns.sht(info['lmax'], info['mmax'], info['mres'])
nr = info['nr']
sh.set_grid(nl_order=2, flags=shtns.sht_gauss)
sh.print_info()
Y00_1 = sh.sh00_1()
l2 = sh.l*(sh.l+1)

U2avg = zeros((nr, sh.nlat))
count = 0
for i in range(istart, iend+1, istep):
	try:
		Ulm = load_field( '%s%04d.%s' % (base,i ,ext), lazy=False)
	except FileNotFoundError:
		print('[%04d] missing file' % i)
	else:
		print('[%04d]\r' % i)
		count += 1
		# compute fluctuations
		if Alm is not None:
			Ulm.data -= Alm.data		# subtract average
		for ir in range(Ulm.irs, Ulm.ire+1):
			if Ulm.ncomp() == 2:
				ur_lm = Ulm.rad(ir).astype(complex128)
				us_lm = Ulm.sph(ir).astype(complex128)
				ut_lm = Ulm.tor(ir).astype(complex128)
				ur,ut,up = sh.synth(ur_lm, us_lm, ut_lm)
				er = square(ur) + square(ut) + square(up)	# energy of fluctuations
			else:
				s_lm = Ulm.sh(ir).astype(complex128)
				s = sh.synth(s_lm)
				er = square(s)       # energy of fluctuations
			U2avg[ir-Ulm.irs,:] += mean(er,axis=1)

print("%d files averaged" % count)

Urms = sqrt(U2avg/count)

save_merid('o_merid_rms.npy',r,sh.cos_theta, Urms, base[5])


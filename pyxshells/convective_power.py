from numpy import *
from pyxshells import *
import shtns

ext = 'bench'
istart = 1
iend = 10

info=get_field_info('fieldU_%04d.%s' % (istart,ext))
info=info[0]
sh = shtns.sht(info['lmax'], info['mmax'], info['mres'])
Y00_1 = sh.sh00_1()

T0lm = load_field('fieldT0.%s' % ext)
C0lm = load_field('fieldC0.%s' % ext)
Phi0 = load_field('fieldPhi0.%s' % ext)
# compute gravity field
grav = zeros(Phi0.ire+1)
T0sh = zeros(Phi0.ire+1)
C0sh = zeros(Phi0.ire+1)
for ir in range(Phi0.irs, Phi0.ire+1):
	grav[ir] = Phi0.grad_r(ir)[0].real / Y00_1
	T0sh[ir] = T0lm.sh(ir)[0].real
	C0sh[ir] = T0lm.sh(ir)[0].real
del Phi0
del T0lm
del C0lm

def coenergy(alm, blm):
	v2 = real(alm)*real(blm) + imag(alm)*imag(blm)
	v2[sh.m==0] *= 0.5          # count m>0 twice (+m and -m)
	return sum(v2)

Power = array([])
for i in range(istart, iend+1):
	try:
		Ulm = load_field( 'fieldU_%04d.%s' % (i,ext))
		#Blm = load_field( 'fieldB_%04d.%s' % (i,ext))
		Tlm = load_field( 'fieldT_%04d.%s' % (i,ext))
		Clm = load_field( 'fieldC_%04d.%s' % (i,ext))
	except FileNotFoundError:
		print('[%04d] missing file' % i)
	else:
		# compute convective power
		Pw,Pw_T,Pw_C = 0.,0.,0.
		for ir in range(Ulm.irs, Ulm.ire+1):
			ur_lm = Ulm.rad(ir).astype(complex128)
			t_lm = Tlm.sh(ir).astype(complex128)
			c_lm = Clm.sh(ir).astype(complex128)
			t_lm[0] += T0sh[ir]			# add base temperature profile
			c_lm[0] += C0sh[ir]			# add base temperature profile
			CoE_T = coenergy(ur_lm, t_lm)
			CoE_C = coenergy(ur_lm, c_lm)
			CoE = coenergy(ur_lm, t_lm+c_lm)
			Pw += Ulm.delta_r(ir)*Ulm.grid.r[ir]**2 * CoE * grav[ir]
			Pw_T += Ulm.delta_r(ir)*Ulm.grid.r[ir]**2 * CoE_T * grav[ir]
			Pw_C += Ulm.delta_r(ir)*Ulm.grid.r[ir]**2 * CoE_C * grav[ir]
		Pw *= 2 / Ulm.shell_volume()
		Pw_T *= 2 / Ulm.shell_volume()
		Pw_C *= 2 / Ulm.shell_volume()
		print('[%04d] Pw = %g (Pw_T = %g,  Pw_C = %g)' % (i,Pw, Pw_T, Pw_C))
		Power = append(Power, array([Pw,Pw_T,Pw_C]))

Power = Power.reshape(-1,3)
savetxt('power_%04d-%04d.%s' % (istart,iend,ext), Power, fmt='%.9g')

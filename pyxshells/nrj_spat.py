#!/usr/bin/python

## Computes energy of a collection of fields, in the spatial domain. ##

from numpy import *
from pyxshells import *
import shtns
import sys

if len(sys.argv) != 6:
    print('  usage: %s base ext istart iend istep' % sys.argv[0])
    exit()

base = sys.argv[1] # 'fieldU_'
ext = sys.argv[2]  # 'st1ns_hr2'
istart = int(sys.argv[3])  # 37
iend = int(sys.argv[4])    # 423
istep = int(sys.argv[5])   # 1

delta = 0.1     # 10% of radial size in each "boundary layer"

info, r = get_field_info('%s%04d.%s' % (base,istart,ext))
sh = shtns.sht(info['lmax'], info['mmax'], info['mres'])
sh.set_grid(nl_order=2)
l2 = (sh.l+1.0)*sh.l
Y00_1 = sh.sh00_1()
sintheta = sqrt(1.0 - square(sh.cos_theta))
wg_2 = sh.gauss_wts()       # Gauss weights
wg = zeros(wg_2.size*2)
wg[0:wg_2.size] = wg_2
wg[wg_2.size:] = wg_2[::-1]

nfields = (iend-istart)//istep + 1

nrj_bli = zeros(nfields)
nrj_blo = zeros(nfields)
nrj_tc = zeros(nfields)
nrj_out = zeros(nfields)

for k in range(0, nfields):
    sys.stdout.write('load file %d...\r' % k)
    sys.stdout.flush()
    Flm = load_field( '%s%04d.%s' % (base,istart+k*istep ,ext))
    r = Flm.grid.r
    ric = 0.35*r[-1]        # inner-core radius
    rbl_in = r[0] + delta*r[-1]
    rbl_out = r[-1] - delta*r[-1]

    # shell by shell computation (to avoid large memory needs)
    for ir in range(Flm.irs+1, Flm.ire):        # skip first and last shells.
        rr = r[ir]
        r2dr = rr**2*Flm.delta_r(ir)
        if Flm.ncomp() == 2:
            ur_lm = Flm.rad(ir).astype(complex128)
            us_lm = Flm.sph(ir).astype(complex128)
            ut_lm = Flm.tor(ir).astype(complex128)
            ur,ut,up = sh.synth(ur_lm, us_lm, ut_lm)
            espat = square(ur) + square(ut) + square(up)    # energy
        else:
            s_lm = Flm.sh(ir).astype(complex128)
            s = sh.synth(s_lm)
            espat = square(s)       # energy
        Eavg = mean(espat,axis=1)   # average along phi
        Eavg *= (r2dr*pi/sh.nphi)*wg        # multiply by Gauss weights for integration

        if rr < rbl_in:
            nrj_bli[k] += sum(Eavg) # the whole shell contributes to boundary layers
        elif rr > rbl_out:
            nrj_blo[k] += sum(Eavg)
        else:
            s = rr * sintheta
            nrj_tc[k] += sum(Eavg * (s <= ric))
            nrj_out[k] += sum(Eavg * (s > ric))

    print("energy in inner bl = %g,  outer bl = %g,  inside tc = %g,  outside tc = %g" % (nrj_bli[k], nrj_blo[k], nrj_tc[k], nrj_out[k]))

print('\ndone')

arr = transpose(vstack((nrj_bli, nrj_blo, nrj_tc, nrj_out)))
savetxt('%s%04d-%04d-%04d_Spec_t_split.%s' % (base,istart,iend,istep,ext), arr)

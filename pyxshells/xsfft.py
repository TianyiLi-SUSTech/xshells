#!/usr/bin/python

## Performs temporal fft of field collection, without loading everything in memory (out-of-core). ##
## outputs same thing as xsfft ##

from numpy import *
from pyxshells import *
import sys

if len(sys.argv) != 6:
    print('  usage: %s base ext istart iend istep' % sys.argv[0])
    exit()

base = sys.argv[1] # 'fieldU_'
ext = sys.argv[2]  # 'st1ns_hr2'
istart = int(sys.argv[3])  # 37
iend = int(sys.argv[4])    # 423
istep = int(sys.argv[5])   # 1


info, r = get_field_info('%s%04d.%s' % (base,istart,ext))
nlm = info['nlm']
t0 = info['time']
info, r = get_field_info('%s%04d.%s' % (base,istart+istep,ext), disp=False)
dt = info['time'] - t0

# open all files (out of core) and create new files to store the FFT
V = []
F = []
ii = 0
t0 = t0 - dt
for i in range(istart, iend+1, istep):
    sys.stdout.write('load file %d...\r' % (i-istart))
    sys.stdout.flush()
    info, r2 = get_field_info('%s%04d.%s' % (base,i,ext), disp=False)
    if any(r2!=r):
        raise RuntimeError('mismatching fields')
    t = info['time']
    if abs(t-t0-dt) > dt*1e-6:
        raise RuntimeError('fields are not equispaced in time')
    t0 = t
    a = load_field( '%s%04d.%s' % (base,i,ext))
    b = clone_field(a, filename='%spyF%04d.%s' % (base,ii,ext), dtype=complex64 )
    ii += 1 
    V.append( a )
    F.append( b )

Nfft = len(V)
print('\ndone loading %d files' % Nfft)

irs = V[0].irs
ire = V[0].ire
ncomp = V[0].ncomp()

### Shell by shell fft ###
for ir in range(irs, ire+1):        # all shells
    sys.stdout.write('shell %d of %d...\r' % (ir,ire))
    sys.stdout.flush()

    for ic in range(0,ncomp):
        data = zeros((Nfft, nlm), dtype=complex64)
        for k in range(0,Nfft):                 # all times
            data[k,:] = V[k].sh(ir,ic)

        data = fft.fft(data, axis=0)        # temporal fft

        for k in range(0,Nfft):                                 # all frequencies
                        F[k].sh(ir,ic)[:] =  data[k,:]/Nfft

### spectrum:
Spec_t = zeros(Nfft)
freq = zeros(Nfft)
df = 1.0/(dt*Nfft)

ii=0
for k in range(Nfft//2+1, Nfft):    # negative frequencies
    f = df*(k-Nfft)
    freq[ii] = f
    Spec_t[ii] = F[k].energy()
    ii += 1
for k in range(0, Nfft//2+1):       # positive frequencie
    f = df*k
    freq[ii] = df*k
    Spec_t[ii] = F[k].energy()
    ii += 1

print('\ndone')

savetxt('%s%04d-%04d-%04d_Spec_t.%s' % (base,istart,iend,istep,ext), transpose(vstack((freq,Spec_t))))

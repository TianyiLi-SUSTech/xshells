/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/* field rendering */

/// render a meridional slice, at longitude phi, with a low memory requirement.
void PolTor::to_merid(double phi, double *vr, double *vt, double *vp, int irs, int ire) const
{
  #pragma omp parallel
  {
	int ir;
	cplx *Q, *S, *T;
	double *Vrr, *Vtr, *Vpr;		// radial shells.
	long nspat = shtns->nspat;

	#pragma omp critical
	{
	  Q = (cplx*) VMALLOC(3*NLM*sizeof(cplx));
	  Vrr = (double*) VMALLOC(3*nspat*sizeof(double));		// alloc memory for one shell.
	}
	S = Q + NLM;		T = Q + 2*NLM;
	Vtr = Vrr + nspat;		Vpr = Vrr + 2*nspat;

	#pragma omp for schedule(dynamic)
	for (ir = irs; ir <= ire; ir++) {
		RadSph(ir, Q, S );
		SH_Zrotate(shtns, Q, -phi, Q);
		SH_Zrotate(shtns, S, -phi, S);
		SH_Zrotate(shtns, Tor[ir], -phi, T);
		SHV3_SPAT(Q, S, T, Vrr, Vtr, Vpr);
		for (int it=0; it<NLAT; it++) {		// copy first meridional slice.
			vr[(ir-irs)*NLAT +it] = Vrr[it];
			vt[(ir-irs)*NLAT +it] = Vtr[it];
			vp[(ir-irs)*NLAT +it] = Vpr[it];
		}
	}
	VFREE(Vrr);		VFREE(Q);
  }
}

void ScalarSH::to_grad_merid(double phi, double *vr, double *vt, double *vp, int irs, int ire) const
{
	int ir;
	cplx *Q, *S;
	double *Vrr, *Vtr, *Vpr;		// radial shells.

	Q = (cplx*) VMALLOC(2*NLM*sizeof(cplx));
	S = Q + NLM;
	const long nspat = shtns->nspat;
	Vrr = (double*) VMALLOC(3*nspat*sizeof(double));		// alloc memory for one shell.
	Vtr = Vrr + nspat;		Vpr = Vrr + 2*nspat;

	for (ir = irs; ir <= ire; ir++) {
		Gradr(ir, Q, S );
		SH_Zrotate(shtns, Q, -phi, Q);
		SH_Zrotate(shtns, S, -phi, S);
		SH_SPAT(Q, Vrr);
		SHS_SPAT(S, Vtr, Vpr);
		for (int it=0; it<NLAT; it++) {		// copy first meridional slice.
			vr[it] = Vrr[it];		vt[it] = Vtr[it];		vp[it] = Vpr[it];
		}
		vr+=NLAT;	vt+=NLAT;	vp+=NLAT;
	}
	VFREE(Vrr);		VFREE(Q);
}


/// render a spherical surface.
void PolTor::to_surf(int ir, double *vr, double *vt, double *vp) const
{
	cplx *Q, *S;
	const long nspat = shtns->nspat;

	if ((ir < irs) || (ir > ire)) {
		memset(vr, 0, nspat*sizeof(double));
		memset(vt, 0, nspat*sizeof(double));
		memset(vp, 0, nspat*sizeof(double));
		return;
	}

	Q = (cplx*) VMALLOC(2*NLM*sizeof(cplx));
	S = Q + NLM;

	RadSph(ir, Q, S );
	SHV3_SPAT(Q, S, Tor[ir], vr, vt, vp);
	VFREE(Q);
}

int PolTor::to_zdisc(double z0, int nphi, double* s, double* vs, double *vp, double *vz, int ir0, int ir1) const
{
	double rr, cost, sint, ss;
	double *vrr;		// vector
	double *vtt;		// vector
	double *vpp;		// vector
	int ir;

	rr = z0;
	if (z0 < 0.0) rr = -z0;
	ir = r_to_idx(rr);	if (r[ir] <= rr) ir++;	//r[ir] > rr;
	if (ir0<ir) ir0=ir;
	if (ir0<irs) ir0=irs;		// skip undef data.
	if (ire<ir1) ir1 = ire;// skip undef data.

	vrr = (double *) VMALLOC( (nphi+2)*sizeof(double) );
	vtt = (double *) VMALLOC( (nphi+2)*sizeof(double) );
	vpp = (double *) VMALLOC( (nphi+2)*sizeof(double) );

	for (int ir = ir0; ir<=ir1; ir++) {
		rr = r[ir];
		ss = sqrt(rr*rr - z0*z0);		// disc radius.
		*s++ = ss;
		cost = z0/rr;		sint = sqrt((1.-cost)*(1.+cost));
		to_lat(ir, cost, vrr,vtt,vpp, nphi);
		for (int ip=0; ip<nphi; ip++) {
			vp[ip] = vpp[ip];
			vs[ip] = vrr[ip]*sint + vtt[ip]*cost;
			vz[ip] = vrr[ip]*cost - vtt[ip]*sint;
		}
		vs += nphi;
		vp += nphi;
		vz += nphi;
	}
	VFREE(vpp);
	VFREE(vtt);
	VFREE(vrr);
	return(ir1-ir0+1);		// return number of radii rendered.
}

/// evaluates spatial field at given latitude and nphi regularly spaced longitudes.
void PolTor::to_lat(int ir, double cost,
					 double *vr, double *vt, double *vp, int np) const
{
	cplx *Q;	// l(l+1) * Pol/r
	cplx *S;	// dP/dr + Pol/r = 1/r.d(rP)/dr = Wr(Pol)

	Q = (cplx*) VMALLOC(sizeof(double)*4*NLM);
	S = Q + NLM;

	RadSph(ir, Q, S);
	SHqst_to_lat(shtns, Q,S,Tor[ir], cost, vr, vt, vp, np, LMAX, MMAX);

	VFREE(Q);
}


/// evaluates spatial field at given latitude and nphi regularly spaced longitudes.
void ScalarSH::to_lat(int ir, double cost, double *s, int np) const
{
	cplx *QS;		// dummy zero fields.
	double *vt, *vp;

	QS = (cplx*) VMALLOC(sizeof(double)*2*(NLM + np+2));
	vt = (double*) (QS + NLM);
	vp = vt + np+2;

	memset(QS, 0, sizeof(double)*2*NLM);
	SHqst_to_lat(shtns, Sca[ir], QS, QS, cost, s, vt, vp, np, LMAX, MMAX);

	VFREE(QS);
}

/// render a meridional slice, at longitude phi, with a low memory requirement.
void ScalarSH::to_merid(double phi, double *s, int irs, int ire) const
{
	int ir;
	cplx *Q;
	double *Vrr;

	Q = (cplx*) VMALLOC(NLM*sizeof(cplx));
	Vrr = (double*) VMALLOC(shtns->nspat * sizeof(double));		// alloc memory for one shell.

	for (ir = irs; ir <= ire; ir++) {
		SH_Zrotate(shtns, Sca[ir], -phi, Q);
		SH_SPAT(Q, Vrr);
		for (int it=0; it<NLAT; it++)		// copy first meridional slice.
			s[it] = Vrr[it];
		s+=NLAT;
	}
	VFREE(Vrr);		VFREE(Q);
}

/// render a spherical surface.
void ScalarSH::to_surf(int ir, double *s) const
{
	if ((ir < irs) || (ir > ire)) {
		memset(s, 0, shtns->nspat * sizeof(double));
		return;
	}
	SH_SPAT(Sca[ir], s);
}

int ScalarSH::to_zdisc(double z0, int nphi, double* s, double*data, int ir0, int ir1) const
{
	double rr, cost, ss;
	double *tp;		// vector
	int ir;

	rr = z0;
	if (z0 < 0.0) rr = -z0;
	ir = r_to_idx(rr);	if (r[ir] <= rr) ir++;	//r[ir] > rr;
	if (ir0<ir) ir0=ir;
	if (ir0<irs) ir0=irs;		// skip undef data.
	if (ire<ir1) ir1 = ire;// skip undef data.

	tp = (double *) VMALLOC( (nphi+2)*sizeof(double) );

	for (int ir = ir0; ir<=ir1; ir++) {
		rr = r[ir];
		ss = sqrt(rr*rr - z0*z0);		// disc radius.
		*s++ = ss;
		cost = z0/rr;
		to_lat(ir, cost, tp, nphi);
		for (int ip=0; ip<nphi; ip++)  data[ip] = tp[ip];
		data += nphi;
	}
	VFREE(tp);
	return(ir1-ir0+1);		// return number of radii rendered.
}


/*
void ScalarSH::to_grad_lat(int ir, double cost,
					 double *vr, double *vt, double *vp, int np) const
{
	cplx Q[NLM] SSE;	// dT/dr
	cplx S[NLM] SSE;	// T/r
	cplx T[NLM] SSE;	// = 0

	memset(T, 0, sizeof(double)*2*NLM);
	Gradr(ir, Q, S);
	SHqst_to_lat(shtns, Q,S,T, cost, vr, vt, vp, np, LMAX, MMAX);
}
*/

/// evaluates spatial field at given point (with r[ir])
void PolTor::to_point(int ir, double cost, double phi, 
					 double *vr, double *vt, double *vp) const
{
	cplx *Q;	// l(l+1) * Pol/r
	cplx *S;	// dP/dr + Pol/r = 1/r.d(rP)/dr = Wr(Pol)

	Q = (cplx*) VMALLOC(sizeof(double)*4*NLM);
	S = Q + NLM;

	if ((ir < irs)||(ir > ire)) {
		*vr = 0.0; *vt = 0.0; *vp = 0.0;	// undefined.
		return;
	}
	RadSph(ir, Q, S);
	SHqst_to_point(shtns, Q,S,Tor[ir], cost, phi, vr, vt, vp);
	
	VFREE(Q);
}

/// evaluates spatial field at given point, with cubic interpolation in radius.
/// Magnetic fields can be evaluated outside definition domain (potential field extrapolation)
void PolTor::to_point_interp(double rr, double cost, double phi, 
					 double *vr, double *vt, double *vp) const
{
	cplx *Q;	// l(l+1) * Pol/r
	cplx *S;	// dP/dr + Pol/r = 1/r.d(rP)/dr = Wr(Pol)
	cplx *T;
	cplx f0,f1, df0, df1, a,b;
	double dr,x,x2,x3,dr_1;
	int ir;

	Q = (cplx*) VMALLOC(sizeof(double)*6*NLM);
	S = Q + NLM;		T = Q + 2*NLM;

	if (rr < 0) {		// use symmetry to go back to rr >= 0 space.
		rr = -rr;	cost = -cost;	phi += M_PI;
	}
	if ( (rr > r[ire])||(rr < r[irs]) ) {		// out of bounds
		if ((rr > r[ire])&&(bco == BC_MAGNETIC)) {		// magnetic field is defined in the insulator
			ir = ire;
			x2 = r_1[ir];		x = rr * x2;
			LM_LOOP(  f0 = Pol[ir][lm] * x2 * pow(x, -2.-el[lm]);		// f0 = Pol/r
				Q[lm] = l2[lm] * f0;
				S[lm] = -el[lm] * f0;   )
			LM_LOOP(  T[lm] = 0.0;  )
		}
		else if ((rr < r[irs])&&(bci == BC_MAGNETIC)) {
			ir = irs;
			x2 = r_1[ir];		x = rr * x2;
			LM_LOOP(  f0 = Pol[ir][lm] * x2 * pow(x, el[lm]-1.);		// f0 = Pol/r
				Q[lm] = l2[lm] * f0;
				S[lm] = (el[lm]+1.) * f0;  )
		} else {
			*vr = 0.0;	*vt = 0.0;	*vp = 0.0;	// undefined => 0
			VFREE(Q);
			return;
		}
	} else {
		ir = ire;
		while ((r[ir] > rr)&&(ir > irs))	ir--;		// irs <= ir <= ire  and  r[ir] <= rr < r[ir+1]
		if (rr == r[ir]) {		// no neeed to intetrpolate :
			to_point(ir, cost, phi, vr, vt, vp);
			VFREE(Q);
			return;
		}
		if (ir+2 > ire) ir = ire-2;		// ensures ir+2 is in bounds.
		if (ir-1 < irs) ir = irs+1;		// and ir-1 also

		dr = r[ir+1] - r[ir];	dr_1 = 1.0/dr;
		x = (rr - r[ir])*dr_1;	x2 = x*x;	x3 = x*x*x;
		double rr_1 = 1.0/rr;
		// Solenoidal deduced from radial derivative of Poloidal
		OpGrad& G0 = GRAD(ir);		OpGrad& G1 = GRAD(ir+1);
		LM_LOOP(  f0 = Pol[ir][lm];		f1 = Pol[ir+1][lm];
			df0 = ( G0.Gl *Pol[ir-1][lm] + G0.Gd *f0 + G0.Gu *f1 )*dr;
			df1 = ( G1.Gl *f0            + G1.Gd *f1 + G1.Gu *Pol[ir+2][lm] )*dr;
			a = df1+df0 -2.*(f1-f0);		b = 3.*(f1-f0) - df1 - 2.*df0;
			f1 = (a*x3 + b*x2 + df0*x + f0)*rr_1;		df1 = (3.*a*x2 + b*x + df0)*dr_1;
			Q[lm] = f1 * l2[lm];		S[lm] = df1 + f1;  )
		LM_LOOP(  f0 = Tor[ir][lm];		f1 = Tor[ir+1][lm];
			df0 = ( G0.Gl *Tor[ir-1][lm] + G0.Gd *f0 + G0.Gu *f1 )*dr;
			df1 = ( G1.Gl *f0            + G1.Gd *f1 + G1.Gu *Tor[ir+2][lm] )*dr;
			a = df1+df0 -2.*(f1-f0);		b = 3.*(f1-f0) - df1 - 2.*df0;
			T[lm] = a*x3 + b*x2 + df0*x + f0;  )
	}
	SHqst_to_point(shtns, Q,S,T, cost, phi, vr, vt, vp);
	VFREE(Q);
}

void PolTor::uncurl_to_point_interp(double rr, double cost, double phi, 
					 double *vr, double *vt, double *vp) const
{
	cplx *Q;	// r.T
	cplx *S;	// zero
	cplx *T;	// P
	cplx f0,f1, df0, df1, a,b;
	double dr,x,x2,x3,dr_1;
	int ir;

	Q = (cplx*) VMALLOC(sizeof(double)*6*NLM);
	S = Q + NLM;		T = Q + 2*NLM;

	if (rr < 0) {		// use symmetry to go back to rr >= 0 space.
		rr = -rr;	cost = -cost;	phi += M_PI;
	}
	if ( (rr > r[ire])||(rr < r[irs]) ) {		// out of bounds
		*vr = 0.0;	*vt = 0.0;	*vp = 0.0;	// undefined => 0
		VFREE(Q);
		return;
	} else {
		ir = ire;
		while ((r[ir] > rr)&&(ir > irs))	ir--;		// irs <= ir <= ire  and  r[ir] <= rr < r[ir+1]
		if (ir+2 > ire) ir = ire-2;		// ensures ir+2 is in bounds.
		if (ir-1 < irs) ir = irs+1;		// and ir-1 also

		dr = r[ir+1] - r[ir];	dr_1 = 1.0/dr;
		x = (rr - r[ir])*dr_1;	x2 = x*x;	x3 = x*x*x;
		// Solenoidal deduced from radial derivative of Poloidal
		OpGrad& G0 = GRAD(ir);		OpGrad& G1 = GRAD(ir+1);
		LM_LOOP(  f0 = Tor[ir][lm];		f1 = Tor[ir+1][lm];
			df0 = ( G0.Gl *Tor[ir-1][lm] + G0.Gd *f0 + G0.Gu *f1 )*dr;
			df1 = ( G1.Gl *f0            + G1.Gd *f1 + G1.Gu *Tor[ir+2][lm] )*dr;
			a = df1+df0 -2.*(f1-f0);		b = 3.*(f1-f0) - df1 - 2.*df0;
			Q[lm] = rr*(a*x3 + b*x2 + df0*x + f0);  )			// Q = r*T
		LM_LOOP(  f0 = Pol[ir][lm];		f1 = Pol[ir+1][lm];
			df0 = ( G0.Gl *Tor[ir-1][lm] + G0.Gd *f0 + G0.Gu *f1 )*dr;
			df1 = ( G1.Gl *f0            + G1.Gl *f1 + G1.Gu *Tor[ir+2][lm] )*dr;
			a = df1+df0 -2.*(f1-f0);		b = 3.*(f1-f0) - df1 - 2.*df0;
			T[lm] = (a*x3 + b*x2 + df0*x + f0);  )				// T = P
		LM_LOOP(  S[lm] = 0.0;  )
	}
	SHqst_to_point(shtns, Q,S,T, cost, phi, vr, vt, vp);
	VFREE(Q);
}

/// evaluates spatial field at given point (with r[ir])
double ScalarSH::to_point(int ir, double cost, double phi) const
{
	return SH_to_point(shtns, Sca[ir], cost, phi);
}

/// evaluates spatial field at given point, with cubic interpolation in radius.
/// Magnetic fields can be evaluated outside definition domain (potential field extrapolation)
double ScalarSH::to_point_interp(double rr, double cost, double phi) const
{
	cplx f0,f1, df0, df1, a,b;
	double dr,x,x2,x3,dr_1;
	int ir;
	cplx *S0, *S1, *S2, *S3;
	cplx *S;

	if (rr < 0) {		// use symmetry to go back to rr >= 0 space.
		rr = -rr;	cost = -cost;	phi += M_PI;
	}
	if ( (rr > r[ire])||(rr < r[irs]) ) {		// out of bounds
		return 0.0;		// undefined => 0
	} else {
		ir = ire;
		while ((r[ir] > rr)&&(ir > irs))	ir--;		// irs <= ir <= ire  and  r[ir] <= rr < r[ir+1]
		if (rr == r[ir]) {		// no neeed to intetrpolate :
			return SH_to_point(shtns, get_data(0,ir), cost, phi);
		}
		if (ir+2 > ire) ir = ire-2;		// ensures ir+2 is in bounds.
		if (ir-1 < irs) ir = irs+1;		// and ir-1 also

		dr = r[ir+1] - r[ir];	dr_1 = 1.0/dr;
		x = (rr - r[ir])*dr_1;	x2 = x*x;	x3 = x*x*x;
		// Solenoidal deduced from radial derivative of Poloidal
		S0 = get_data(0,ir-1);		S1 = get_data(0,ir);
		S2 = get_data(0,ir+2);		S3 = get_data(0,ir+2);

		S = (cplx*) VMALLOC(sizeof(double)*2*NLM);
		OpGrad& G0 = GRAD(ir);		OpGrad& G1 = GRAD(ir+1);
		LM_LOOP(  f0 = S1[lm];		f1 = S2[lm];
			df0 = ( G0.Gl *S0[lm] + G0.Gd *f0 + G0.Gu *f1 )*dr;
			df1 = ( G1.Gl *f0     + G1.Gd *f1 + G1.Gu *S3[lm] )*dr;
			a = df1+df0 -2.*(f1-f0);		b = 3.*(f1-f0) - df1 - 2.*df0;
			S[lm] = a*x3 + b*x2 + df0*x + f0;  )
		x = SH_to_point(shtns, S, cost, phi);
		VFREE(S);
		return x;
	}
}


/**********************
 *** SPATIAL OUTPUT ***
 **********************/

void Spatial::write_shell(const char *fn, int ir) const
{
	FILE *fp;
	int i,j,k;	//phi, theta

	if ((!data) || (get_data(0,ir) == NULL))
		runerr("[write_shell] Shell not allocated.");
	
	fp = fopen(fn,"w");
	if (fp == NULL) runerr("[write_shell] File creation error");
	fprintf(fp,"%%plot_surf.py\n%% [XSHELLS] Surface data (sphere) shell #%d (r=%.4f). first line is (theta 0 0), first row is phi, then for each point, (r,theta,phi) components are stored together.\n0 ",ir,r[ir]);
		for(j=0;j<NLAT;j++) {
			fprintf(fp,"%.6g 0 0 ",acos(ct[j]));	// first line = theta (radians)
		}
	for (i=0; i<NPHI; i++) {
		fprintf(fp,"\n%.6g ",phi_rad(i));		// first row = phi (radians)
		for(j=0; j<NLAT; j++) {
			k = i*NLAT+j;
			if (ncomp==3) {
				fprintf(fp,"%.6g %.6g %.6g  ",get_data(0,ir)[k], get_data(1,ir)[k], get_data(2,ir)[k]);		// data (r,t,phi)
			} else if (ncomp==1) {
				fprintf(fp,"%.6g 0 0  ",get_data(0,ir)[k]);		// data (scalar)
			}
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}

#ifdef XS_HDF5
/* write spatial data in HDF5 format */
#include <hdf5.h>
#include <hdf5_hl.h>

void write_hdf5_grid_xdmf(const char *fn, const int ncomp, const char** name_arr, int istart, int iend, int irstep=1)
{
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = (NPHI==1) ? 2 : 3;
	char fn_grid[64], fn_xml[256];
	const int nr = (iend-istart+irstep)/irstep;

	// generate grid
	sprintf(fn_grid,"grid_%dx%dx%d.h5",nr,NLAT,NPHI);
	FILE* f = fopen(fn_grid, "r");
	if (f==NULL) {		// generate grid if it does not exist yet.
		float* XYZ = (float*) malloc( sizeof(float) * nr*(NPHI+1)*NLAT);
		double* sp = (double*) malloc( sizeof(double) * 2*(NPHI+1));
		double* cp = sp + NPHI+1;
		file = H5Fcreate(fn_grid, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		dims[0] = nr;	dims[1] = NLAT;		dims[2] = NPHI+1;	// 0 vary slowest, 2 fastest.

		for (int i=0; i<=NPHI; i++) {		// generate cos(phi) and sin(phi) arrays.
			double phi = phi_rad(i);
			cp[i] = cos(phi);
			sp[i] = sin(phi);
		}

		for (int i=0; i<nr; i++)
			for (int it=0; it<NLAT; it++) {
				double s = r[i*irstep+istart]*st[it];
				for (int ip=0; ip<=NPHI; ip++)
					XYZ[i*(NPHI+1)*NLAT + it*(NPHI+1) + ip] = s * cp[ip];		// x = r.cos(phi).sin(theta)
			}
		H5LTmake_dataset_float(file, "x", rank, dims, XYZ);

		for (int i=0; i<nr; i++)
			for (int it=0; it<NLAT; it++) {
				double s = r[i*irstep+istart]*st[it];
				for (int ip=0; ip<=NPHI; ip++)
					XYZ[i*(NPHI+1)*NLAT + it*(NPHI+1) + ip] = s * sp[ip];		// y = r.sin(phi).sin(theta)
			}
		H5LTmake_dataset_float(file, "y", rank, dims, XYZ);

		for (int i=0; i<nr; i++)
			for (int it=0; it<NLAT; it++) {
				double z = r[i*irstep+istart]*ct[NLAT-1-it];		// reverse order to have increasing z
				for (int ip=0; ip<=NPHI; ip++)
					XYZ[i*(NPHI+1)*NLAT + it*(NPHI+1) + ip] = z;		// z = r.cos(theta)
			}
		H5LTmake_dataset_float(file, "z", rank, dims, XYZ);

		printf("done\n");
		H5Fclose(file);
		free(sp);
		free(XYZ);
	} else fclose(f);

	// generate xdmf file
	sprintf(fn_xml, "%s.xdmf",fn);
	f = fopen(fn_xml, "w");
	fprintf(f, "<?xml version=\"1.0\" ?>\n<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
	fprintf(f,"  <Domain>\n    <Grid GridType=\"Uniform\">\n");
	fprintf(f,"      <Topology TopologyType=\"3DSMesh\" Dimensions=\"%d %d %d\"/>\n", nr, NLAT, NPHI+1);
	fprintf(f,"      <Geometry GeometryType=\"X_Y_Z\">\n");
	fprintf(f,"        <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/x</DataItem>\n", nr, NLAT, NPHI+1, fn_grid);
	fprintf(f,"        <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/y</DataItem>\n", nr, NLAT, NPHI+1, fn_grid);
	fprintf(f,"        <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/z</DataItem>\n", nr, NLAT, NPHI+1, fn_grid);
	fprintf(f,"      </Geometry>\n");
	if (ncomp==0) {		// 3 cartesian components
		fprintf(f, "    <Attribute Name=\"V\" AttributeType=\"Vector\" Center=\"Node\">\n");
		fprintf(f, "		<DataItem ItemType=\"Function\" Dimensions=\"%d %d %d 3\" Function=\"JOIN($0 , $1 , $2)\">\n",nr, NLAT, NPHI+1);
		fprintf(f, "		 <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/%s</DataItem>\n", nr, NLAT, NPHI+1, fn, name_arr[0]);
		fprintf(f, "		 <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/%s</DataItem>\n", nr, NLAT, NPHI+1, fn, name_arr[1]);
		fprintf(f, "		 <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/%s</DataItem>\n", nr, NLAT, NPHI+1, fn, name_arr[2]);
		fprintf(f, "		</DataItem>\n    </Attribute>\n");
	} else {
		for (int k=0; k<ncomp; k++) {
			fprintf(f,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Node\">\n",name_arr[k]);
			fprintf(f,"		<DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">%s:/%s</DataItem>\n", nr, NLAT, NPHI+1, fn, name_arr[k]);
			fprintf(f,"    </Attribute>\n");
		}
	}
	fprintf(f,"    </Grid>\n  </Domain>\n</Xdmf>\n");
	fclose(f);
}

void Spatial::write_hdf5(const char *fn, int istart, int iend, int cartesian, int irstep) const
{
	float *dataf;	// write buffer
	double *dbl;
	long int i, ip, it;
	size_t size;
	int k;
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = (NPHI==1) ? 2 : 3;
	const int nr = (iend-istart+irstep)/irstep;
	static const char* name_arr[] = {"Vr", "Vtheta", "Vphi", "Vx", "Vy", "Vz"};

	if ((ncomp > 3)||(ncomp <= 0)) runerr("[write_hdf5] supports only field with 1,2 or 3 components");
	if (istart < irs) istart = irs;
	if (iend > ire) iend = ire;
    file = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	dims[0] = nr;	dims[1] = NLAT;		dims[2] = NPHI+1;	// 0 vary slowest, 2 fastest.

	/* coordinates */
	size = (NPHI >= NLAT) ? NPHI+1 : NLAT;		// max(NPHI+1,NLAT)
	size = (nr > size) ? nr : size;				// max(nr,size)
	dbl = (double *) malloc(sizeof(double) * size);
	for (i=0; i<nr; i++) dbl[i] = r[i*irstep +istart];
	H5LTmake_dataset_double(file, "r", 1, &dims[0], dbl);		// r
	for (i=0; i<NLAT; i++)		dbl[i] = acos(ct[NLAT-1-i]);		// reverse theta order to have increasing z.
	H5LTmake_dataset_double(file, "theta", 1, &dims[1], dbl);		// theta
	for (i=0; i<NPHI; i++)		dbl[i] = phi_rad(i);
	dbl[NPHI] = dbl[0];
	H5LTmake_dataset_double(file, "phi", 1, &dims[2], dbl);			// phi
	free(dbl);

    size = NLAT*(NPHI+1)*nr;
	if ((ncomp==3)&&(cartesian)) {	// cartesian coordinates:
		dataf = (float*) malloc(3*size * sizeof(float));
		if (dataf==0) runerr("[write_hdf5] not enough memory");
		for (i=0; i<nr; i++) {	// transpose and convert to float.
			float* vx = dataf + i*(NPHI+1)*NLAT;
			float* vy = vx+size;		float* vz = vx+2*size;
			for (ip=0; ip<NPHI; ip++)
				for (it=0; it<NLAT; it++) {
					int itr = NLAT-1-it;		// reverse theta order to have increasing z (needed for correct normals in paraview).
					int ir0 = i*irstep + istart;
					double vr = get_data(0,ir0)[ip*NLAT +itr];
					double vt = get_data(1,ir0)[ip*NLAT +itr];
					double vp = get_data(2,ir0)[ip*NLAT +itr];
					spher_to_cart(ct[itr], phi_rad(ip), &vr, &vt, &vp);		// convert to cartesian coordinates
					vx[it*(NPHI+1) + ip] = vr;
					vy[it*(NPHI+1) + ip] = vt;
					vz[it*(NPHI+1) + ip] = vp;
				}
			ip=NPHI;
				for (it=0; it<NLAT; it++) {		// copy for periodicity
					vx[it*(NPHI+1) + ip] = vx[it*(NPHI+1)];
					vy[it*(NPHI+1) + ip] = vy[it*(NPHI+1)];
					vz[it*(NPHI+1) + ip] = vz[it*(NPHI+1)];
				}
		}
		H5LTmake_dataset_float(file, name_arr[3], rank, dims, dataf);
		H5LTmake_dataset_float(file, name_arr[4], rank, dims, dataf+size);
		H5LTmake_dataset_float(file, name_arr[5], rank, dims, dataf+2*size);
	} else {
		dataf = (float*) malloc(size * sizeof(float));
		if (dataf==0) runerr("[write_hdf5] not enough memory");
		for (k=0; k<ncomp; k++) {
			for (i=0; i<nr; i++) {	// transpose and convert to float.
				int ir0 = i*irstep + istart;
				for (ip=0; ip<NPHI; ip++)
					for (it=0; it<NLAT; it++)
						dataf[i*(NPHI+1)*NLAT + it*(NPHI+1) + ip] = get_data(k,ir0)[ip*NLAT +(NLAT-1-it)];
				ip=NPHI;
					for (it=0; it<NLAT; it++)
						dataf[i*(NPHI+1)*NLAT + it*(NPHI+1) + ip] = get_data(k,ir0)[NLAT-1-it];
			}
			H5LTmake_dataset_float(file, name_arr[k], rank, dims, dataf);
		}
	}
	free(dataf);
	H5Fclose(file);

	// write grid and xdmf file:
	write_hdf5_grid_xdmf(fn, cartesian ? 0 : ncomp, name_arr + cartesian*3, istart, iend, irstep);
}

/// create hdf5 file with spherical coordinates
hid_t create_hdf5_spherical_coord(const char* fn, hsize_t* dims, int nr, int istart, int irstep)
{
	hid_t file = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	dims[0] = nr;	dims[1] = NLAT;		dims[2] = NPHI+1;	// 0 vary slowest, 2 fastest.
	size_t size = (NLAT > NPHI) ? NLAT : NPHI+1;		// max(nlat, nphi+1)
	size = (nr > size) ? nr : size;				// max(nr,size)
	double* dbl = (double *) malloc(sizeof(double) * size);
	for (int i=0; i<nr; i++) dbl[i] = r[i*irstep +istart];
	H5LTmake_dataset_double(file, "r", 1, &dims[0], dbl);		// r
	for (int i=0; i<NLAT; i++)		dbl[i] = acos(ct[NLAT-1-i]);		// reverse theta order to have increasing z.
	H5LTmake_dataset_double(file, "theta", 1, &dims[1], dbl);		// theta
	for (int i=0; i<NPHI; i++)		dbl[i] = phi_rad(i);
	dbl[NPHI] = dbl[0];
	H5LTmake_dataset_double(file, "phi", 1, &dims[2], dbl);			// phi
	free(dbl);
	return file;
}

void PolTor::write_hdf5(const char *fn, int istart, int iend, int cartesian, int irstep) const
{
	float *dataf;	// write buffer
	long int i, ip, it;
	size_t size;
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = (NPHI==1) ? 2 : 3;
	const int nr = (iend-istart+irstep)/irstep;
	static const char* name_arr[] = {"Vr", "Vtheta", "Vphi", "Vx", "Vy", "Vz"};

	if ((ncomp > 3)||(ncomp <= 0)) runerr("[write_hdf5] supports only field with 1,2 or 3 components");
	if (istart < irs) istart = irs;
	if (iend > ire) iend = ire;
	file = create_hdf5_spherical_coord(fn, dims, nr, istart, irstep);

	Shell2d vr(shtns), vt(shtns), vp(shtns);
    size = NLAT*(NPHI+3)*nr;
	dataf = (float*) malloc(3*size * sizeof(float));
	if (dataf==0) runerr("[write_hdf5] not enough memory");
	for (i=0; i<nr; i++) {	// transpose and convert to float.
		int ir0 = i*irstep + istart;
		to_surf(ir0, vr,vt,vp);
		float* vx = dataf + i*(NPHI+1)*NLAT;
		float* vy = vx+size;		float* vz = vx+2*size;
		for (ip=0; ip<NPHI; ip++)
			for (it=0; it<NLAT; it++) {
				int itr = NLAT-1-it;		// reverse theta order to have increasing z (needed for correct normals in paraview).
				double vrr = vr(itr,ip);
				double vtt = vt(itr,ip);
				double vpp = vp(itr,ip);
				if (cartesian) {
					spher_to_cart(ct[itr], phi_rad(ip), &vrr, &vtt, &vpp);		// convert to cartesian coordinates
				}
				vx[it*(NPHI+1) + ip] = vrr;
				vy[it*(NPHI+1) + ip] = vtt;
				vz[it*(NPHI+1) + ip] = vpp;
			}
		ip=NPHI;
			for (it=0; it<NLAT; it++) {		// copy for periodicity
				vx[it*(NPHI+1) + ip] = vx[it*(NPHI+1)];
				vy[it*(NPHI+1) + ip] = vy[it*(NPHI+1)];
				vz[it*(NPHI+1) + ip] = vz[it*(NPHI+1)];
			}
	}
	H5LTmake_dataset_float(file, name_arr[0+cartesian*3], rank, dims, dataf);
	H5LTmake_dataset_float(file, name_arr[1+cartesian*3], rank, dims, dataf+size);
	H5LTmake_dataset_float(file, name_arr[2+cartesian*3], rank, dims, dataf+2*size);
	free(dataf);
	H5Fclose(file);

	// write grid and xdmf file:
	write_hdf5_grid_xdmf(fn, cartesian ? 0 : 3, name_arr + cartesian*3, istart, iend, irstep);
}


void ScalarSH::write_hdf5(const char *fn, int istart, int iend, int irstep) const
{
	float *dataf;	// write buffer
	long int i, ip, it;
	size_t size;
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = (NPHI==1) ? 2 : 3;
	const int nr = (iend-istart+irstep)/irstep;
	static const char* name = "Vr";

	if ((ncomp > 3)||(ncomp <= 0)) runerr("[write_hdf5] supports only field with 1,2 or 3 components");
	if (istart < irs) istart = irs;
	if (iend > ire) iend = ire;
	file = create_hdf5_spherical_coord(fn, dims, nr, istart, irstep);

	Shell2d shell(shtns);
    size = NLAT*(NPHI+1)*nr;
	dataf = (float*) malloc(size * sizeof(float));
	if (dataf==0) runerr("[write_hdf5] not enough memory");
	for (i=0; i<nr; i++) {	// transpose and convert to float.
		int ir0 = i*irstep + istart;
		to_surf(ir0, shell);
		float* s = dataf + i*(NPHI+1)*NLAT;
		for (ip=0; ip<NPHI; ip++)
			for (it=0; it<NLAT; it++) {
				int itr = NLAT-1-it;		// reverse theta order to have increasing z (needed for correct normals in paraview).
				s[it*(NPHI+1) + ip] = shell(itr, ip);
			}
		ip=NPHI;
			for (it=0; it<NLAT; it++) {		// copy for periodicity
				s[it*(NPHI+1) + ip] = s[it*(NPHI+1)];
			}
	}
	H5LTmake_dataset_float(file, name, rank, dims, dataf);
	free(dataf);
	H5Fclose(file);

	// write grid and xdmf file:
	write_hdf5_grid_xdmf(fn, 1, &name, istart, iend, irstep);
}


void ScalarSH::write_box_hdf5(const char *fn, double x0, double y0, double z0, double lx, double ly, double lz, int nx, int ny, int nz)
{
	double *s;
	int i,j,k;
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = 3;

	x0 -= lx/2;		y0 -= ly/2;		z0 -= lz/2;
	lx /= (nx-1);		ly /= (ny-1);		lz /= (nz-1);

	s = (double*) malloc(nx*ny*nz * sizeof(double));
	if (s==NULL) runerr("[write_box_hdf5] allocation error");

    file = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    dims[0] = nx;		dims[1] = ny;		dims[2] = nz;	// 0 vary slowest, 2 fastest.

	// coordinates
	for (i=0; i<nx; i++) s[i] = x0 + i*lx;
	H5LTmake_dataset_double(file, "x", 1, &dims[0], s);		// x
	for (i=0; i<ny; i++) s[i] = y0 + i*ly;
	H5LTmake_dataset_double(file, "y", 1, &dims[1], s);		// y
	for (i=0; i<nz; i++) s[i] = z0 + i*lz;
	H5LTmake_dataset_double(file, "z", 1, &dims[2], s);		// z

	// data
	#pragma omp parallel for schedule(dynamic) private(i,j,k)
	for (k=0; k<nz; k++) for (j=0; j<ny; j++) for (i=0; i<nx; i++) {
		double x = x0 + i*lx;
		double y = y0 + j*ly;
		double z = z0 + k*lz;
		double rr = sqrt(x*x + y*y + z*z);
		double phi = atan(y/x);	if (x < 0.) phi += M_PI;
		double ct = z/rr;
		s[k*nx*ny + j*nx + i] = to_point_interp(rr, ct, phi);
	}

	H5LTmake_dataset_double(file, "s", rank, dims, s);
    H5Fclose(file);
	free(s);
}

void PolTor::write_box_hdf5(const char *fn, double x0, double y0, double z0, double lx, double ly, double lz, int nx, int ny, int nz, int uncurl)
{
	double *vx, *vy, *vz;
	int i,j,k;
	hid_t file;
	hsize_t dims[3];           // dataset dimensions
	const int rank = 3;

	x0 -= lx/2;		y0 -= ly/2;		z0 -= lz/2;
	lx /= (nx-1);		ly /= (ny-1);		lz /= (nz-1);

	vx = (double*) malloc(nx*ny*nz * sizeof(double));
	vy = (double*) malloc(nx*ny*nz * sizeof(double));
	vz = (double*) malloc(nx*ny*nz * sizeof(double));

	if ((vx==NULL)||(vy==NULL)||(vz==NULL)) runerr("[write_box_hdf5] allocation error");

    file = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    dims[0] = nx;		dims[1] = ny;		dims[2] = nz;	// 0 vary slowest, 2 fastest.

	// coordinates
	for (i=0; i<nx; i++) vx[i] = x0 + i*lx;
	for (i=0; i<ny; i++) vy[i] = y0 + i*ly;
	for (i=0; i<nz; i++) vz[i] = z0 + i*lz;
	H5LTmake_dataset_double(file, "x", 1, &dims[0], vx);		// x
	H5LTmake_dataset_double(file, "y", 1, &dims[1], vy);		// y
	H5LTmake_dataset_double(file, "z", 1, &dims[2], vz);		// z

	// data
	#pragma omp parallel for schedule(dynamic) private(i,j,k)
	for (k=0; k<nz; k++) for (j=0; j<ny; j++) for (i=0; i<nx; i++) {
		double vr, vt, vp;
		double x = x0 + i*lx;
		double y = y0 + j*ly;
		double z = z0 + k*lz;
		double rr = sqrt(x*x + y*y + z*z);
		double phi = atan(y/x);	if (x < 0.) phi += M_PI;
		double ct = z/rr;
		if (uncurl) {
			uncurl_to_point_interp(rr, ct, phi, &vr, &vt, &vp);
		} else	to_point_interp(rr, ct, phi, &vr, &vt, &vp);
		spher_to_cart(ct, phi, &vr, &vt, &vp);
		vx[k*nx*ny + j*nx + i] = vr;
		vy[k*nx*ny + j*nx + i] = vt;
		vz[k*nx*ny + j*nx + i] = vp;
	}

	H5LTmake_dataset_double(file, "vx", rank, dims, vx);
	H5LTmake_dataset_double(file, "vy", rank, dims, vy);
	H5LTmake_dataset_double(file, "vz", rank, dims, vz);
    H5Fclose(file);
	free(vz);	free(vy);	free(vx);
}
#endif


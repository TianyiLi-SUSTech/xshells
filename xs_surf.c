/* handle surface maps for QG core flow simulation */


struct PolTor_surf {
	cplx *__restrict Pol;
	cplx *__restrict Tor;
	double phase0, freq0;
	double *freq;
	int lmax;
	void* next;				// allows to build chained lists.
};

struct PolTor_surf* pt_surf = NULL;
int surf_drift = 0;		// no drift by zero
int surf_stream = 0;	// not a stream function.
int surf_incomp = 0;	// do not correct for incompressibility.

/// write 2D surface vector (on a spherical shell)
void write_shell2(const char *fn, double *vt, double *vp)
{
	FILE *fp;
	long int i,j,k;	//phi, theta

	fp = fopen(fn,"w");
	fprintf(fp,"%% [SURF2VOL] Surface data (sphere, r=1). first line is (theta 0 0), first row is phi, then for each point, (r,theta,phi) components are stored together.\n0 ");
		for(j=0;j<NLAT;j++) {
			fprintf(fp,"%.6g 0 0 ",acos(ct[j]));	// first line = theta (radians)
		}
	for (i=0; i<NPHI; i++) {
		fprintf(fp,"\n%.6g ",(2*i*M_PI)/(NPHI*MRES));		// first row = phi (radians)
		for(j=0; j<NLAT; j++) {
			k = i*NLAT+j;
			fprintf(fp,"0 %.6g %.6g  ",vt[k],vp[k]);		// data
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}

/// 2D velocity field on the surface extrapolated to 3D field (spatial).
void vect_surf_to_3D(double *vt, double *vp, struct VectField *V, double ric)
{
	double rr, sr, zr, ztop, alp0, alp1;
	double vsr, vzr, vpr, vtt, vtb, vpt, vpb;
	int ir,it,ip, it0,nlat_2;
	int irs = V->irs;
	int ire = V->ire;

	nlat_2 = (NLAT+1)/2;
	#pragma omp for schedule(static) private(ir,it,it0,ip, rr,sr,zr,ztop,alp0,alp1, vsr,vzr,vpr,vtt,vtb,vpt,vpb)
	for (ir=irs; ir<=ire; ir++) {
		rr = r[ir]/r[ire];		// non-dimensional radius.
		for (it=0, it0=0; it<nlat_2; it++) {
			sr = rr*st[it];
			zr = rr*ct[it];
			ztop = sqrt(1.0 - sr*sr);
			while ((it0+2 < nlat_2)&&(st[it0+1] < sr)) it0++;
			alp1 = (sr - st[it0])/(st[it0+1]-st[it0]);		// interp lin /s.
			alp0 = 1. - alp1;
			for (ip=0; ip<NPHI; ip++) {
				vtt = alp0*vt[ip*NLAT + it0] + alp1*vt[ip*NLAT + it0+1];
				vtb = alp0*vt[ip*NLAT + NLAT-it0-1] + alp1*vt[ip*NLAT + NLAT-it0-2];
				vpt = alp0*vp[ip*NLAT + it0] + alp1*vp[ip*NLAT + it0+1];
				vpb = alp0*vp[ip*NLAT + NLAT-it0-1] + alp1*vp[ip*NLAT + NLAT-it0-2];
			  if (st[it0+1] >= ric) {
//				vpr = vpt;
//				vsr = ztop*vtt;
//				vzr = -sr*vtt*zr/ztop;
				vpr = 0.5*((vpt+vpb) + zr/ztop*(vpt-vpb));
				vsr = 0.5*(ztop*(vtt-vtb) + zr*(vtt+vtb));
				vzr = -0.5*sr*((vtt+vtb) + zr/ztop*(vtt-vtb));
				V->vr[ir][ip*NLAT +it] = vzr*ct[it] + vsr*st[it];
				V->vt[ir][ip*NLAT +it] = vsr*ct[it] - vzr*st[it];
				V->vp[ir][ip*NLAT +it] = vpr;

//				vzr = -vzr;
				vpr = 0.5*((vpt+vpb) - zr/ztop*(vpt-vpb));
				vsr = 0.5*(ztop*(vtt-vtb) - zr*(vtt+vtb));
				vzr = -0.5*sr*((vtt+vtb) - zr/ztop*(vtt-vtb));
				V->vr[ir][ip*NLAT +NLAT-it-1] = -vzr*ct[it] + vsr*st[it];
				V->vt[ir][ip*NLAT +NLAT-it-1] = -vsr*ct[it] - vzr*st[it];
				V->vp[ir][ip*NLAT +NLAT-it-1] = vpr;
			  } else {		// inner core handling
				double sic = sr/ric;
				double zic = sqrt(1.-sic*sic);
				V->vp[ir][ip*NLAT +it] = vpt;
				V->vp[ir][ip*NLAT +NLAT-it-1] = vpb;

				vsr = ztop*vtt;
				vpt = -sr*vtt;		// vz_top
				vpb = -vsr*sic/zic; 	// vz_btm
				vzr = vpb + (zr-zic*ric)/(ztop-zic*ric)*(vpt-vpb);
				V->vr[ir][ip*NLAT +it] = vzr*ct[it] + vsr*st[it];
				V->vt[ir][ip*NLAT +it] = vsr*ct[it] - vzr*st[it];

				vsr = -ztop*vtb;
				vpb = -sr*vtb;		// vz_btm
				vpt = vsr*sic/zic; 	// vz_top
				vzr = vpt + (zr-zic*ric)/(ztop-zic*ric)*(vpb-vpt);
				V->vr[ir][ip*NLAT +NLAT-it-1] = -vzr*ct[it] + vsr*st[it];
				V->vt[ir][ip*NLAT +NLAT-it-1] = -vsr*ct[it] - vzr*st[it];
			  }
			}
		}
	}
}

/// extends the surface data into volume.
void calc_Usurf_to_vol(struct VectField *V, double time)
{
	double *vt, *vp;
	struct PolTor_surf *pts = pt_surf;

	if (pts == NULL) {
		#pragma omp master
		{	V->inz0 = V->ire;		V->inz1 = V->ire-1;	}		// everything marked as zero.
		return;
	}
	#pragma omp single copyprivate(vt,vp)
	{
		int lm;
		int lmax = pts->lmax;
		vt = (double*) fftw_malloc(sizeof(double) * 2*shtns->nspat);
		vp = vt + shtns->nspat;
		cplx *p = (cplx*) fftw_malloc(sizeof(cplx) * 2*NLM);
		cplx *t = p + NLM;
		for (lm = 0; lm < NLM; lm++) {
			p[lm] = 0.0;	t[lm] = 0.0;
		}
		do {
			double f = cos(pts->freq0 * time + pts->phase0);
			if (pts->freq == NULL) {
				for (lm=0; lm < NLM; lm++) {		// variation style Composantes Principales.
					p[lm] += pts->Pol[lm] * f;	t[lm] += pts->Tor[lm] * f;
				}
			} else {
				for (lm = 0; lm <= LMAX; lm++) {
					p[lm] += pts->Pol[lm] * f;	t[lm] += pts->Tor[lm] * f;
				}
				for (lm = LMAX+1; lm < NLM; lm++) {
					double phase = pts->freq[lm] * time;
					cplx eip = cplx(f*cos(phase) , f*sin(phase));
					p[lm] += pts->Pol[lm] * eip;	t[lm] += pts->Tor[lm] * eip;
				}
			}
			pts = (PolTor_surf*) pts->next;
		} while(pts != NULL);
		if (surf_stream) {
			SHtor_to_spat_l(shtns, t, vt,vp, lmax);		// transform to spatial coordinates.
			double* vp1 = (double *) fftw_malloc(NSPAT_ALLOC(shtns) * sizeof(double));
			if (surf_incomp) {
				SH_to_spat_l(shtns, t, vp1, lmax);
				write_shell2("o_shell.t",vp1,vp1);
			}
			for (int j=0; j<NLAT; j++) {
				double ct_1 = 1./(ct[j]*r[V->ire]);
				double s = r[V->ire]*st[j];
				for (int i=0; i<NPHI; i++) {
					if (surf_incomp)		// correction for divergence-free flow.
						vp[i*NLAT+j] += s*ct_1 * vp1[i*NLAT+j];
					// stream-fucntion must be divided by cos(theta)
					vt[i*NLAT+j] *= ct_1;		vp[i*NLAT+j] *= ct_1;
				}
			}
			free(vp1);
		} else {
			SHsphtor_to_spat_l(shtns, p,t, vt,vp, lmax);		// transform to spatial coordinates.
		}
		write_shell2("o_shell.surf",vt,vp);
		fftw_free(p);
	}	// copyprivate copies vt and vp to all threads.
	vect_surf_to_3D(vt, vp, V, r[V->irs]);
	#pragma omp barrier
	#pragma omp master
	{
		fftw_free(vt);
	//	write_shell("o_shell.drift", V, NM);
	//	write_merid("o_Vp.drift", V->vp, 0, NG, NM);
	}
}

/// Allocate memory for a Poloidal/Toroidal representation of a field.
/// This will reset the Boundary Conditions to NONE.
struct PolTor_surf* alloc_PolTor_surf(int lmax, double phase, double freq)
{
	struct PolTor_surf *pts;
	int lm;

	pts = new PolTor_surf;
	pts->freq0 = freq;		pts->phase0 = phase;
	pts->lmax = lmax;		// mark all as zero.
	pts->freq = NULL;		pts->next = NULL;

	pts->Pol = (cplx*) fftw_malloc(2*sizeof(cplx)*NLM);
	pts->Tor = pts->Pol + NLM;

	for (lm=0; lm<NLM; lm++) {		// zero out
		pts->Pol[lm] = 0.0;	pts->Tor[lm] = 0.0;
	}
	return(pts);
}

/// free memory allocated by alloc_PolTorField
void free_PolTor_surf(struct PolTor_surf *pts)
{
	if (pts != NULL) {
		if (pts->freq != NULL) free(pts->freq);		// free freq memory
		fftw_free(pts->Pol);		// free field memory
		free(pts);				// free structure memory
	}
}


/* read surface data from A. Pais. */

/// Given a file of spherical harmonic coefficients from A. Pais,
/// return a guess of spherical harmonic degree truncation
void pais_get_lmax(char *fname, int *lmax, int *nset)
{
	double f;
	long int i,lines;
	int match, n, lmax1, nset1, n0, n1;
	FILE *fp;
	char buf[256];

	printf("[pais_get_lmax] File '%s' ",fname);
	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[pais_get_lmax] file not found !");

// count lines :
	i = 0;		lines = 0;
	while (1) {
		fgets(buf, 254, fp);		// line by line
		if (feof(fp)) break;
		i += sscanf(buf, "%lf %lf", &f, &f);		// line count as 2 if real and imag stored together.
		lines++;
	}
	fclose(fp);

	printf("has %ld lines (%ld values) ",lines,i);
// find truncation and number of sets.
	match=0;
	lmax1 = *lmax;		// by default, try to match the specified lmax.
	n0 = *nset;		n1 = *nset;		// by default, try to match specified nset.
	if (*nset <= 0) {
		n0=1;	n1=2;		// if nset not specified, try 1 or 2.
	}
	for (n=n0; n<=n1; n++) {
		f = sqrt(2.25 + ((double) i)/n) - 1.5;
		if (*lmax <= 0)  lmax1 = f;			// guess lmax if not specified (*lmax <= 0).
		#ifdef XS_DEBUG
			printf("[nset=%d, lmax=%f] ",n,f);
		#endif
		if (fabs(f-lmax1) < 1.e-10) {
			match++;
			nset1 = n;
		}
	}
	if (match == 1) {
		*nset = nset1;
		*lmax = lmax1;
		printf("=> matching (lmax=%d, nset=%d).\n",lmax1, nset1);
	} else {
		runerr("[Pais] no unique match !");			// failed to match (lmax,nset).
	}
	return;
}

/// load spherical harmonic data written by Alexandra Pais.
/// 1 column ascii with sh data.
/// Memory must be allocated before, returns number of real values read.
int pais_load_sh_text(char *fname, int lmax, int iset, double scale, cplx *sh, double *freq)
{
	double c, s, f;
	long int l, m, j, col;
	FILE *fp;
	char buf[256];

	for (l=0; l<NLM; l++) sh[l] = 0.0;		// zero out field.
	if (freq != NULL) for (l=0; l<NLM; l++) freq[l] = 0.0;		// zero out frequency.

	printf("[Pais] Reading SH surface data from text file '%s' (lmax=%d)...\n",fname,lmax);
	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[pais_load_sh_text] file not found !");

	l=0;	j=0;
	while( l<2*iset*(nlm_calc(lmax,lmax,1)-1) ) {
		fgets(buf, 254, fp);		// line by line
		if (feof(fp)) break;
		col = sscanf(buf, "%lf %lf", &f, &f);		// line count as 2 if real and imag stored together.
		j++;	l += col;
	}
	#ifdef XS_DEBUG
		if (j != 0) printf("       => skipped %d lines (%d complex values)\n",j, l/2);
	#endif

// load data : import from 'real' SH, Schmitt semi-normalized + Condon-Shortley phase [A. Pais]
	j=0;	s=0.0;	f=0.0;
	for (l=1; l<=lmax; l++) {			// cos data (+ optional sin and freq)
		for (m=0; m<=l; m++, j++) {
			fgets(buf, 254, fp);		// line by line
			col = sscanf(buf, "%lf %lf %lf", &c, &s, &f);
			if ((m<=MMAX)&&(l<=LMAX)) {
				if (m>0) {	c /= sqrt(2.);	s /= sqrt(2.);	}
				set_Ylm(sh, l, m, cplx(c , -s) * (scale * (1-2*(m&1)) / sqrt(2*l +1)));	// *sqrt(8*pi) to match Alexandra's data
				if (freq != NULL) freq[LM(l,m)] = f;
			}
		}
	}
	if (col == 1) {		// there was no imaginary part, read it now.
		for (l=1; l<=lmax; l++) {			// sin data.
			for (m=0; m<=l; m++, j++) {
				fscanf(fp, "%lf", &s);
				if ((m<=MMAX)&&(l<=LMAX)) {
					if (m>0) f /= sqrt(2.);
					((double*) sh)[LM(l,m)*2+1] = -s*scale * (1-2*(m&1)) / sqrt(2*l +1);
				}
			}
		}
	}
	fclose(fp);
	#ifdef XS_DEBUG
		printf("       => read %d lines (%d real values)\n",j, j*col);
	#endif
	return(j*col);		// return number of real values read.
}

/// allocate memory and load pol/tor data wiht optional drift frequency (returns NULL otherwise)
struct PolTor_surf* load_poltor_surf_txt(char* fname, double scale)
{
	struct PolTor_surf *pts;
	double *pfreq, *tfreq;
	int lm, nset, lmax, lmax_in, k;

	lmax_in = 0;	nset = 0;
	pais_get_lmax(fname, &lmax_in, &nset);
	lmax = (lmax_in < LMAX) ? lmax_in : LMAX;
	pts = alloc_PolTor_surf(lmax, 0.0, 0.0);
	pfreq = (double*) malloc(sizeof(double) * NLM*2);
	tfreq = pfreq + NLM;

	k=0;
	if (nset==2) pais_load_sh_text(fname, lmax_in, k++, scale, pts->Pol, pfreq);	// load poloidal from file.
	pais_load_sh_text(fname, lmax_in, k, scale, pts->Tor, tfreq);	// load toroidal.
	if (nset==1) {		// perform some computations on streamfunction
	}

	for (lm=0; lm<2*NLM; lm++) {
		if (pfreq[lm] != 0.0) surf_drift=1;	// drift required.
	}
	if (surf_drift == 0) {		// no drift
		printf("  static velocity field.\n");
		free(pfreq);		pfreq = NULL;
	} else
		printf("  drifting velocity field.\n");
	pts->freq = pfreq;

	return pts;
}

int load_PC_surf(const char* fname, double ampl0)
{
	int npc, i, lmax, nset, lmax1, nset1;
	double ampl, phase, freq, t0;
	struct PolTor_surf *pts;
	struct PolTor_surf **next = &pt_surf;
	FILE *fp;
	char str[240];
	char fn2[120];

	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[load_PC_surf] file not found !");

	lmax=0;		t0=0;		nset = 0;		npc = 0;
	while (fgets(str, 239, fp)) {
		i = sscanf(str,"%s %lf %lf %lf", fn2, &ampl, &phase, &freq);
		if ((i==4)&&(fn2[0] != '#')) {		// valid mode definition.
			lmax1 = lmax;	nset1 = nset;
			pais_get_lmax(fn2, &lmax1, &nset1);		// check for matching lmax and nset.
			ampl *= ampl0;		// rescale everything with ampl0.
			phase *= M_PI/180.;	// convert to radians.
			pts = alloc_PolTor_surf(lmax, phase, freq);
			if (freq != 0.0) surf_drift = 1;		// enable drift
			int k = 0;
			if (nset == 2) pais_load_sh_text(fn2, lmax, k++, ampl, pts->Pol, NULL);		// poloidal
			pais_load_sh_text(fn2, lmax, k, ampl, pts->Tor, NULL);	// toroidal
			if (nset == 1) {
				surf_stream = 1;		// indicate a stream-function (stored in toroidal).
				shtns_cfg sh0 = shtns_create(lmax, 0, 1, sht_norm);		// remove average value at theta=pi/2
				double x = SH_to_point(sh0, pts->Tor, 0, 0);
				//printf("average value at equator before correction = %g\n",x);
				pts->Tor[0] -= x*sh00_1(shtns);
				//x = SH_to_point(sh0, pts->Tor, 0, 0);
				//printf("average value at equator after correction = %g\n",x);
				shtns_destroy(sh0);
			}
			*next = pts;
			next = (PolTor_surf**) &(pts->next);
			npc ++;		// count number of components.
		} else {
			sscanf(str,"lmax = %d",&lmax);
			sscanf(str,"nset = %d",&nset);
			sscanf(str,"t0 = %lf",&t0);
			sscanf(str,"incomp = %d",&surf_incomp);
		}
	}
	fclose(fp);
	if (lmax==0) runerr("[load_PC_surf] lmax not specified !");
	printf("[load_PC_surf] %d components loaded (lmax=%d, nset=%d, incomp=%d).\n", npc, lmax, nset, surf_incomp);
	return npc;
}

#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

define([AC_CACHE_LOAD], )dnl
define([AC_CACHE_SAVE], )dnl
AC_PREREQ([2.62])
AC_INIT([xshells],[2.5.2],[],[xshells],[https://bitbucket.org/nschaeff/xshells])
configure_flags="$@"
AC_LANG([C++])
AC_CONFIG_SRCDIR([xshells_big.cpp])

# optional variables :
AC_ARG_VAR(PYTHON, [the python interpreter (defaults to 'python')])

# optional features with --enable-XXX
AC_ARG_ENABLE([debug],
	AS_HELP_STRING([--enable-debug], [Add some runtime checks and compilation warnings]))
AC_ARG_ENABLE([simd],
	AS_HELP_STRING([--disable-simd], [Do not use vector extensions (SSE2, AVX, ...)]), [], [enable_simd=default])
AC_ARG_ENABLE([mpi],
	AS_HELP_STRING([--disable-mpi], [Do not use the MPI enabled compiler]))
AC_ARG_ENABLE([mkl],
	AS_HELP_STRING([--enable-mkl], [Try to link with intel MKL instead of FFTW (slightly faster)]), [], [enable_mkl=default])
AC_ARG_ENABLE([knl],
	AS_HELP_STRING([--enable-knl], [Enable compilation for Xeon Phi (KNL)]), [], [enable_knl=no])
AC_ARG_ENABLE([gnuplot],
	AS_HELP_STRING([--enable-gnuplot], [Display real time energy plots uing Gnuplot]), [], [enable_gnuplot=default])
AC_ARG_WITH([shtns],
	AS_HELP_STRING([--with-shtns], [Indicate where to find the shtns package]), [], [with_shtns=shtns])

dnl Sanitize $prefix. Autoconf does this by itself, but so late in the
dnl generated configure script that the expansion does not occur until
dnl after our eval magic below.
AS_IF([test "$prefix" = "NONE"],[prefix=$ac_default_prefix])


### DEAL WITH SHTNS ###
LIB_SHTNS=shtns
LIB_SHTNS_OMP=shtns_omp
AS_IF([test "x$enable_knl" == "xyes"], [
    LIB_SHTNS=shtns_mic
    LIB_SHTNS_OMP=shtns_mic_omp
])

## configure and build SHTns for xshells
shtns_dir=$with_shtns
AS_IF([test -d $shtns_dir], [
    pushd $shtns_dir
    AC_CHECK_FILE(lib$LIB_SHTNS.a, [], [
	AC_MSG_NOTICE([***** CONFIGURE SHTNS *****])
	echo $configure_flags
	./configure --enable-ishioka $configure_flags --disable-openmp
	AS_IF([ test $? != 0 ], [AC_MSG_ERROR([single-thread shtns configuration failed])])
	AC_MSG_NOTICE([***** COMPILING SHTNS *****])
	make -j4
	AS_IF([ test $? != 0 ], [AC_MSG_ERROR([single-thread shtns compilation failed])])
    ])

    AS_IF([test "x$enable_openmp" != "xno"], [
	AC_CHECK_FILE(lib$LIB_SHTNS_OMP.a, [], [
	    AC_MSG_NOTICE([***** CONFIGURE SHTNS (multi-thread) *****])
	    ./configure --enable-ishioka $configure_flags --enable-openmp
	    AS_IF([ test $? != 0 ], [AC_MSG_ERROR([multi-thread shtns configuration failed. You may try disabling openmp with --disable-openmp])])
	    AC_MSG_NOTICE([***** COMPILING SHTNS (multi-thread) *****])
	    make -j4
	    AS_IF([ test $? != 0 ], [AC_MSG_ERROR([multi-thread shtns compilation failed. You may try disabling openmp with --disable-openmp])])
	])
    ])
    popd
    CXXFLAGS="$CXXFLAGS -I$shtns_dir -L$shtns_dir"
])

AC_MSG_NOTICE([***** CONFIGURING XSHELLS *****])

# Enable DEBUG ?
AS_IF([test "x$enable_debug" == "xyes"], [
	CXXFLAGS="$CXXFLAGS -O2 -g -DXS_DEBUG=1"
], [
	CXXFLAGS="$CXXFLAGS -O3 -g"
])

CXXFLAGS="$CXXFLAGS -I$prefix/include -L$prefix/lib"

AC_PROG_SED
if test "$SED" = :; then
	AC_MSG_ERROR([sed program required.])
fi


##########################################
### define macro AX_CHECK_COMPILE_FLAG ###
##########################################
AC_DEFUN([AX_CHECK_COMPILE_FLAG],
[AS_VAR_PUSHDEF([CACHEVAR],[ax_cv_check_[]_AC_LANG_ABBREV[]flags_$4_$1])dnl
AC_CACHE_CHECK([whether _AC_LANG compiler accepts $1], CACHEVAR, [
  ax_check_save_flags=$[]_AC_LANG_PREFIX[]FLAGS
  _AC_LANG_PREFIX[]FLAGS="$[]_AC_LANG_PREFIX[]FLAGS $4 $1"
  AC_COMPILE_IFELSE([AC_LANG_PROGRAM()],
    [AS_VAR_SET(CACHEVAR,[yes])],
    [AS_VAR_SET(CACHEVAR,[no])])
  _AC_LANG_PREFIX[]FLAGS=$ax_check_save_flags])
AS_IF([test x"AS_VAR_GET(CACHEVAR)" = xyes],
  [m4_default([$2], [CXXFLAGS="$CXXFLAGS $1"])],
  [m4_default([$3], :)])
AS_VAR_POPDEF([CACHEVAR])dnl
])dnl AX_CHECK_COMPILE_FLAG
## end macro AX_CHECK_COMPILE_FLAG

####################################
### define macro AX_PROG_CXX_MPI ###
####################################
AC_DEFUN([AX_PROG_CXX_MPI], [
# Check for compiler
# Needs to be split off into an extra macro to ensure right expansion
# order.
AC_REQUIRE([_AX_PROG_CXX_MPI],[_AX_PROG_CXX_MPI([$1])])

AS_IF([test x"$_ax_prog_cxx_mpi_mpi_wanted" = xno],
  [ _ax_prog_cxx_mpi_mpi_found=no ],
  [
    AC_LANG_PUSH([C++])

    # test whether MPI_Init() is available
    # We do not use AC_SEARCH_LIBS here, as it caches its outcome and
    # thus disallows corresponding calls in the other AX_PROG_*_MPI
    # macros.
    for lib in NONE mpi mpich; do
      save_LIBS=$LIBS
      if test x"$lib" = xNONE; then
        AC_MSG_CHECKING([for function MPI_Init])
      else
        AC_MSG_CHECKING([for function MPI_Init in -l$lib])
        LIBS="-l$lib $LIBS"
      fi
      AC_LINK_IFELSE([
        AC_LANG_PROGRAM([
extern "C" { void MPI_Init(); }
],[MPI_Init();])],
        [ _ax_prog_cxx_mpi_mpi_found=yes ],
        [ _ax_prog_cxx_mpi_mpi_found=no ])
      AC_MSG_RESULT($_ax_prog_cxx_mpi_mpi_found)
      if test "x$_ax_prog_cxx_mpi_mpi_found" = "xyes"; then
        break;
      fi
      LIBS=$save_LIBS
    done

    # Check for header
    AS_IF([test x"$_ax_prog_cxx_mpi_mpi_found" = xyes], [
      AC_MSG_CHECKING([for mpi.h])
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([#include <mpi.h>])],
        [ AC_MSG_RESULT(yes)],
        [ AC_MSG_RESULT(no)
         _ax_prog_cxx_mpi_mpi_found=no
      ])
    ])
    AC_LANG_POP([C++])
])

# Finally, execute ACTION-IF-FOUND/ACTION-IF-NOT-FOUND:
AS_IF([test x"$_ax_prog_cxx_mpi_mpi_found" = xyes], [
        ifelse([$2],,[AC_DEFINE(HAVE_MPI,1,[Define if you have the MPI library.])],[$2])
        :
],[
        $3
        :
])

])dnl AX_PROG_CXX_MPI

AC_DEFUN([_AX_PROG_CXX_MPI], [
  AC_ARG_VAR(MPICXX,[MPI C++ compiler command])
  ifelse([$1],,[_ax_prog_cxx_mpi_mpi_wanted=yes],[
    AC_MSG_CHECKING([whether to compile using MPI])
    if $1; then
      _ax_prog_cxx_mpi_mpi_wanted=yes
    else
      _ax_prog_cxx_mpi_mpi_wanted=no
    fi
    AC_MSG_RESULT($_ax_prog_cxx_mpi_mpi_wanted)
  ])
  AS_IF([test x"$_ax_prog_cxx_mpi_mpi_wanted" = xyes], [
    AC_CHECK_TOOLS([MPICXX], [mpic++ mpicxx mpiicpc mpiCC sxmpic++ hcp mpxlC_r mpxlC mpixlcxx_r mpixlcxx mpg++ mpc++ mpCC cmpic++ mpiFCC CCicpc pgCC pathCC sxc++ xlC_r xlC bgxlC_r bgxlC openCC sunCC crayCC g++ c++ gpp aCC CC cxx cc++ cl.exe FCC KCC RCC], [$CXX])
    CXX="$MPICXX"      # overwrite CXX with the mpi compiler.
  ])
  AC_PROG_CXX
])dnl _AX_PROG_CXX_MPI
## end macro AX_PROG_CXX_MPI


# try icc first if available
dnl AC_PROG_CXX([icc g++ gcc cl KCC CC cxx cc++ xlC aCC c++])

# find mpi compiler
AX_PROG_CXX_MPI([test x"$enable_mpi" != xno],[use_mpi=yes],[
	use_mpi=no
	if test x"$enable_mpi" = xyes; then
	   AC_MSG_FAILURE([MPI compiler not found.])
	else
	  AC_MSG_WARN([MPI compiler not found, disabling MPI.])
	fi
])

# C++11 support is required.
AC_MSG_CHECKING([whether $CXX compiler supports c++11 standard])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
    #if __cplusplus < 201103L
	not c++ 11
    #endif
]])], [
    AC_MSG_RESULT(yes)
],[
    AC_MSG_RESULT(no)
    # try to add -std=c++11 option
    AX_CHECK_COMPILE_FLAG([-std=c++11],[CXX="$CXX -std=c++11"],[AC_MSG_ERROR(["compiler $CXX does not seem to support C++11 standard"])])
])

# if debug disabled, do not warn for unused variables
AS_IF([test "x$enable_debug" == "xyes"], [
	AX_CHECK_COMPILE_FLAG([-Wunused])
	AX_CHECK_COMPILE_FLAG([-Wuninitialized])
	AX_CHECK_COMPILE_FLAG([-Wmaybe-uninitialized])
])
AX_CHECK_COMPILE_FLAG([-Wtype-limits])
AX_CHECK_COMPILE_FLAG([-Wformat])

AS_IF([test "x$enable_knl" != "xyes"], [
	# add gcc compile options if supported.
	AX_CHECK_COMPILE_FLAG([-march=native],[CXXFLAGS="$CXXFLAGS -march=native"], [
		AX_CHECK_COMPILE_FLAG([-mtune=native])
	])
	# these are icc compile option, try if it is supported:
	AX_CHECK_COMPILE_FLAG([-xHost])
  ],[
	# KNL native cross-compiling with ICC
	AX_CHECK_COMPILE_FLAG([-xMIC-AVX512],[CXXFLAGS="$CXXFLAGS -xMIC-AVX512"], [AC_MSG_ERROR(["Xeon Phi (KNL) not supported by compiler. Did you use the Intel Compiler?"])])
	# With KNL, enable mkl by default.
	AS_IF([test "x$enable_mkl" = "xdefault"], [enable_mkl=yes])
])

# add g++ compile options if supported. Do NOT set -ffast-math !!
# these may be acceptable: -fno-math-errno -freciprocal-math -fno-trapping-math -ffinite-math-only -fcx-limited-range -fno-signed-zeros
AX_CHECK_COMPILE_FLAG([-fno-math-errno])
AX_CHECK_COMPILE_FLAG([-fno-trapping-math])
AX_CHECK_COMPILE_FLAG([-freciprocal-math])
AX_CHECK_COMPILE_FLAG([-ffinite-math-only])
AX_CHECK_COMPILE_FLAG([-fcx-limited-range])
AX_CHECK_COMPILE_FLAG([-fno-exceptions])
AX_CHECK_COMPILE_FLAG([-fno-rtti])
AX_CHECK_COMPILE_FLAG([-mno-vzeroupper])

# add icc compile options if supported.
AX_CHECK_COMPILE_FLAG([-fp-model=precise])
AX_CHECK_COMPILE_FLAG([-complex-limited-range])
AX_CHECK_COMPILE_FLAG([-ipo])
AX_CHECK_COMPILE_FLAG([-prec-div -prec-sqrt])


# Checks for header files.
AC_CHECK_HEADERS([stdlib.h stdio.h string.h math.h])
AC_CHECK_HEADER([shtns.h],[],AC_MSG_ERROR([shtns.h not found. Install the SHTns library from https://bitbucket.org/nschaeff/shtns or indicate location of shtns.h by adding CXXFLAGS="-I/path/to/shtns/include" to the ./configure command line.]),[AC_INCLUDES_DEFAULT])


#### Checks for libraries ####
AC_CHECK_LIB([m],[cos],,AC_MSG_ERROR([math library not found.]))
# HDF5 library
AC_CHECK_LIB([hdf5],[H5open],[H5OPTS="-lhdf5"])
AC_CHECK_LIB([hdf5_hl],[H5Dclose],[H5OPTS="-DXS_HDF5 $H5OPTS -lhdf5_hl"],,[$H5OPTS])

# use the same libraries for the in-shell openmp version:
LIBSOMP="$LIBS"

# Check for sequential FFTW (FFTW preferred, MKL optional)
mkl_seq_found="no"
AS_IF([test "x$enable_mkl" = "xyes"], [
  # optionally use MKL instead of fftw3
  CXXFLAGS="$CXXFLAGS -DXS_MKL -I$MKLROOT/include -I$MKLROOT/include/fftw"
  AX_CHECK_COMPILE_FLAG([-mkl=sequential], [mkl_seq_found="-mkl=sequential"],[
    AX_CHECK_COMPILE_FLAG([-mkl], [mkl_seq_found="-mkl"])
  ])
  AS_IF([test "$mkl_seq_found" != "no"], [
  	LIBS="$mkl_seq_found $LIBS"	# easy way !
	AC_CHECK_FUNC(fftw_plan_many_dft, [AC_MSG_NOTICE(["sequential FFTW interface found in MKL, link with $mkl_seq_found"])],
		[AC_MSG_ERROR(["FFTW interface not found in MKL libraries ($mkl_seq_found)."])])
  ],[
	# assume 64 bit mode, add default directory to find the MKL
	LDFLAGS="$LDFLAGS -L$MKLROOT/lib/intel64"

	# sequential MKL libs
	mkl_libs="-Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group"
	AC_CHECK_LIB(mkl_sequential, fftw_plan_many_dft, [LIBS="$mkl_libs $LIBS"],
		AC_MSG_ERROR(["FFTW interface not found in sequential MKL libraries ($mkl_libs)."]),
	["$mkl_libs"])

	AC_MSG_NOTICE(["sequential FFTW interface found in MKL, link with $mkl_libs"])
  ])
  # fftw3_mkl.h header is needed.
  AC_CHECK_HEADERS([fftw3.h fftw3_mkl.h],[],[AC_MSG_ERROR([fftw3_mkl.h not found. Please add CXXFLAGS="-I/path/to/mkl/include/fftw" to the ./configure command line.])],[AC_INCLUDES_DEFAULT])
],[
    # fftw3
    AC_CHECK_HEADER([fftw3.h],[],[AC_MSG_ERROR([fftw3.h not found. Please add CXXFLAGS="-I/path/to/fftw/include" to the ./configure command line.])],[AC_INCLUDES_DEFAULT])
    AC_CHECK_LIB([fftw3],[fftw_plan_many_dft],,AC_MSG_ERROR([FFTW3 library required.]))
])

# OpenMP test and multi-threaded FFTW
AS_IF([test "x$enable_openmp" != "xno"], [
    shtns_found=yes
    AC_OPENMP
    AX_CHECK_COMPILE_FLAG([-Wunknown-pragmas],[OPENMP_CXXFLAGS="$OPENMP_CXXFLAGS -Wunknown-pragmas"])
    dnl AC_CHECK_HEADERS([omp.h])
    LIBS_BACKUP=$LIBS
    LIBS=$LIBSOMP

    # Check for threaded FFTW (FFTW preferred, MKL optional)
    mkl_par_found="no"
    AS_IF([test "x$enable_mkl" = "xyes"], [
      # optionally use MKL instead of fftw3
      AX_CHECK_COMPILE_FLAG([-mkl=parallel], [mkl_par_found="-mkl=parallel"],[
	AX_CHECK_COMPILE_FLAG([-mkl], [mkl_par_found="-mkl"])
      ])
      AS_IF([test "$mkl_par_found" != "no"], [
	    LIBS="$mkl_par_found $LIBS"	# easy way !
	    AC_CHECK_FUNC(fftw_init_threads, [AC_MSG_NOTICE(["parallel FFTW interface found in MKL, link with $mkl_par_found"])],
		    [AC_MSG_ERROR(["parallel FFTW interface not found in MKL libraries ($mkl_par_found)."])])
      ],[
	    # assume 64 bit mode, add default directory to find the MKL
	    LDFLAGS="$LDFLAGS -L$MKLROOT/lib/intel64"

	    # threaded MKL libs for in-shell OpenMP
	    mkl_libs="-Wl,--start-group -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -Wl,--end-group"
	    AC_CHECK_LIB(mkl_gnu_thread, fftw_init_threads, [LIBS="$mkl_libs $LIBS"],
		    AC_MSG_WARN(["FFTW interface not found in threaded MKL libraries ($mkl_libs)."]),
		    [$OPENMP_CXXFLAGS $mkl_libs])

	    AC_MSG_NOTICE(["parallel FFTW interface found in MKL, link with $mkl_libs"])
      ])
    ],[
	AC_CHECK_LIB([fftw3_omp], [fftw_init_threads], [LIBS="-lfftw3_omp -lfftw3 $LIBS"], AC_MSG_WARN([multithreaded FFTW3 not found]),[$OPENMP_CXXFLAGS -lfftw3])
    ])

    AC_CHECK_LIB([$LIB_SHTNS_OMP], [shtns_use_threads],, [
	AC_MSG_WARN([multi-threaded SHTns not found])
	shtns_found=no
    ], [$OPENMP_CXXFLAGS])
    LIBSOMP=$LIBS
    LIBS=$LIBS_BACKUP
    AC_CHECK_LIB([$LIB_SHTNS],[shtns_create],, [
	AC_MSG_WARN([single-threaded SHTns not found])
	AS_IF([test "x$shtns_found" == "xno"], [
		AC_MSG_ERROR([SHTns library not found. Indicate location of $LIB_SHTNS and/or $LIB_SHTNS_OMP with CXXFLAGS="-L/path/to/shtns/lib"])
	])
	shtns_found=no
    ])

    AS_IF([test "x$shtns_found" == "xno"], [
	AC_MSG_WARN([some executables may not compile without the appropriate SHTns library])
    ])
], [
    # Try to use SIMD from OpenMP, without the multi-threading.
    AX_CHECK_COMPILE_FLAG([-fopenmp-simd])
    # Check for SHTns
    AC_CHECK_LIB([$LIB_SHTNS],[shtns_create],,AC_MSG_ERROR([SHTns library not found. Indicate location of $LIB_SHTNS with CXXFLAGS="-L/path/to/shtns/lib"]))
])

# with intel compiler, we should not use -DXS_VEC
AS_IF([test "x$enable_simd" == "xdefault"], [
	AC_EGREP_CPP(icc,
	  [#ifdef __INTEL_COMPILER
	   icc
	   #endif
	  ], [enable_simd=no])
])

# Disable SIMD ?
AS_IF([test "x$enable_simd" != "xno"], [
	CXXFLAGS="$CXXFLAGS -DXS_VEC=1"
])

# Enable Gnuplot ?
AS_IF([test "x$enable_gnuplot" == "xno"], [
	CXXFLAGS="$CXXFLAGS -DXS_NOPLOT"
],[
	AC_CHECK_HEADERS([unistd.h signal.h],[],[CXXFLAGS="$CXXFLAGS -DXS_NOPLOT"])
])

# Checks related to Python and NumPy paths:
AS_IF([test "x$PYTHON" = "x"], [
	AC_MSG_CHECKING(for python with numpy package)
	py_test="python python2 python3"
],[
	AC_MSG_CHECKING(if $PYTHON has numpy package)
	py_test="$PYTHON"
])
for py in $py_test
do :
	numpy_inc=`$py -c "from numpy import get_include; print(get_include())" 2>/dev/null`
	AS_IF([test "x$numpy_inc" != "x"],[break])
done
AS_IF([test "x$numpy_inc" = "x"], [
	AC_MSG_RESULT(no)
	AC_MSG_WARN([NumPy package is required for the python interface.])
],[
	AC_MSG_RESULT($py)
	PYTHON=$py
])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T

echo "prefix=$prefix"
AC_SUBST([H5OPTS])
AC_SUBST([LIBSOMP])
AC_CONFIG_FILES([Makefile])
AC_OUTPUT

echo "****** CONFIGURE DONE. ******"
echo "**  Now, choose an example problem to get started, for instance:"
echo "**      cp problems/geodynamo/xshells.{par,hpp} ."
echo "**  Compile an xshells executable:"
echo "**      make xsbig"
echo "**  Edit the 'xshells.par' file to adjust run-time parameters & run!"
echo "**  See documentation for more details: https://nschaeff.bitbucket.io/xshells/manual.html"
echo "*********"

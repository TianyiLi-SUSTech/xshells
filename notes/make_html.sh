#!/usr/bin/bash

### using latexml ###
latexml --dest=manual.xml manual --inputencoding=utf8
#latexmlpost -dest=manual.html manual.xml --css=LaTeXML-navbar-left.css --navigationtoc=context --splitat=chapter --javascript=LaTeXML-maybeMathjax.js
latexmlpost -dest=manual.html manual.xml --css=LaTeXML-navbar-left.css --navigationtoc=context --splitat=chapter --javascript=https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_HTMLorMML

# delete line with titlepic
sed '/titlepic/d' manual.html > manual_edit.html
mv manual_edit.html manual.html

### with 1 page per chapter.
#htlatex manual.tex "ht5mjlatex.cfg, 2, charset=utf-8" " -cunihtf -utf8"
### with frames
# htlatex manual.tex "ht5mjlatex.cfg, frames, charset=utf-8" " -cunihtf -utf8"
#htlatex manual.tex "ht5mjlatex_new.cfg, frames, charset=utf-8" " -cunihtf -utf8"
#htlatex manual.tex "ht5mjlatex_nomathml, frames, charset=utf-8" " -cunihtf -utf8"

## open external links on top of frames.
#for html in manual*html; do
#  sed -e "s/\(href=\"http[s]*:\/\/[^\"]*\"\)/\1 target=\"_top\"/g" $html > parsed_$html
#  mv parsed_$html $html
#done



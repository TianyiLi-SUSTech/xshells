#!/usr/bin/python
#plot a slice
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

parser = argparse.ArgumentParser(
    description='plots a field at a given surface  (uses output of xspp surf)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('fname', type=str,
                    help='file to read, must be output by xspp surf')
parser.add_argument('components', type=int, nargs='*', default=[0,1,2],
                    help='vector components (0=r, 1=theta, 2=phi) to plot')
parser.add_argument('-z', '--zoom', type=str, default="1",
                    help='zoom level for color bar, can also be a tuple giving min and max values')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display of the figure (forces a save)')

clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import sys              # acces a la ligne de commande.
import xsplot
import re

fname=clin.fname
czoom=eval(clin.zoom)
rc("font", family="serif")
rc("font", size=10)

if fname.endswith(".npy"):
    a = load(fname)    # load numpy data
    for idx in clin.components:
        if idx<a.shape[0] and amax(abs(a[idx,1:,1:])) > 0:
            figure()
            xsplot.plot_slice(a, idx, czoom=czoom)
            if nodisplay:
                savefig(fname.replace('.npy', '-%d.png' % idx))
else:
    t,p,ur,ut,up = xsplot.load_surf(fname)

    label = array([r'$r$',r'$\theta$',r'$\phi$'])

    for i in clin.components:
        if (i == 0) :
            figure()
            xsplot.plot_surf(t,p, ur, title=label[i], czoom=czoom)
        if (i == 1) :
            figure()
            xsplot.plot_surf(t,p, ut, title=label[i], czoom=czoom)
        if (i == 2) :
            figure()
            xsplot.plot_surf(t,p, up, title=label[i], czoom=czoom)
        if (i == 3) :   # both ut and up on the same plot
            figure()
            utp = ut[:,:]
            utp[:,len(p)/2:] = up[:,len(p)/2:]
            xsplot.plot_surf(t,p, utp, czoom=czoom)
            text(-2.8, 1.2, label[1], fontsize=16)
            text( 2.6, 1.2, label[2], fontsize=16)

        if nodisplay:
            savefig(fname + '-%d.png' % i)

if not nodisplay:
    show()

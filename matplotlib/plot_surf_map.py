#!/usr/bin/python
import matplotlib
import os

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False
if nodisplay:
    matplotlib.use("Agg")

from pylab import *     # matplotlib
from mpl_toolkits.basemap import *  # maps and coastlines
import sys              # acces a la ligne de commande.
import xsplot
import re

#import numpy as np
#import matplotlib.pyplot as plt

argn = len(sys.argv)
if argn < 2:
    print("usage: " + sys.argv[0] + " <xspp_surf_file> [0|1|2] [1|2|0] ...")
    print("  optional parameters 0,1 or 2 chooses component (x,y or z)")
    exit()

# a mollweide projection for r_sphere = 100.0
#m = Basemap(projection='moll',lon_0=180,rsphere=100.0,area_thresh=1000000)
# a hammer projection for r_sphere = 100.0
m = Basemap(projection='hammer',lon_0=180,rsphere=100.0,area_thresh=100000)

fname = sys.argv[1]
if fname.endswith('.npy'):
    a = load(fname)
    t = a[0,0,1:]*180./pi
    p0 = a[0,1:,0]*180./pi
    comp = list(range(0,a.shape[0]))
    data, label, cmaps = [],[],[]
    for i in comp:
        d,p = addcyclic(a[i,1:,1:].T, p0)
        data.append(d)
        name = xsplot.get_slice_name(a[i,0,0])
        label.append('$'+name+'$')
        cmaps.append( xsplot.get_cmap(name) )
else:
    t,p0,ur0,ut0,up0 = xsplot.load_surf(fname)
    ur,p = addcyclic(ur0,p0)
    ut,p = addcyclic(ut0,p0)
    up,p = addcyclic(up0,p0)
    data = [ur,ut,up]
    comp = list(range(0,3))
    label = array([r'$v_r$',r'$v_\theta$',r'$v_\phi$'])
    cmaps = [cm.PuOr, cm.PuOr, cm.PuOr]

if argn > 2:
    for i in range(2,argn):
        comp[i-2] = int(sys.argv[i])
    comp = comp[0:argn-2]

# transform to projected coordinates
x, y = m(*meshgrid(p,90-t))

rc("font", family="sans")
rc("font", size=14)


for i in comp:
    figure(figsize=(6.6, 4))
#   subplots_adjust(left=0.01, bottom=0.01, right=0.9, top=0.99, wspace=0.2, hspace=0.2)
    subplots_adjust(left=0.01, bottom=0.1, right=0.99, top=0.99, wspace=0.2, hspace=0.2)

    m.contourf(x,y, data[i], 20, cmap=cmaps[i])
    mm = amax(abs(data[i]))

    m.drawcoastlines(linewidth=0.5, color='grey')    # draw discrete coastlines
    m.drawmapboundary()     # draw a line around the map region
#   m.drawparallels(np.arange(-90.,120.,30.)) # draw parallels
    m.drawparallels(np.arange(-70.,71.,70.),linewidth=1.0,dashes=[5,5],color='black') # draw tangent cylinder
    m.drawmeridians(np.arange(0.,370.,60.),linewidth=0.5,dashes=[5,5],color='grey') # draw meridians
    cb=colorbar(orientation='horizontal',fraction=0.045,pad=0.03,aspect=35)
#   cb=colorbar(fraction=0.02,pad=0.02)
    clim(-mm,mm)
    if (i < 3) :
        text(515, 250, label[i], fontsize=28 )

    if nodisplay:
        savefig( "plot_surf_map_%d.png" % i )

show()

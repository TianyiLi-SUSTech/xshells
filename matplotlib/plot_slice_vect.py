#!/usr/bin/python
#plot a slice
from pylab import *     # matplotlib
import sys              # acces a la ligne de commande.
import xsplot

ir0=1

r,ct,ur = xsplot.load_merid(sys.argv[1])    #Ur
r,ct,ut = xsplot.load_merid(sys.argv[2])    #Ut
r,ct,up = xsplot.load_merid(sys.argv[3])    #Up
#r,ct,up = xsplot.load_merid(sys.argv[3], ang=1)    # Up to angular velocity

st = sqrt(1-ct*ct)

x = r*matrix(st)
y = r*matrix(ct)

#colormap for phi component
#pcolor(array(x),array(y),up,shading='interp')
contourf(array(x),array(y),up,15,cmap=cm.Spectral)
colorbar()

#quiver for r,theta
s = ur.shape
l = s[0]
m = s[1]
s = l//11
t = (m+15)//16
print(s,t)

#cartesian coordinates
ux = ur*st + ut*ct
uy = ur*ct - ut*st

quiver(array(x[s:l:s,0:m:t]),array(y[s:l:s,0:m:t]),ux[s:l:s,0:m:t],uy[s:l:s,0:m:t])
#quiver(array(x[l/3:l:s,:]),array(y[l/3:l:s,:]),ux[l/3:l:s,:],uy[l/3:l:s,:])

axis('equal')
axis('off')
savefig('slice.png')
#savefig('slice.pdf')
show()

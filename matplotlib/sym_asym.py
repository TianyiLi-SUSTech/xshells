#!/usr/bin/python
#plot a slice
from pylab import *     # matplotlib
import xsplot
import re

# standalone execution of statements.
if __name__ == "__main__":
    import sys      # command line access
    if len(sys.argv) < 2 :
        print("usage: " + sys.argv[0] + " <xspp_surf_file>")
        exit()
    else:
        filename = sys.argv[1]

# load data
th,phi,vr,vt,vp = xsplot.load_surf(filename)

nth = len(th)
nphi = len(phi)

vts = copy(vt)
vta = copy(vt)
vps = copy(vp)
vpa = copy(vp)

for i in range(len(th)):
    vts[i,:] = vt[i,:] + vt[nth-1-i,:]
    vta[i,:] = vt[i,:] - vt[nth-1-i,:]
    vps[i,:] = vp[i,:] + vp[nth-1-i,:]
    vpa[i,:] = vp[i,:] - vp[nth-1-i,:]

sym = sum(vps*vps,1) + sum(vta*vta,1)
asym = sum(vpa*vpa,1) + sum(vts*vts,1)

plot(th,sym, th,asym)
grid()
show()

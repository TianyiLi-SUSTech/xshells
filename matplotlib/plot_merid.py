#!/usr/bin/python
#plot a slice
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

parser = argparse.ArgumentParser(
    description='plots a meridional slice of a field in a disc (uses output of xspp merid)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('fnames', type=str, nargs='*',
                    help='files to read, must be output by xspp merid')
parser.add_argument('-z', '--zoom', type=str, default="1",
                    help='zoom level for color bar, can also be a tuple giving min and max values')
parser.add_argument('--angp', action='store_true',
                    help='set for data divided by s (ie: azimuthal velocity to angular velocity)')
parser.add_argument('--angt', action='store_true',
                    help='set for data divided by r (ie: theta velocity to angular velocity)')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display of the figure (forces a save)')
        
clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import sys              # acces a la ligne de commande.
import xsplot

#rcParams['axes.formatter.limits'] = (-3,3)

import matplotlib.ticker as ticker
# a cool formatter.
fmt=ticker.ScalarFormatter(useMathText=True)
fmt.set_powerlimits((-3,3))

czoom = eval(clin.zoom)

if clin.angp:
    ang = 1
elif clin.angt:
    ang = 2
else:
    ang = 0
rg=0 #rg=0.35

rc("font", family="sans")
rc("font", size=14)

n = len(clin.fnames)
    
if clin.fnames[0].endswith('.npy'):
    fname = clin.fnames[0]
    a = load(fname) # load data
    n -= 1
    if n == 0:
        n = a.shape[0]
        idxs = range(n)
    else:
        idxs = clin.fnames[1:]
        n = len(idxs)
    figure(figsize=(4.8*n-0.2, 6))
    subplots_adjust(left=0.01, bottom=0.01, right=1-0.25/n, top=0.95, wspace=0.5, hspace=0.2)
    ii = 1
    for idx in idxs:
        idx = int(idx)
        ax = subplot(1,n,ii)
        if idx<a.shape[0] and amax(abs(a[idx,1:,1:])) > 0:
            rmax = amax(a[idx,1:,0])
            label = xsplot.plot_slice(a, idx, czoom=czoom)
            #print(label)
            #ax.text(0.75*rmax,0.8*rmax,'$'+label+'$', fontsize=28)
        ii = ii + 1
    if nodisplay:
        savefig(fname.replace('.npy', '.png'))
else:
    n = len(clin.fnames)
    figure(figsize=(4.8*n-0.2, 6))
    subplots_adjust(left=0.01, bottom=0.01, right=1-0.25/n, top=0.99, wspace=0.5, hspace=0.2)
    ii = 1
    for f in clin.fnames:
        label = f
        field_name = f[2]
        cmap = cm.RdBu_r
        if (field_name == 'T'):
            label=r"$T$"
            cmap = cm.RdYlBu_r
        if (field_name == 'B'):
            cmap = cm.PuOr
        if (f[3] == 's'):
            label=r"$%c_s$" % field_name
        if (f[3] == 'p'):
            label=r"$%c_\phi$" % field_name
        if (f[3] == 'z'):
            label=r"$%c_z$" % field_name
        if (f[3] == 't'):
            label=r"$%c_\theta$" % field_name
        if (f[3] == 'r'):
            label=r"$%c_r$" % field_name

        ax = subplot(1,n,ii)
        r,ct,V = xsplot.load_merid(f, ang)
        rmax = amax(r)
        m = xsplot.plot_merid(r, ct, V, czoom=czoom, rg=rg, cmap=cmap)
        ax.text(0.75*rmax,0.8*rmax,label, fontsize=28)
        ii = ii + 1

    #xlim(0,shift*1.1 -0.1)
    #clim(-mall/czoom,mall/czoom)
    #subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95, wspace=0.0, hspace=0.0)
    #colorbar(shrink=0.9)

    if nodisplay:
        savefig(clin.output)

if not nodisplay:
    show()

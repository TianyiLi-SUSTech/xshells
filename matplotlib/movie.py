#!/usr/bin/python
#plot a slice
from matplotlib.pyplot import figure, savefig, close
from subprocess import *
import xsplot
import sys              # acces a la ligne de commande.
import glob
import re
import argparse

parser = argparse.ArgumentParser(
    description='''Makes movies of Vs and Vz (cylindrical vector components) in a
    specified meridional crossection for a given run of xshells.

    ''',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('base', type=str,
                    help='file base to read')
parser.add_argument('job', type=str,
                    help='name of the xshells job')
parser.add_argument('-i', '--index', type=int, default=1,
                    help='first file to place in movie')
parser.add_argument('-a', '--angle', type=str, default="45",
                    help='phi angle of slice')

clin = parser.parse_args()

ir0=1
rg=0.1

i=clin.index;   # snapshot numbering

base=clin.base
job=clin.job
files=sorted(glob.glob( base + '_*.' + job))
#files=sorted(glob.glob( 'field*' ))

# get the names of the files to load by searching the output string of
# xspp
vsreg = re.compile('(o_Vs.\d?)')
vzreg = re.compile('(o_Vz.\d?)')
for f in files[i-1:] :
    # call xspp and find the output files from the returned string
    xsppout=check_output(["./xspp", f, "merid", clin.angle])
    vsfile = vsreg.search(xsppout).expand(r'\1')
    vzfile = vzreg.search(xsppout).expand(r'\1')

    r,ct,V = xsplot.load_merid(vsfile)
    figure()
    xsplot.plot_merid(r,ct,V, title=str(i))
    savefig("slide_Vs_%04d.png" % i)
    close()

    r,ct,V = xsplot.load_merid(vzfile)
    figure()
    xsplot.plot_merid(r,ct,V, title=str(i))
    savefig("slide_Vz_%04d.png" % i)
    close()

    print('>>> slide', i, "saved.\n")
    i+=1

#retcode = os.system( 'mencoder "mf://slide_*.png" -mf fps=10 -o movie.avi -ovc lavc -lavcopts vcodec=mjpeg' )

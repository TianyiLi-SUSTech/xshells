#!/usr/bin/python
#plot a slice
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

parser = argparse.ArgumentParser(
    description='plots a field on a spherical surface as streamlines and colormaps (uses output of xspp surf)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('fname', type=str,
                    help='file to read, must be output by xspp surf. Theta and phi component used for the streamplot')
parser.add_argument('fname2', type=str, nargs='?', default=0,
                    help='file to read, must be output by xspp surf. Radial component is used for colormap plot')
parser.add_argument('-z', '--zoom', type=str, default="1",
                    help='zoom level for color bar, can also be a tuple giving min and max values')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display of the figure (forces a save)')

clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import sys              # acces a la ligne de commande.
import xsplot
import re

czoom=eval(clin.zoom)

if clin.fname.endswith('.npy'):
    a = load(clin.fname)
    t = a[0,0,1:]*180./pi
    p = a[0,1:,0]*180./pi
    ut, up = a[1,1:,1:].T, a[2,1:,1:].T
else:
    t,p,ur,ut,up = xsplot.load_surf(clin.fname)
if clin.fname2 != 0:
    if clin.fname2.endswith('.npy'):
        a = load(clin.fname2)
        br = a[0,1:,1:].T
        cmap = xsplot.get_cmap( xsplot.get_slice_name( a[0,0,0] ) )
    else:
        t,p,br,bt,bp = xsplot.load_surf(clin.fname2)
        cmap = cm.seismic
else:
    br=0

b_scale = -0.74232

rc("font", family="serif")
rc("font", size=10)

label = array([r'$r$',r'$\theta$',r'$\phi$'])

xsplot.stream_surf(t,p, ut,up, col=br*b_scale, czoom=czoom, cmap=cmap, projection='aitoff')

show()

#!/usr/bin/python
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

parser = argparse.ArgumentParser(
    description='''Plots streamlines of the axisymmetric component of a poloidal field
    over contours of the axisymmetric component of a toroidal field''',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('tor', nargs='?', default='o_Vp.m0',
                    help='toroidal field (color contour)')
parser.add_argument('pol', nargs='?', default='o_Vpol.m0',
                    help='poloidal field (streamlines)')
parser.add_argument('--ang', action='store_true',
                    help='set to draw as angular velocity (divide by s)')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display. Image file written instead')
parser.add_argument('-o', '--output', type=str, default='plot_axi.png',
                    help='name of output file.')
clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")
outputfile = nodisplay + (clin.output != parser.get_default('output'))

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import sys
import xsplot

ang = int(clin.ang)
fn = [clin.tor, clin.pol]

if clin.tor.endswith('.npy'):
    y = load(clin.tor)
    r,ct = y[0,1:,0], cos(y[0,0,1:])
    Vp,Pol = y[0,1:,1:], y[1,1:,1:]
    field_name = xsplot.get_slice_name(y[0,0,0])
    cmap = xsplot.get_cmap(field_name)
    field_name = field_name[0]
    if ang:     # convert to angular velocity
        st = sqrt(1-ct*ct).reshape(1,-1)
        Vp = Vp/(r.reshape(-1,1)*st)
        Vp[:,0] = Vp[:,1]
        Vp[:,-1] = Vp[:,-2]
else:
    field_name = fn[0][2]
    cmap = cm.RdBu_r
    if (field_name == 'B'):
        cmap = cm.PuOr
    r,ct,Vp  = xsplot.load_merid(fn[0], ang=ang)
    r,ct,Pol = xsplot.load_merid(fn[1], ang=0)

figure(figsize=(4.6, 6))
subplots_adjust(left=0.01, bottom=0.01, right=0.75, top=0.99, wspace=0.5, hspace=0.2)

m = xsplot.plot_merid(r,ct,Vp, strm=Pol, cmap=cmap)
clim(-m,m)

rmax=amax(r)
label=r"$%c$" % field_name
text(0.75*rmax,0.8*rmax,label, fontsize=28)

if outputfile:
    savefig(clin.output)
if not nodisplay:
    show()


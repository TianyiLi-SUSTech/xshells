#!/usr/bin/python
#plot a slice
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

parser = argparse.ArgumentParser(
    description='plots a meridional slice of a field in a disc (uses output of xspp merid)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('fname', type=str,
                    help='energy file to read')
parser.add_argument('keys', type=str, nargs='*',
                    help='diagnostic keyword to plot')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display of the figure (forces a save)')
parser.add_argument('-o', '--output', type=str, default='plot_diags.png',
                    help='name of output file')
parser.add_argument('-l', '--logy', action='store_true',
                    help='use logarithmic scale on the y-axis')

clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")
outputfile = nodisplay + (clin.output != parser.get_default('output'))

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import sys              # acces a la ligne de commande.
import xsplot

## load diags
d = xsplot.load_diags(clin.fname)


fig = figure()
ax = fig.add_subplot(111)

if 't' in d:
    t = d['t']
elif 'alpha' in d:
    t = d['alpha']

for f in clin.keys:
    for k in f.strip(',').split(','):    # allow commas on command line.
        ax.plot(t, d[k], label=k, marker='+')

if clin.logy:
	ax.set_yscale('log')

ax.grid()
ax.legend(loc='best')
title(clin.fname)

if outputfile:
    savefig(clin.output)
if not nodisplay:
    show()

#!/usr/bin/python
#plot a slice
import matplotlib
import os
import argparse

## test if we are displaying on screen or not.
nodisplay = True
if 'DISPLAY' in os.environ:
    if len(os.environ['DISPLAY']) > 0:
        nodisplay = False

rg=0

parser = argparse.ArgumentParser(
    description='plots contours of a field in a disc (uses output of xspp disc)',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('fname', type=str,
                    help='file to read, must be output by xspp disc')
parser.add_argument('components', type=int, nargs='*', default=[0,1,2],
                    help='vector components (x, y, or z) to plot')
parser.add_argument('-z', '--zoom', type=str, default="1",
                    help='zoom level for color bar, can also be a tuple giving min and max values')
parser.add_argument('--nodisplay', action='store_true',
                    help='set to inhibit display of the figure (forces a save)')

clin = parser.parse_args()

# determine if there will be a displayed plot and if there will be an
# output file
nodisplay = nodisplay + clin.nodisplay
if nodisplay:
    matplotlib.use("Agg")

from pylab import *     # matplotlib loaded after matplotlib.use("Agg")
import xsplot
import sys              # acces a la ligne de commande.
import re               # regular expression

czoom = eval(clin.zoom)
fname = clin.fname
figsze = (16,14)

if fname.endswith(".npy"):
    a = load(fname)    # load numpy data
    for idx in clin.components:
        if idx<a.shape[0] and amax(abs(a[idx,1:,1:])) > 0:
            figure(figsize=figsze)
            xsplot.plot_slice(a, idx, czoom=czoom)
            if nodisplay:
                savefig(fname.replace('.npy', '-%d.png' % idx))
else:
    r,phi, vr, vp, vz = xsplot.load_disc(fname, mres=1)
    a=(vr, vp, vz)
    label = ('$%c_s$' % fname[2],'$%c_\phi$' % fname[2],'$%c_z$' % fname[2])
    for i in clin.components:
        if amax(a[i]) > 0:
            print(label[i])
            figure(figsize=figsze)
            xsplot.plot_disc(r,phi, a[i], czoom=czoom, rg=rg, title=label[i])
            if nodisplay:
                savefig(fname + '-%d.png' % i)

if not nodisplay:
    show()

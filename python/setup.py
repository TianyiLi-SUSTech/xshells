#!/usr/bin/python

from distutils.core import setup

setup(name='pyxspp',
      version='2.2',
      description='python tools for handling field output of xshells',
      url='https://bitbucket.org/nschaeff/shtns/',
      author='Nathanael Schaeffer',
      author_email='nathanael.schaeffer@gmail.com',
      py_modules=['pyxshells','xsplot'],
      scripts=['xsplot'],
      requires=['numpy'],
)

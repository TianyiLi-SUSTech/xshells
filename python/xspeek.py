#!/bin/python
## display in a more readable way the end of an energy.job file from xshells.

from numpy import *
import re
import sys
import subprocess

def get_endlines(filename, nlines=5, firstlines=2):
    f = open(filename, 'rb')
    header = f.readline()   # header
    while len(header) < 3:  header = f.readline()  # skip empty lines

    data = []
    for i in range(0, firstlines):
        l = f.readline()        # first line of data
        data.append( array(l.split(), dtype=float64) )

    f.seek(0,2);  sze = f.tell()    # file size
    ofs = min(nlines*2000, sze)
    f.seek(-ofs, 2)
    lb = f.readlines()
    while len(lb) < nlines and ofs < sze:
        print('ofs = ', ofs)
        ofs = min(3*ofs, sze)
        f.seek(-ofs, 2)
        lb = f.readlines()
    if len(lb) < nlines:
        nlines = len(lb)-1
    lb = lb[-nlines:]
    for l in lb:
        data.append( array(l.split(), dtype=float64) )
    data = array(data)

    field=[]
    if header is not None:        # there is a header
        header = header.decode().lstrip('% \n')        # the header
        RE=re.compile(r'[,\s\t]+')  # regex to split up fields
        field=RE.split(header)[0:-1]

    return(data, field)


def print_transpose(data, keys, prec=8):
    nlines = data.shape[0]
    lenk = 0
    for k in keys:
        lenk = max(lenk, len(k))

    fmth = "%%%ds " % (lenk+1)
    fmtd = "%%%d.%dg " % (prec+7,prec)
    fmts = fmth + fmtd*nlines

    for i in range(0,len(keys)):
        if any(data[:,i] != 0.0):   # supress lines with all zero
            print(fmts % ((keys[i],) + tuple(reversed(data[:,i]))))


def print_normal(data, keys, prec=5, ncol=14, lnames=[]):
    nlines = data.shape[0]

    # find entries that are non-zero
    ilist = []
    keys2 = []
    for i in range(0,data.shape[1]):
        if any(data[:,i] != 0.0):
            ilist.append(i)
            keys2.append(keys[i])
    # keep only those entries
    data = data[:, ilist]
    keys = keys2

    lenk = prec+6
    for k in keys:
        lenk = max(lenk, len(k))

    lenh = 0
    if len(lnames) == 0:
        lnames = nlines * [""]
    else:
        for n in lnames:
            lenh = max(lenh, len(n)+1)
        ncol -= (lenh+lenk-1)//lenk         # adjust number of columns displayed

    fmth0 = "%%-%ds" % lenh
    fmth = "%%-%ds " % lenk
    fmtd = "%%-%d.%dg " % (lenk, prec)
    i=0
    while i < len(keys):
        if i>0:  print("")
        nc = min(ncol, len(keys)-i)
        if nc <= 0:
            break
        print( (fmth0 + fmth*nc) % (("",) + tuple(keys[i:i+nc])) )     # print chunck of keys as header
        for l in range(0,nlines):
            print( (fmth0 + fmtd*nc) % ((lnames[l],) + tuple(data[l,i:i+nc])) )
        i+=nc

def plot_txt(fname, keys, idx=[1,], logscale=True):
    try:
        gnuplot = subprocess.Popen(["/usr/bin/gnuplot"], stdin=subprocess.PIPE)
    except:
        return
    gnuplot.stdin.write(b"set term dumb size 150 28 ansi\n")   # color output with: 'ansi' or 'ansi256'. b&w ouput with 'mono' or nothing.
    gnuplot.stdin.write(b"set style data lines\n")
    gnuplot.stdin.write(b"set tics nomirror scale 0.3\n")
    gnuplot.stdin.write(b"set key above\n")
    gnuplot.stdin.write(b"set colorsequence classic\n")
    if logscale:  gnuplot.stdin.write(b"set logscale y 10\n")
    if isinstance(fname, str):   fname = [fname, ]      # fname must be a list
    if len(fname) > 1:     # loop on file names:
        gnuplot.stdin.write(b"plot '%s' using 1:%d title '%s:%s'" % (fname[0].encode(), idx[0]+1, fname[0].encode(), keys[idx[0]].encode()))
        for k in range(1,len(fname)):
            gnuplot.stdin.write(b", '%s' using 1:%d title '%s:%s'" % (fname[k].encode(), idx[0]+1, fname[k].encode(), keys[idx[0]].encode()))
    else:
        gnuplot.stdin.write(b"plot '%s' using 1:%d title '%s'" % (fname[0].encode(), idx[0]+1, keys[idx[0]].encode()))
        for k in range(1,len(idx)):
            gnuplot.stdin.write(b", '' using 1:%d title '%s'" % (idx[k]+1, keys[idx[k]].encode()))
    gnuplot.stdin.write(b"\nquit\n")
    gnuplot.stdin.flush()
    gnuplot.wait()

if len(sys.argv) < 2:
    print(" usage: %s <energy.job>\n" % sys.argv[0])
    quit()

# FIRST FILE
data, keys = get_endlines(sys.argv[1])

if len(sys.argv) == 2:
    llist = [0,1,-3,-2,-1]
    if len(data) <= len(llist):
        llist = range(0,len(data))
    #print_transpose(data[llist,:], keys)
    #print("")
    print_normal(data[llist], keys)
    plot_txt(sys.argv[1], keys,[1,2,3,4])   # ascii-plot of energies

else:   # SEVERAL FILES (assuming same diags !!!)
    f_list = [sys.argv[1],]
    data = data[-1,:]
    for f in sys.argv[2:]:
        if f not in f_list:
            data2, keys2 = get_endlines(f,1,0)
            if len(keys2) == len(keys):
                data = vstack((data, data2[-1,:]))
                f_list.append(f)
            else:
                print("WARNING: skipping '%s' with %d columns" % (f, len(keys2)) )

    print_normal(data, keys, lnames=f_list)
    plot_txt(f_list, keys, [1,] if any(data[:,1] != 0) else [2,])   ## ascii-plot of kinetic or magnetic energy of all files

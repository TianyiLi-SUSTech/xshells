/** \file xshells.hpp
Compile-time parmeter and definition file for XSHELLS.
 - execution control : XS_LINEAR, ...
 - forcing : calc_Uforcing.
 - mask : build_mask
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
#define XS_LINEAR

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
//#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
//#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF

/* enable output magnetic field surface secular variation up to lmax=lmax_out_sv (set in xshells.par) */
//#define XS_WRITE_SV

/* output z-avereged fields */
//#define XS_WRITE_ZAVG

/* compute electric potential and saves DTS specific measurements at end of run */
//#define XS_DTS_POTENTIAL

/* load surface spectral data from A. Pais and allow 3D extrapolation. */
//#define XS_SURF_PAIS

/* record time serie every XS_TIME_SERIE time step (for DTS/Aldo). 0 means deactivate */
//#define XS_TIME_SERIE 1

/* keep only the axisymmetric JxB (for DTS/Aldo) */
//#define XS_JxB_M0

///  EXECUTION CONTROL  ///
/// 3. testing, unsupported alpha features (not working) ///

/* Poincare force for libration, controlled by a_forcing and w_forcing. 0 for longitudinal libration, 1 for latitudinal libration. */
//#define XS_LIBRATION 1

/* Fake elliptic boundary conditions (alpha). */
//#define XS_ELLIPTIC

/* use pseudo-penalization (slow) : allows computation in arbitrary fluid domains (alpha). */
//#define XS_PENALIZE

#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else

/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	double DeltaOm = a_forcing;

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 1
		DeltaOm = a_forcing;
		t = - w_forcing*t;		// rotation of equatorial spin-axis.
	#define U_FORCING "Inner-core 'spin-over' forcing (l=1, m=1)"
		Bi->Omega_x = DeltaOm * cos(t); 	Bi->Omega_y = -DeltaOm * sin(t);
//	#define U_FORCING "Mantle 'spin-over' forcing (l=1, m=1)"
//		Bo->Omega_x = DeltaOm * cos(t); 	Bo->Omega_y = -DeltaOm * sin(t);
/*  ________
*/

//	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 0
		if (w_forcing < 0.0)		// Periodic forcing on frequency |w_forcing|
		{
			DeltaOm *= sin(-w_forcing*t);
		}
		else if (w_forcing > 0.0)	// Impulsive forcing on time-scale 2.*pi/w_forcing.
		{
			if (t*w_forcing > 63.) {		// t > 10*2*pi/w_forcing
				DeltaOm = 0.0;
			} else {
				t = (t*w_forcing/(2.*M_PI) - 3.);	//t = (t - 3.*t_forcing)/t_forcing;
				DeltaOm *= exp(-t*t);	//   /(t_forcing*sqrt(pi)); //constant energy forcing.
			}
		}
	#define U_FORCING "Inner-core differential rotation (l=1, m=0)"
		Bi->Omega_z = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Inner-core equatorial differential rotation (l=1, m=1)"
//		Bi->Omega_x = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Mantle differential rotation (l=1, m=0)"
//		Bo->Omega_z = DeltaOm;		// set differential rotation of mantle.
//	#define U_FORCING "Mantle differential rotation (l=1, m=1)"
//		Bo->Omega_x = DeltaOm;		// set differential rotation of mantle.
/*  ________
*/

}


#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	int i;
	for (i=0; i<NR; i++) {
		etar[i] = eta0;			// eta0 by default.

	/*	add // at the begining of this line to uncomment the following bloc.
	// DTS conductivity profile
		if (i > NM)		etar[i] = eta0 *9.0; 	// outer shell : inox (*9.0)
		if (i < NG)		etar[i] = eta0 /4.2;	// inner-core : copper (/4.2)
/*  ________
*/	

	/*	add // at the begining of this line to uncomment the following bloc.
	// Earth Mantle
		if (i > NM)		etar[i] = eta0 *1e4; 	// mantle almost insulating
/*  ________
*/	

	}
}
#endif


/* DO NOT REMOVE */
#endif

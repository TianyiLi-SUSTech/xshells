/** \file xshells.hpp
Compile-time parmeters and customizable functions for XSHELLS.
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* call custom_diagnostic() (defined below) from main loop to perform and store custom diagnostics */
#define XS_CUSTOM_DIAGNOSTICS

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
//#define XS_LINEAR

/* Impose arbitrary stationary flow at the boundaries (for Diapir/Monteux) */
#define XS_SET_BC

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
//#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF

/* enable output magnetic field surface secular variation up to lmax=lmax_out_sv (set in xshells.par) */
//#define XS_WRITE_SV

/* compute electric potential and saves DTS specific measurements at end of run */
//#define XS_DTS_POTENTIAL

///  EXECUTION CONTROL  ///
/// 3. testing, unsupported alpha features (not working) ///

/* Poincare force for libration, controlled by a_forcing and w_forcing. 0 for longitudinal libration, 1 for latitudinal libration. */
//#define XS_LIBRATION 0

/* use pseudo-penalization (slow) : alternative to XS_MASK, allows computation in arbitrary fluid domains (alpha). */
//#define XS_PSEUDO_PENALIZE
//#define XS_PENALIZE

#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else

/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	double DeltaOm = a_forcing;

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 1
		DeltaOm = a_forcing;
		t = - w_forcing*t;		// rotation of equatorial spin-axis.
	#define U_FORCING "Inner-core 'spin-over' forcing (l=1, m=1)"
		Bi->Omega_x = DeltaOm * cos(t); 	Bi->Omega_y = -DeltaOm * sin(t);
//	#define U_FORCING "Mantle 'spin-over' forcing (l=1, m=1)"
//		Bo->Omega_x = DeltaOm * cos(t); 	Bo->Omega_y = -DeltaOm * sin(t);
/*  ________
*/

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 0
		if (w_forcing < 0.0)		// Periodic forcing on frequency |w_forcing|
		{
			DeltaOm *= sin(-w_forcing*t);
		}
		else if (w_forcing > 0.0)	// Impulsive forcing on time-scale 2.*pi/w_forcing.
		{
			if (t*w_forcing > 63.) {		// t > 10*2*pi/w_forcing
				DeltaOm = 0.0;
			} else {
				t = (t*w_forcing/(2.*M_PI) - 3.);	//t = (t - 3.*t_forcing)/t_forcing;
				DeltaOm *= exp(-t*t);	//   /(t_forcing*sqrt(pi)); //constant energy forcing.
			}
		}
	#define U_FORCING "Inner-core differential rotation (l=1, m=0)"
		Bi->Omega_z = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Inner-core equatorial differential rotation (l=1, m=1)"
//		Bi->Omega_x = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Mantle differential rotation (l=1, m=0)"
//		Bo->Omega_z = DeltaOm;		// set differential rotation of mantle.
//	#define U_FORCING "Mantle differential rotation (l=1, m=1)"
//		Bo->Omega_x = DeltaOm;		// set differential rotation of mantle.
/*  ________
*/

}

#ifdef XS_SET_BC
void set_U_bc(double ampl)
{
	int l,m;

//	/*	add // at the begining of this line to uncomment the following bloc.
	// sinking diapir
//	l=1;	m=0;	Ulm.set_Prlm(NM+1, l, m, -ampl);		// set u_theta "polar bubble sinking" at the outer boundary.
	l=1;	m=1;	Ulm.set_Prlm(NM+1, l, m, -ampl);		// set u_theta "equatorial bubble sinking" at the outer boundary.
/*  ________
*/

}
#endif


#ifdef XS_CUSTOM_DIAGNOSTICS
/// compute custom diagnostics. They can be stored in the diags array (up to ndiags elements), which is written to the energy file.
/// own(ir) is a macro that returns true if the shell is owned by the process (useful for MPI).
/// i_mpi is the rank of the mpi process (0 is the root).
/// This function is called outside a parallel region, so it is safe to use openmp parallel constructs inside.
/// After this function returns, the diags array is summed accross processes and written in the energy.job file.
/// XS_NDIAGS must be set to the number of recorded custom diagnostics.
void custom_diagnostic(diagnostics& all_diags)
{
	// Uncomment the lines below to use corresponding diagnostic.
	#include "diagnostics/split_energies.cpp"
	#include "diagnostics/fluid_rotation_vector.cpp"
	#include "diagnostics/geodynamo_surface_dipole.cpp"
	#include "diagnostics/dissipation.cpp"
	#include "diagnostics/convective_power.cpp"
	#include "diagnostics/azimuthal_drift_rate.cpp"		// useful for the convection benchmakr (1)
}
#endif


#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	int i;
	for (i=0; i<NR; i++) {
		etar[i] = eta0;			// eta0 by default.

//	/*	add // at the begining of this line to uncomment the following bloc.
	// DTS conductivity profile
		if (i > NM)		etar[i] = eta0 *9.0; 	// outer shell : inox (*9.0)
		if (i < NG)		etar[i] = eta0 /4.2;	// inner-core : copper (/4.2)
/*  ________
*/	

	/*	add // at the begining of this line to uncomment the following bloc.
	// Earth Mantle
		if (i > NM)		etar[i] = eta0 *1e4; 	// mantle almost insulating
/*  ________
*/	

	}
}
#endif


/* DO NOT REMOVE */
#endif

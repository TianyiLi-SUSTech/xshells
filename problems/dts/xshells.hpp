/** \file xshells.hpp
Compile-time parmeters and customizable functions for XSHELLS.
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* call custom_diagnostic() (defined below) from main loop to perform and store custom diagnostics */
#define XS_CUSTOM_DIAGNOSTICS

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
//#define XS_LINEAR

/* Impose arbitrary stationary flow at the boundaries (for Diapir/Monteux) */
//#define XS_SET_BC

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
//#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF

/* enable output magnetic field surface secular variation up to lmax=lmax_out_sv (set in xshells.par) */
//#define XS_WRITE_SV

/* compute electric potential and saves DTS specific measurements at end of run */
//#define XS_DTS_POTENTIAL

///  EXECUTION CONTROL  ///
/// 3. testing, unsupported alpha features (not working) ///

/* Poincare force for libration, controlled by a_forcing and w_forcing. 0 for longitudinal libration, 1 for latitudinal libration. */
//#define XS_LIBRATION 1

/* use pseudo-penalization (slow) : allows computation in arbitrary fluid domains (alpha). */
//#define XS_PENALIZE
//#define XS_PSEUDO_PENALIZE

#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else

/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	double DeltaOm = a_forcing;

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 1
		DeltaOm = a_forcing;
		t = - w_forcing*t;		// rotation of equatorial spin-axis.
	#define U_FORCING "Inner-core 'spin-over' forcing (l=1, m=1)"
		Bi->Omega_x = DeltaOm * cos(t); 	Bi->Omega_y = -DeltaOm * sin(t);
//	#define U_FORCING "Mantle 'spin-over' forcing (l=1, m=1)"
//		Bo->Omega_x = DeltaOm * cos(t); 	Bo->Omega_y = -DeltaOm * sin(t);
/*  ________
*/

//	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 0
		if (w_forcing < 0.0)		// Constant forcing for time t < 2*pi/w_forcing.
		{
			if (-t*w_forcing > 2.*M_PI)  DeltaOm = 0.0;

		}
		else if (w_forcing > 0.0)	// Impulsive forcing on time-scale 2.*pi/w_forcing.
		{
			if (t*w_forcing > 63.) {		// t > 10*2*pi/w_forcing
				DeltaOm = 0.0;
			} else {
				t = (t*w_forcing/(2.*M_PI) - 3.);	//t = (t - 3.*t_forcing)/t_forcing;
				DeltaOm *= exp(-t*t);	//   /(t_forcing*sqrt(pi)); //constant energy forcing.
			}
		}
	#define U_FORCING "Inner-core differential rotation (l=1, m=0)"
		Bi->Omega_z = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Inner-core equatorial differential rotation (l=1, m=1)"
//		Bi->Omega_x = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Mantle differential rotation (l=1, m=0)"
//		Bo->Omega_z = DeltaOm;		// set differential rotation of mantle.
//	#define U_FORCING "Mantle differential rotation (l=1, m=1)"
//		Bo->Omega_x = DeltaOm;		// set differential rotation of mantle.
/*  ________
*/

}

#ifdef XS_CUSTOM_DIAGNOSTICS
/// compute custom diagnostics. They can be stored in the all_diags array , which is written to the energy.jobname file.
/// own(ir) is a macro that returns true if the shell is owned by the process (useful for MPI).
/// i_mpi is the rank of the mpi process (0 is the root).
/// This function is called outside an OpenMP parallel region, and it is permitted to use openmp parallel constructs inside.
/// After this function returns, the all_diags array is summed accross processes and written in the energy.jobname file.
void custom_diagnostic(diagnostics& all_diags)
{
	// Uncomment the lines below to use particular diagnostic.
	#include "diagnostics/split_energies.cpp"
	#include "diagnostics/m_spectrum.cpp"


//	/*	add // at the begining of this line to uncomment the following bloc.
	// stores point measurements (for DTS).
	{
		const int npts = 15; // B_phi in the sleeve at 5 radii x 3 latitudes
		const double rar[] = { 185.7/210, 162.7/210, 139.7/210, 116.7/210, 93.7/210 }; // radii (mm/mm)
		const double tar[] = { (90-40)*M_PI/180, (90-10)*M_PI/180, (90+20)*M_PI/180 }; // colatitudes (rd)

		double* diags = all_diags.append(npts, " BP1+40, BP1+10, BP1-20,  BP2+40, BP2+10, BP2-20,  BP3+40, BP3+10, BP3-20,  BP4+40, BP4+10, BP4-20,  BP5+40, BP5+10, BP5-20\t ");	// all processes must reserve the same diagnostics
		
		for (int ii = 0; ii<5; ii++) {
			int ir = r_to_idx( rar[ii] );

			if ((own(ir)) && (evol_ubt & EVOL_B)) {
				const size_t nspat = shtns->nspat;
				cplx *Q, *S;
				double *br,*bt,*bp;
				Q = (cplx*) VMALLOC(sizeof(double)*(4*NLM + 3*nspat));		// alloc aligned memory
				br = (double*) (Q + 2*NLM);
				S = Q + NLM;
				bt = br + nspat;	bp = br + 2*nspat;

				Blm.RadSph(ir, Q, S);
				SHV3_SPAT(Q, S, Blm.Tor[ir], br, bt, bp);
				for (int j=0; j<3; j++) {
					int it = theta_to_idx(tar[j]);
					diags[ii*3+j] = bp[it];
				}
				VFREE(Q);		// free memory
			}

		}
	}
	{
		const int npts = 2; // U_phi at s=0.4 on the equator and at z=-0.4
		const double rar[] = { 0.4, 0.56569 };
		const double tar[] = { (90*M_PI/180), (90+45)*M_PI/180 }; // colatitudes (rad)

		double* diags = all_diags.append(npts, " Up_eq, Up_so\t ");	// all processes must reserve the same diagnostics
		
		for (int ii = 0; ii<npts; ii++) {
			int ir = r_to_idx( rar[ii] );
			int it = theta_to_idx(tar[ii]);

			if ((own(ir)) && (evol_ubt & EVOL_U)) {
				const size_t nspat = shtns->nspat;
				cplx *Q, *S;
				double *br,*bt,*bp;
				Q = (cplx*) VMALLOC(sizeof(double)*(4*NLM + 3*nspat));		// alloc aligned memory
				br = (double*) (Q + 2*NLM);
				S = Q + NLM;
				bt = br + nspat;	bp = br + 2*nspat;

				Ulm.RadSph(ir, Q, S);
				SHV3_SPAT(Q, S, Ulm.Tor[ir], br, bt, bp);
				diags[ii] = bp[it];
				VFREE(Q);		// free memory
			}

		}
	}

/*  ________
*/

//	/*	add // at the begining of this line to uncomment the following bloc.
	// compute electric potential at a given radius (for DTS).
	#ifndef XS_LINEAR
	{
		const int npts = 5; // electric potential in the outer shell at 5 latitudes
		const double tar[] = {(90-45)*M_PI/180, (90-35)*M_PI/180, (90-25)*M_PI/180, (90-15)*M_PI/180, (90-5)*M_PI/180}; // colatitudes (rad)
		const int ir = r_to_idx(213./210.); // radius (mm/mm)

		double* diags = all_diags.append(npts, " V+45, V+35, V+25, V+15, V+05\t ");	// all processes must reserve the same diagnostics

		if ((own(ir)) && ((evol_ubt & (EVOL_B|EVOL_U)) == (EVOL_U|EVOL_B))) {
			const size_t nspat = shtns->nspat;
			cplx *Vlm;
			double *V;
			Vlm = (cplx*) VMALLOC(sizeof(double) * (2*NLM + nspat));
			V = (double*) (Vlm + NLM);

			Electric_Potential_shell(ir, Vlm);
			SH_SPAT(Vlm, V);		// to spatial domain
			for (int j=0; j<npts; j++) {
				int it = theta_to_idx(tar[j]);
				diags[j] = V[it];		// record potential
			}
			VFREE(Vlm); // free memory
		}
	}
	#endif
/*  ________
*/

}
#endif


#if defined(XS_PENALIZE) || defined(XS_PSEUDO_PENALIZE)

class  DTSshaft {
	double smin;		// shaft axis
	double Vtotal;		// total volume
	double Vsolid;		// volume of the solid
  public:
	/// these public members are mandatory to define a proper penalizer.
	double C_pen;		// penalization coefficient
	double v2int_solid;	// v2 integrated over the solid.
	int shell_penalized(int ir) const;
	int point_penalized(int ir, int it, int ip) const;
	void init(mparser& par);		// initialize from parameter file
	void adjust_penalizer(double nrjU);
};

void DTSshaft::init(mparser &par)
{
	smin = 0.095*r[NM];
	printf("> Penalizing s < %g (DTS shaft). Inner sphere should also be fixed)\n",smin);

	C_pen = par.var["C_pen"];
	if (C_pen == 0) C_pen = 1.0;

	Vtotal = shell_volume(r[NG], r[NM]);	// approx volume
	Vsolid = M_PI*smin*smin*(r[NM]-r[NG]);	// approx volume
}

int DTSshaft::shell_penalized(int ir) const
{
	if ((r[ir] > r[NM]) || (r[ir] < r[NG])) return 0;		// nothing to do, the shell is completely fluid.
	return 1;
}

int DTSshaft::point_penalized(int ir, int it, int ip) const
{
	if ((r[ir] > r[NM]) || (r[ir] < r[NG])) return 0;		// nothing to do, the shell is completely fluid.

	double s = r[ir]*st[it];
	if (s < smin) return 1;			// we are in the excluded region
	return 0;	// liquid
}

void DTSshaft::adjust_penalizer(double nrjU)
{
	if (nrjU > 0.0) {
		double vrms_solid = sqrt(v2int_solid / Vsolid);
		double vrms_total = sqrt(2.*nrjU / Vtotal);

		double f = vrms_solid / vrms_total;
		printf(">  vrms_solid/vrms_total = %g :: old C_pen = %g   ", f, C_pen);
		if (f > 0.1) {
			C_pen *= 10.;
		} else if (f > 0.01) {
			C_pen *= 2.;
		} else if (f < 0.001) {
			C_pen *= 0.6;
		}
		printf("new C_pen = %g\n", C_pen);
	}
}

DTSshaft penalizer;	// the penalizer is the DTS shaft.

#endif



#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	for (int i=0; i<NR; i++) {
		etar[i] = eta0;			// eta0 by default.

//	/*	add // at the begining of this line to uncomment the following bloc.
	// DTS conductivity profile
		if (i > NM)		etar[i] = eta0 *9.0; 	// outer shell : inox (*9.0)
		if (i < NG)		etar[i] = eta0 /4.2;	// inner-core : copper (/4.2)
/*  ________
*/	

	}
}
#endif


#ifdef XS_DTS_POTENTIAL
  #include "xshells_render.cpp"
// write magnetic field and electric potential probes
// WARNING: the non-axisymmetric magnetic field is set to ZERO !
void write_point_measurments(PolTor &Blm, ScalarSH &Vlm, SolidBody &IC, const char* job)
{
	cplx Qlm[NLM];
	int ir,lm,it;
	double rr, br,bt,bp;
	FILE *fp;
	char fname[120];

	// in the latitude arrays below, value 999 marks the end of the array.
	int Br_lat_N[] = {3,9,15,21,27,33, 999};	// lat nord, coordonnees des sondes. Pas coord sud car sym vav equateur.
	int Bt_lat_N[] = {3,15,27,33,45, 999};		// lat nord
	int V_lat[]    = {5,15,25,35,45, 999};		// sondes de potentiel
	int ddg_lat[]  = {-20, 40, 10, 999};

	// radial position in the ddg from surface radius rsurf (999 marks the end of the array.)
	double rsurf = 0.240;
	double rnorm = 0.210;
	double Br_ddg_r[] = {0.044, 0.084,  999};	// dans l'ordre rayon=[R1,R3]
	double Bt_ddg_r[] = {0.036, 0.077,  999};	// dans l'ordre rayon=[T1,T3]
	double Bp_ddg_r[] = {0.0325, 0.0525, 0.0735, 0.096, 0.1145, 0.134,  999}; 	// dans l'ordre rayon=[P1,P2,P3,P4,P5,P6]

	// compute torque before substracting imposed dipole.
	double torque = calc_TorqueMag_surf(Blm, B0lm, NG);

	// non-axisymmetric magnetic field is set to zero => only m=0 contributes to the following probes !
	for (ir=Blm.irs; ir<=Blm.ire; ir++) {
		for (lm=0; lm<=IC.lmax; lm++) {         // handle imposed axisymmetric field (m=0)
			double z = pow(r[ir]/r[Blm.irs], -lm-1);                // internal field
			Blm.Pol[ir][lm] -= IC.PB0lm[lm]/(2*lm+1) * z;           // substract imposed dipole
		}
		for (lm=LMAX+1; lm<NLM; lm++) {
			Blm.Pol[ir][lm] = 0.0;
			Blm.Tor[ir][lm] = 0.0;
		}
	}

	sprintf(fname,"dts_probes.%s",job);
	fp = fopen(fname, "w");
	fprintf(fp, "%% DTS probes readings (Inner-core angular position phi=%g radians)\n",IC.phi0);
	for (lm=0; lm<NLM; lm++) Qlm[lm] = 0.0;		// zero

	// *** doigt de gant ***
	it=0;
	while(ddg_lat[it] != 999) {
		fprintf(fp, "%% ddg magnetic probes (lat=%d):\n", ddg_lat[it]);
		double theta = (90. - ddg_lat[it])*M_PI/180;
		ir=0;
		while(Br_ddg_r[ir] != 999) {
			rr = (rsurf - Br_ddg_r[ir])/rnorm;
			Blm.to_point_interp(rr, cos(theta), 0, &br, &bt, &bp);
			fprintf(fp, "%g %g %g\n", rr, theta, br);
			ir++;
		}
		ir=0;
		while(Bt_ddg_r[ir] != 999) {
			rr = (rsurf - Bt_ddg_r[ir])/rnorm;
			Blm.to_point_interp(rr, cos(theta), 0, &br, &bt, &bp);
			fprintf(fp, "%g %g %g\n", rr, theta, bt);
			ir++;
		}
		ir=0;
		while(Bp_ddg_r[ir] != 999) {
			rr = (rsurf - Bp_ddg_r[ir])/rnorm;
			Blm.to_point_interp(rr, cos(theta), 0, &br, &bt, &bp);
			fprintf(fp, "%g %g %g\n", rr, theta, bp);
			ir++;
		}
		it++;
	}

	// *** electric potential at surface probes ***
	ir = NM+1;		// right after the last fluid shell.
	if (ir>=NR) ir=NR-1;	// we should stay in the magnetic domain
	fprintf(fp, "%% surface potential probes (r=%g):\n", r[ir]);
	for (lm=0; lm<=LMAX; lm++) Qlm[lm] = Vlm[ir][lm];		// copy axisymmetric potential of this shell.
	it=0;
	while(V_lat[it] != 999) {
		double theta = (90. - V_lat[it])*M_PI/180;
		double vv = SH_to_point(shtns, Qlm, cos(theta), 0);
		fprintf(fp, "%g %g %g\n", r[ir], theta, vv);
		it++;
	}

	// *** magnetic field at surface probes ***
	rr = 216./210.;		// this radius is arbitrary and can be outside the magnetic domain.
	fprintf(fp, "%% surface magnetic probes (r=%g):\n", rr);
	fprintf(fp, "%%    B_r\n");
	it=0;
	while(Br_lat_N[it] != 999) {
		double theta = (90. - Br_lat_N[it])*M_PI/180;
		Blm.to_point_interp(rr, cos(theta), 0, &br, &bt, &bp);
		fprintf(fp, "%g %g %g\n", rr, theta, br);
		it++;
	}
	fprintf(fp, "%%    B_theta\n");
	it=0;
	while(Bt_lat_N[it] != 999) {
		double theta = (90. - Bt_lat_N[it])*M_PI/180;
		Blm.to_point_interp(rr, cos(theta), 0, &br, &bt, &bp);
		fprintf(fp, "%g %g %g\n", rr, theta, bt);
		it++;
	}

	// *** torque at inner-core ***
	fprintf(fp, "%% torque at inner core (r=%g):\n",r[NG]);
	fprintf(fp, "%g 0 %.15g",r[NG], torque);

	fclose(fp);
}
#endif

/* DO NOT REMOVE */
#endif

from numpy import *
from pyxshells import *
import shtns

# this program will compute the EMF produced by the "fluctuating" fields
ext = 'dns1'		# job name
istart = 1700		# starting snapshot
iend = 1709			# end snapshot


info,r=get_field_info('fieldU_%04d.%s' % (istart,ext))
sh = shtns.sht(info['lmax'], info['mmax'], info['mres'])
sh.set_grid(nlat=150, nl_order=2, flags=shtns.sht_reg_poles)

Ulm = load_field('fieldU_avg_%04d-%04d.%s' % (istart,iend,ext), lazy=False)
Blm = load_field('fieldB_avg_%04d-%04d.%s' % (istart,iend,ext), lazy=False)

nr = Ulm.ire-Ulm.irs+1
Uavg = zeros((nr, 3, sh.nlm),dtype=complex128)
Bavg = zeros((nr, 3, sh.nlm),dtype=complex128)

# compute the 3 components of average fields:
for ir in range(Ulm.irs, Ulm.ire+1):
	Uavg[ir-Ulm.irs, 0, :] = Ulm.rad(ir)
	Uavg[ir-Ulm.irs, 1, :] = Ulm.sph(ir)
	Uavg[ir-Ulm.irs, 2, :] = Ulm.tor(ir)
	Bavg[ir-Ulm.irs, 0, :] = Blm.rad(ir)
	Bavg[ir-Ulm.irs, 1, :] = Blm.sph(ir)
	Bavg[ir-Ulm.irs, 2, :] = Blm.tor(ir)

Uavg[:, :, sh.m > 0] = 0.0		# keep only the m=0 components
Bavg[:, :, sh.m > 0] = 0.0

EMFavg = zeros((nr, 3, sh.nlat, sh.nphi))		# accumulator for EMF

nacc = 0
for i in range(istart, iend+1):
	try:
		Ulm = load_field( 'fieldU_%04d.%s' % (i,ext), lazy=False)
		Blm = load_field( 'fieldB_%04d.%s' % (i,ext), lazy=False)
	except IOError:
			print('[%04d] missing files' % i)
	else:
		nacc += 1
		print('[%04d]' % i)
		# compute emf of fluctuations
		for ir in range(Ulm.irs, Ulm.ire+1):
			urad = Ulm.rad(ir) - Uavg[ir-Ulm.irs, 0, :]	# velocity fluctuations
			usph = Ulm.sph(ir) - Uavg[ir-Ulm.irs, 1, :]
			utor = Ulm.tor(ir) - Uavg[ir-Ulm.irs, 2, :]
			
			brad = Blm.rad(ir) - Bavg[ir-Ulm.irs, 0, :]	# magnetic fluctuations
			bsph = Blm.sph(ir) - Bavg[ir-Ulm.irs, 1, :]
			btor = Blm.tor(ir) - Bavg[ir-Ulm.irs, 2, :]

			ur,ut,up = sh.synth(urad, usph, utor)
			br,bt,bp = sh.synth(brad, bsph, btor)

			uxb_r = ut*bp - up*bt
			uxb_t = up*br - ur*bp
			uxb_p = ur*bt - ut*br

			EMFavg[ir - Ulm.irs, 0, :,:] += uxb_r		# accumulate EMF
			EMFavg[ir - Ulm.irs, 1, :,:] += uxb_t
			EMFavg[ir - Ulm.irs, 2, :,:] += uxb_p

print('=> %d snapshots averaged.' % nacc)
EMFavg *= (1.0/nacc)	# rescale by number of snapshots

EMFavg_m0 = average(EMFavg, axis=3)		# average over phi

# write down the result as a merid slice (r,theta,phi).
a = zeros((nr+1, sh.nlat+1))
a[0,1:] = sh.cos_theta
a[1:,0] = Ulm.grid.r[Ulm.irs:Ulm.ire+1]

a[1:,1:] = EMFavg_m0[:, 0, :]
savetxt('EMF_fluct_r.%s' % ext, a)			# radial component of mean EMF
a[1:,1:] = EMFavg_m0[:, 1, :]
savetxt('EMF_fluct_t.%s' % ext, a)			# theta component of mean EMF
a[1:,1:] = EMFavg_m0[:, 2, :]
savetxt('EMF_fluct_p.%s' % ext, a)			# phi component of mean EMF


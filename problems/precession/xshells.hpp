/** \file xshells.hpp
Compile-time parmeters and customizable functions for XSHELLS.
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* call custom_diagnostic() (defined below) from main loop to perform and store custom diagnostics */
#define XS_CUSTOM_DIAGNOSTICS

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
//#define XS_LINEAR

/* Impose arbitrary stationary flow at the boundaries (for Diapir/Monteux) */
//#define XS_SET_BC

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
//#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
//#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF



#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else


/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	double DeltaOm = a_forcing;

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 1
		DeltaOm = a_forcing;
		t = - w_forcing*t;		// rotation of equatorial spin-axis.
	#define U_FORCING "Inner-core 'spin-over' forcing (l=1, m=1)"
		Bi->Omega_x = DeltaOm * cos(t); 	Bi->Omega_y = -DeltaOm * sin(t);
//	#define U_FORCING "Mantle 'spin-over' forcing (l=1, m=1)"
//		Bo->Omega_x = DeltaOm * cos(t); 	Bo->Omega_y = -DeltaOm * sin(t);
/*  ________
*/

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 0
		if (w_forcing < 0.0)		// Periodic forcing on frequency |w_forcing|
		{
			DeltaOm *= sin(-w_forcing*t);
		}
		else if (w_forcing > 0.0)	// Impulsive forcing on time-scale 2.*pi/w_forcing.
		{
			if (t*w_forcing > 63.) {		// t > 10*2*pi/w_forcing
				DeltaOm = 0.0;
			} else {
				t = (t*w_forcing/(2.*M_PI) - 3.);	//t = (t - 3.*t_forcing)/t_forcing;
				DeltaOm *= exp(-t*t);	//   /(t_forcing*sqrt(pi)); //constant energy forcing.
			}
		}
	#define U_FORCING "Inner-core differential rotation (l=1, m=0)"
		Bi->Omega_z = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Inner-core equatorial differential rotation (l=1, m=1)"
//		Bi->Omega_x = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Mantle differential rotation (l=1, m=0)"
//		Bo->Omega_z = DeltaOm;		// set differential rotation of mantle.
//	#define U_FORCING "Mantle differential rotation (l=1, m=1)"
//		Bo->Omega_x = DeltaOm;		// set differential rotation of mantle.
/*  ________
*/

}


#ifdef XS_CUSTOM_DIAGNOSTICS
/// compute custom diagnostics. They can be stored in the all_diags array , which is written to the energy.jobname file.
/// own(ir) is a macro that returns true if the shell is owned by the process (useful for MPI).
/// i_mpi is the rank of the mpi process (0 is the root).
/// This function is called outside an OpenMP parallel region, and it is permitted to use openmp parallel constructs inside.
/// After this function returns, the all_diags array is summed accross processes and written in the energy.jobname file.
void custom_diagnostic(diagnostics& all_diags)
{
	int sb_removed = 0;
//	/*	add // at the begining of this line to uncomment the following bloc.
	// removes axial solid-body rotation before the diagnostics (amplitude = 1).
	#ifndef XS_LINEAR
	if ((evol_ubt & EVOL_U) && (!frame.check_time_dep())) {
		solid_body_rotation_add(Ulm, -1.0);
		sb_removed = 1;
	}
	#endif
/*  ________
*/

	// Uncomment the lines below to use particular diagnostic.
	#include "diagnostics/split_energies.cpp"
	#include "diagnostics/central_symmetry_energy.cpp"
	#include "diagnostics/m_spectrum.cpp"
	#include "diagnostics/fluid_rotation_vector.cpp"
	#include "diagnostics/geodynamo_surface_dipole.cpp"
	#include "diagnostics/dissipation.cpp"

	if (sb_removed) {		// put axial solid-body rotation back in.
		solid_body_rotation_add(Ulm, 1.0);
	}
}
#endif


#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	static double etam = 0;
	
	if (etam==0) {
		etam = mp.var["etam"];		// value read from xshells.par file.
	}

	for (int i=0; i<NR; i++) {
		etar[i] = eta0;				// eta0 by default.
		if ((i > NM) && (etam))		etar[i] = eta0 * etam; 		// mantle almost insulating
	}
}
#endif


/* DO NOT REMOVE AND DO NOT ADD ANYTHING BEYOND THIS LINE */
#endif


##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

## precession spherical shell, computed in the non-rotating but precessing frame.
## Parameters reproduce one run of Tilgner GAFD 2007.

job = tilgner           # job name, used as a suffix for output files.

### PHYSICAL PARAMS ###
Pm = 8
Omega0 = 0.14   		# precession rate
Omega0_angle = 120*pi/180	# precession angle (radians)
nu = 5e-4		# viscosity
eta = nu/Pm

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.
u = sb_rot * 1		# in the non-rotating but precessing frame
#b = random * 1e-4

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file).
BC_U = 2,1		# inner,outer boundary conditions (1=no-slip (default), 2=free-slip)
R_U = 1/9:10/9		# Velocity field boundaries
R_B = 1/9:10/9
kill_sbr = 0		# (for free-slip only) 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed. 2: conserve angular momentum to its inital value.

### NUMERICAL SCHEME ###
NR = 144		# total number of radial shells (overriden by r = ...)
N_BL = 0,0		# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 1	# 0: fixed time-step (default), 1: variable time-step
dt = 0.001		# time step
iter_max = 25000	  	# iteration number (total number of text and energy file ouputs)
sub_iter = 50	  	# sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 5000    	# number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 63	# max degree of spherical harmonics
Mmax = 63	# max fourier mode (phi)
Mres = 1	# phi-periodicity.
#Nlat = 90	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
restart = 1	# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
movie = 1		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#zavg = 2		# 1=output z-averaged field, 2=also output rms z-averaged.
#lmax_out_surf = 84		# output surface (secular variation) data up to lmax_out_surf if lmax_out_surf > 0
backup_time = 330               # ensures that full fields are saved to disk at least every backup_time minutes, for restart.
nbackup = 4                     # stops after 4 backups
out_prec = 2                    # precision of snapshots.

L_rmin = 0+8*sqrt(nu)	# lower radial bound for integration of angular momentum 
L_rmax = 1-8*sqrt(nu)	# upper radial bound for integration of angular momentum

### ALGORITHM FINE TUNING ###

## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
C_vort = 0.1
C_u = 0.1
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 4

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10


